/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Davide
 */
public class DatiFatturazione {
    private int id;
    private int idEsercente;
    private String ragioneSociale;
    private String sedeLegale;
    private String partitaIva;
    private String codiceFiscale;
    private String dataContratto;
    private String dataRegistrazione;
    private String pdfContratto;
    private String iban;
    
    public DatiFatturazione(){
        
    }

    /**
     * @return the ragioneSociale
     */
    public String getRagioneSociale() {
        return ragioneSociale;
    }

    /**
     * @param ragioneSociale the ragioneSociale to set
     */
    public void setRagioneSociale(String ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
    }

    /**
     * @return the sedeLegale
     */
    public String getSedeLegale() {
        return sedeLegale;
    }

    /**
     * @param sedeLegale the sedeLegale to set
     */
    public void setSedeLegale(String sedeLegale) {
        this.sedeLegale = sedeLegale;
    }

    /**
     * @return the partitaIva
     */
    public String getPartitaIva() {
        return partitaIva;
    }

    /**
     * @param partitaIva the partitaIva to set
     */
    public void setPartitaIva(String partitaIva) {
        this.partitaIva = partitaIva;
    }

    /**
     * @return the codiceFiscale
     */
    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    /**
     * @param codiceFiscale the codiceFiscale to set
     */
    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    /**
     * @return the dataContratto
     */
    public String getDataContratto() {
        return dataContratto;
    }
    

    /**
     * @param dataContratto the dataContratto to set
     */
    public void setDataContratto(String dataContratto) {
        this.dataContratto = dataContratto;
    }

    /**
     * @return the dataRegistrazione
     */
    public String getDataRegistrazione() {
        return dataRegistrazione;
    }

    /**
     * @param dataRegistrazione the dataRegistrazione to set
     */
    public void setDataRegistrazione(String dataRegistrazione) {
        this.dataRegistrazione = dataRegistrazione;
    }

    /**
     * @return the pdfContratto
     */
    public String getPdfContratto() {
        return pdfContratto;
    }

    /**
     * @param pdfContratto the pdfContratto to set
     */
    public void setPdfContratto(String pdfContratto) {
        this.pdfContratto = pdfContratto;
    }

    /**
     * @return the idEsercente
     */
    public int getIdEsercente() {
        return idEsercente;
    }

    /**
     * @param idEsercente the idEsercente to set
     */
    public void setIdEsercente(int idEsercente) {
        this.idEsercente = idEsercente;
    }

    /**
     * @return the iban
     */
    public String getIban() {
        return iban;
    }

    /**
     * @param iban the iban to set
     */
    public void setIban(String iban) {
        this.iban = iban;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
}
