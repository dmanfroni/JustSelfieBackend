/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author Davide
 */
public class ScaglioneSelfie implements Serializable{
    private int id;
    private int scaglioneMinimo;
    private int scaglioneMassimo;
    private double provvigione;
    
    public ScaglioneSelfie(){
        
    }

    /**
     * @return the scaglioneMinimo
     */
    public int getScaglioneMinimo() {
        return scaglioneMinimo;
    }

    /**
     * @param scaglioneMinimo the scaglioneMinimo to set
     */
    public void setScaglioneMinimo(int scaglioneMinimo) {
        this.scaglioneMinimo = scaglioneMinimo;
    }

    /**
     * @return the scaglioneMassimo
     */
    public int getScaglioneMassimo() {
        return scaglioneMassimo;
    }

    /**
     * @param scaglioneMassimo the scaglioneMassimo to set
     */
    public void setScaglioneMassimo(int scaglioneMassimo) {
        this.scaglioneMassimo = scaglioneMassimo;
    }

    

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the provvigione
     */
    public double getProvvigione() {
        return provvigione;
    }

    /**
     * @param provvigione the provvigione to set
     */
    public void setProvvigione(double provvigione) {
        this.provvigione = provvigione;
    }
    
}
