/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Davide
 */
public enum StatoApprovazione {
    APPROVATO,
    NON_APPROVATO,
    IN_APPROVAZIONE
}
