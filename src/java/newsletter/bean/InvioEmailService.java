/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsletter.bean;

import java.io.File;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import newsletter.model.EmailUtente;
import newsletter.model.Template;
import newsletter.model.TipoRegistrazione;
import util.DBConnection;
import util.Mailin;
import util.NewsLetterDAO;

/**
 *
 * @author Davide
 */
@ManagedBean(name = "invioEmailService")
@ViewScoped
public class InvioEmailService implements Serializable {

    private List<TipoRegistrazione> tipiRegistrazione;
    private List<Template> template;
    private List<EmailUtente> emailUtente;
    private int idTipoRegistrazione;
    private int idTemplate;
    private int idGruppo;
    private List<Integer> gruppi;
    private final int GROUP_SIZE = 250;
    private String response;

    @PostConstruct
    public void init() {
        try(Connection connection = DBConnection.getNewsletterConnection()) {
            setTemplate(NewsLetterDAO.readTemplate(connection));
            setTipiRegistrazione(NewsLetterDAO.readTipoRegistrazione(DBConnection.getNewsletterConnection()));
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void inviaEmail() {
         try(Connection connection = DBConnection.getNewsletterConnection()) {
            Template template = NewsLetterDAO.readTemplate(connection, idTemplate);
            List<EmailUtente> emailUtenteList = NewsLetterDAO.readEmailUtenteFiltrate(connection, idTipoRegistrazione);
            int totaleEmail = emailUtenteList.size();
            int emailInviate = 0;
            for(EmailUtente emailUtente : emailUtenteList){
                sendEmail(template, emailUtente);
                emailInviate++;
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Successo!",  "Email presenti:" + totaleEmail + " - Email inviate: " + emailInviate));
           
            
        } catch (Exception ex) {
 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!",  ex.getMessage()));
           
        }
    }
    
    private void sendEmail(Template template, EmailUtente emailUtente) {
        String title = template.getTitle();
        String text = template.getText();
        String sendFrom = template.getSendFrom();
        String subject = template.getSubject();
        

        if (text.contains("#SELFIECOIN")) {
            text = text.replace("#SELFIECOIN", emailUtente.getSelfieCoin() + "");
        }
        if (text.contains("#NOMEUTENTE")) {
            text = text.replace("#NOMEUTENTE", emailUtente.getName() + "");
        }
        ClassLoader classLoader = getClass().getClassLoader();
        File input = new File(classLoader.getResource("/newsletter/properties/user_acquisition_email.html").getFile());
        StringBuilder result = new StringBuilder();
        try (Scanner scanner = new Scanner(input)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.contains("#TEXT")) {
                    line = line.replace("#TEXT", text);
                }
                if (line.contains("#TITLE")) {
                    line = line.replace("#TITLE", title);
                }
                result.append(line);
            }
            scanner.close();
            Mailin http = new Mailin("https://api.sendinblue.com/v2.0", "wr8g3YFWtKpNcfXT");
            Map< String, Object> to = new HashMap< String, Object>();
            to.put(emailUtente.getEmail().trim(), emailUtente.getName().trim());
            Map< String, Object> data = new HashMap< String, Object>();
            data.put("to", to);
            data.put("html", result.toString());
            data.put("subject", subject);
            data.put("from", new String[]{"info@justselfie.it", sendFrom});
            String str = http.send_email(data);
            System.out.println(emailUtente.getEmail() + "\n" +str);
        }catch(Exception ex){
            ex.printStackTrace();
            
        }
    }
    
     

    /**
     * @return the tipiRegistrazione
     */
    public List<TipoRegistrazione> getTipiRegistrazione() {
        return tipiRegistrazione;
    }

    /**
     * @param tipiRegistrazione the tipiRegistrazione to set
     */
    public void setTipiRegistrazione(List<TipoRegistrazione> tipiRegistrazione) {
        this.tipiRegistrazione = tipiRegistrazione;
    }

    /**
     * @return the template
     */
    public List<Template> getTemplate() {
        return template;
    }

    /**
     * @param template the template to set
     */
    public void setTemplate(List<Template> template) {
        this.template = template;
    }

    /**
     * @return the idTipoRegistrazione
     */
    public int getIdTipoRegistrazione() {
        return idTipoRegistrazione;
    }

    /**
     * @param idTipoRegistrazione the idTipoRegistrazione to set
     */
    public void setIdTipoRegistrazione(int idTipoRegistrazione) {
        this.idTipoRegistrazione = idTipoRegistrazione;
    }

    /**
     * @return the idTemplate
     */
    public int getIdTemplate() {
        return idTemplate;
    }

    /**
     * @param idTemplate the idTemplate to set
     */
    public void setIdTemplate(int idTemplate) {
        this.idTemplate = idTemplate;
    }

    /**
     * @return the idGruppo
     */
    public int getIdGruppo() {
        return idGruppo;
    }

    /**
     * @param idGruppo the idGruppo to set
     */
    public void setIdGruppo(int idGruppo) {
        this.idGruppo = idGruppo;
    }

    /**
     * @return the gruppi
     */
    public List<Integer> getGruppi() {
        return gruppi;
    }

    /**
     * @param gruppi the gruppi to set
     */
    public void setGruppi(List<Integer> gruppi) {
        this.gruppi = gruppi;
    }

    /**
     * @return the emailUtente
     */
    public List<EmailUtente> getEmailUtente() {
        return emailUtente;
    }

    /**
     * @param emailUtente the emailUtente to set
     */
    public void setEmailUtente(List<EmailUtente> emailUtente) {
        this.emailUtente = emailUtente;
    }

    /**
     * @return the response
     */
    public String getResponse() {
        return response;
    }

    /**
     * @param response the response to set
     */
    public void setResponse(String response) {
        this.response = response;
    }

}
