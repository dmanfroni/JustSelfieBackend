/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsletter.bean;

import java.io.File;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.ws.rs.core.Response;
import newsletter.model.EmailUtente;
import newsletter.model.Template;
import newsletter.model.TipoRegistrazione;
import util.Costanti;
import util.DBConnection;
import util.Mailin;
import util.NewsLetterDAO;

/**
 *
 * @author Davide
 */
@ManagedBean(name = "emailUtenteService")
@ViewScoped
public class EmailUtenteService implements Serializable {

    private List<EmailUtente> elencoEmailUtenti;
    private List<TipoRegistrazione> elencoTipiRegistrazione;
    private EmailUtente emailUtenteSelezionato;
    private List<Template> elencoTemplate;
    private int idTemplateSelezionato;

    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.containsKey("id")) {
            int id = Integer.valueOf(params.get("id"));
            try(Connection connection = DBConnection.getNewsletterConnection()) {
                setEmailUtenteSelezionato(NewsLetterDAO.readEmailUtente(connection, id));
                setElencoTemplate(NewsLetterDAO.readTemplate(connection));
                setElencoTipiRegistrazione(NewsLetterDAO.readTipoRegistrazione(connection));
            } catch (ClassNotFoundException | SQLException ex) {
                System.out.println(ex.getMessage());
                setEmailUtenteSelezionato(new EmailUtente());
            }
        } else {
            try(Connection connection = DBConnection.getNewsletterConnection()) {
                setElencoEmailUtenti(NewsLetterDAO.readEmailUtente(connection));
            } catch (ClassNotFoundException | SQLException ex) {
                System.out.println(ex.getMessage());
                setElencoEmailUtenti(new ArrayList<>());
            }
        }
    }

    public String createOrUpdateIfExist() {
        try(Connection connection = DBConnection.getNewsletterConnection()) {
            
            if (getEmailUtenteSelezionato().getId() > 0) {
                NewsLetterDAO.updateEmailUtente(connection, getEmailUtenteSelezionato());
                return "elencoEmailUtente";
            } else {
                int selfieCoin = 0;
                String registeredAt = "";

                SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date endDate = myFormat.parse(Costanti.dayPromoEnd);
                Date currentDate = new Date();
                registeredAt = myFormat.format(currentDate);
                long diff = endDate.getTime() - currentDate.getTime();
                selfieCoin = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) * Costanti.factorySelfieCoin;
                getEmailUtenteSelezionato().setSelfieCoin(selfieCoin);
                getEmailUtenteSelezionato().setRegisteredAt(registeredAt);

                NewsLetterDAO.createEmailUtente(connection, getEmailUtenteSelezionato());
                return "elencoEmailUtente";
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Qualcosa è andato storto durante l'aggiornamento. Ricontrollare i dati inseriti"));
            return "";
        }
    }

    public String createOrUpdateIfExistWithEmail() {
        try (Connection connection = DBConnection.getNewsletterConnection()){
            
            if (getEmailUtenteSelezionato().getId() > 0) {
                NewsLetterDAO.updateEmailUtente(connection, getEmailUtenteSelezionato());
                
            } else {
                int selfieCoin = 0;
                String registeredAt = "";
                SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date endDate = myFormat.parse(Costanti.dayPromoEnd);
                Date currentDate = new Date();
                registeredAt = myFormat.format(currentDate);
                long diff = endDate.getTime() - currentDate.getTime();
                selfieCoin = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) * Costanti.factorySelfieCoin;
                getEmailUtenteSelezionato().setSelfieCoin(selfieCoin);
                getEmailUtenteSelezionato().setRegisteredAt(registeredAt);
                NewsLetterDAO.createEmailUtente(connection, getEmailUtenteSelezionato());
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Qualcosa è andato storto durante l'aggiornamento. Ricontrollare i dati inseriti"));
            return "";
        }

        sendEmail();
        return "elencoEmailUtente";
    }
    
    

    private void sendEmail() {
        String title = "";
        String text = "";
        String sendFrom = "";
        String subject = "";
        try (Connection con = DBConnection.getNewsletterConnection();){
            String query = "SELECT * FROM newsletter_template WHERE id=" + getIdTemplateSelezionato();
            ResultSet resultSet = con.createStatement().executeQuery(query);
            if (resultSet.first()) {
                title = resultSet.getString("title");
                text = resultSet.getString("text");
                sendFrom = resultSet.getString("subject");
                subject = resultSet.getString("send_from");
            }
            resultSet.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Qualcosa è andato storto durante l'aggiornamento. Ricontrollare i dati inseriti"));
            return;
        }

        if (text.contains("#SELFIECOIN")) {
            text = text.replace("#SELFIECOIN", getEmailUtenteSelezionato().getSelfieCoin() + "");
        }
        if (text.contains("#NOMEUTENTE")) {
            text = text.replace("#NOMEUTENTE", getEmailUtenteSelezionato().getName() + "");
        }
        ClassLoader classLoader = getClass().getClassLoader();
        File input = new File(classLoader.getResource("/newsletter/properties/user_acquisition_email.html").getFile());
        StringBuilder result = new StringBuilder();
        try (Scanner scanner = new Scanner(input)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.contains("#TEXT")) {
                    line = line.replace("#TEXT", text);
                }
                if (line.contains("#TITLE")) {
                    line = line.replace("#TITLE", title);
                }
                result.append(line);
            }
            scanner.close();
            Mailin http = new Mailin("https://api.sendinblue.com/v2.0", "wr8g3YFWtKpNcfXT");
            Map< String, Object> to = new HashMap< String, Object>();
            to.put(getEmailUtenteSelezionato().getEmail().trim(), getEmailUtenteSelezionato().getName().trim());
            Map< String, Object> data = new HashMap< String, Object>();
            data.put("to", to);
            data.put("html", result.toString());
            data.put("subject", subject);
            data.put("from", new String[]{"info@justselfie.it", sendFrom});

            String str = http.send_email(data);
            System.out.println(str);
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    
    public void delete(EmailUtente emailUtente) {
        try (Connection connection = DBConnection.getNewsletterConnection()){
            this.elencoEmailUtenti.remove(emailUtente);
            NewsLetterDAO.deleteEmailUtente(connection, emailUtente);
        } catch (SQLException | ClassNotFoundException ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Qualcosa è andato storto durante l'aggiornamento. Ricontrollare i dati inseriti"));

        }
    }

    /**
     * @return the elencoEmailUtenti
     */
    public List<EmailUtente> getElencoEmailUtenti() {
        return elencoEmailUtenti;
    }

    /**
     * @param elencoEmailUtenti the elencoEmailUtenti to set
     */
    public void setElencoEmailUtenti(List<EmailUtente> elencoEmailUtenti) {
        this.elencoEmailUtenti = elencoEmailUtenti;
    }

    /**
     * @return the elencoTemplate
     */
    public List<Template> getElencoTemplate() {
        return elencoTemplate;
    }

    /**
     * @param elencoTemplate the elencoTemplate to set
     */
    public void setElencoTemplate(List<Template> elencoTemplate) {
        this.elencoTemplate = elencoTemplate;
    }

    /**
     * @return the emailUtenteSelezionato
     */
    public EmailUtente getEmailUtenteSelezionato() {
        return emailUtenteSelezionato;
    }

    /**
     * @param emailUtenteSelezionato the emailUtenteSelezionato to set
     */
    public void setEmailUtenteSelezionato(EmailUtente emailUtenteSelezionato) {
        this.emailUtenteSelezionato = emailUtenteSelezionato;
    }

    /**
     * @return the idTemplateSelezionato
     */
    public int getIdTemplateSelezionato() {
        return idTemplateSelezionato;
    }

    /**
     * @param idTemplateSelezionato the idTemplateSelezionato to set
     */
    public void setIdTemplateSelezionato(int idTemplateSelezionato) {
        this.idTemplateSelezionato = idTemplateSelezionato;
    }

    /**
     * @return the elencoTipiRegistrazione
     */
    public List<TipoRegistrazione> getElencoTipiRegistrazione() {
        return elencoTipiRegistrazione;
    }

    /**
     * @param elencoTipiRegistrazione the elencoTipiRegistrazione to set
     */
    public void setElencoTipiRegistrazione(List<TipoRegistrazione> elencoTipiRegistrazione) {
        this.elencoTipiRegistrazione = elencoTipiRegistrazione;
    }

}
