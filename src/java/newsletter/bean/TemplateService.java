/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsletter.bean;

import newsletter.model.Template;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import model.*;
import app.dao.DAOCategorie;
import util.DBConnection;
import util.NewsLetterDAO;

/**
 *
 * @author Davide
 */
@ManagedBean(name = "templateService")
@ViewScoped
public class TemplateService implements Serializable {

    private List<Template> elencoTemplate;
    private Template templateSelezionato;

    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.containsKey("id")) {
            int id = Integer.valueOf(params.get("id"));
            try {
                Connection connection = DBConnection.getNewsletterConnection();
                setTemplateSelezionato(NewsLetterDAO.readTemplate(connection, id));
            } catch (ClassNotFoundException | SQLException ex) {
                System.out.println(ex.getMessage());
                setTemplateSelezionato(new Template());
            }
        } else {
            try {
                Connection connection = DBConnection.getNewsletterConnection();
                setElencoTemplate(NewsLetterDAO.readTemplate(connection));
            } catch (ClassNotFoundException | SQLException ex) {
                System.out.println(ex.getMessage());
                setElencoTemplate(new ArrayList<>());
            }
        }
    }

    public String createOrUpdateIfExist() {
        try {
            Connection connection = DBConnection.getNewsletterConnection();
            if (templateSelezionato.getId() > 0) {
                NewsLetterDAO.updateTemplate(connection, templateSelezionato);
                return "elencoTemplate";
            } else {
                NewsLetterDAO.createTemplate(connection, templateSelezionato);
                return "elencoTemplate";
            }
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Qualcosa è andato storto durante l'aggiornamento. Ricontrollare i dati inseriti"));
            return "";
        }
    }

    /**
     * @return the elencoCategorie
     */
    public List<Template> getElencoTemplate() {
        return elencoTemplate;
    }

    /**
     * @param elencoCategorie the elencoCategorie to set
     */
    public void setElencoTemplate(List<Template> elencoTemplate) {
        this.elencoTemplate = elencoTemplate;
    }

    public void delete(Template template) {
        try {
            this.elencoTemplate.remove(template);
            NewsLetterDAO.deleteTemplate(DBConnection.getNewsletterConnection(), template);
        } catch (SQLException|ClassNotFoundException ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Qualcosa è andato storto durante l'aggiornamento. Ricontrollare i dati inseriti"));

        }
    }

    /**
     * @return the templateSelezionato
     */
    public Template getTemplateSelezionato() {
        return templateSelezionato;
    }

    /**
     * @param templateSelezionato the templateSelezionato to set
     */
    public void setTemplateSelezionato(Template templateSelezionato) {
        this.templateSelezionato = templateSelezionato;
    }

}
