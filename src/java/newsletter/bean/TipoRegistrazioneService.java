/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsletter.bean;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import newsletter.model.EmailUtente;
import newsletter.model.Template;
import newsletter.model.TipoRegistrazione;
import util.Costanti;
import util.DBConnection;
import util.NewsLetterDAO;

/**
 *
 * @author Davide
 */
@ManagedBean(name = "tipoRegistrazioneService")
@ViewScoped
public class TipoRegistrazioneService implements Serializable {

    private List<TipoRegistrazione> elencoTipiRegistrazione;
    private TipoRegistrazione tipoRegistrazioneSelezionato;

    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.containsKey("id")) {
            int id = Integer.valueOf(params.get("id"));
            try {
                Connection connection = DBConnection.getNewsletterConnection();
                setTipoRegistrazioneSelezionato(NewsLetterDAO.readTipoRegistrazione(connection, id));
            } catch (ClassNotFoundException | SQLException ex) {
                System.out.println(ex.getMessage());
                setTipoRegistrazioneSelezionato(new TipoRegistrazione());
            }
        } else {
            try {
                Connection connection = DBConnection.getNewsletterConnection();
                setElencoTipiRegistrazione(NewsLetterDAO.readTipoRegistrazione(connection));
            } catch (ClassNotFoundException | SQLException ex) {
                System.out.println(ex.getMessage());
                setElencoTipiRegistrazione(new ArrayList<>());
            }
        }
    }

    public String createOrUpdateIfExist() {
        try {
            Connection connection = DBConnection.getNewsletterConnection();
            if (getTipoRegistrazioneSelezionato().getId() > 0) {
                NewsLetterDAO.updateTipoRegistrazione(connection, getTipoRegistrazioneSelezionato());
                return "elencoTipoRegistrazione";
            } else {
                NewsLetterDAO.createTipoRegistrazione(connection, getTipoRegistrazioneSelezionato());
                return "elencoTipoRegistrazione";
            }
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Qualcosa è andato storto durante l'aggiornamento. Ricontrollare i dati inseriti"));
            return "";
        }
    }

    public void esportaContatti(TipoRegistrazione tipoRegistrazione) {
        try {
            Connection connection = DBConnection.getNewsletterConnection();
            List<EmailUtente> elencoutenti = NewsLetterDAO.readEmailUtenteFiltrate(connection, tipoRegistrazione.getId());
            File file = new File(Costanti.ESPORTAZIONE_CONTATTI, tipoRegistrazione.getName().replace(" ", "_") + ".csv");
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write("NOME;EMAIL;SELFIECOIN;REGISTRAZIONE\n");
            for (EmailUtente emailUtente : elencoutenti) {
                fileWriter.append(emailUtente.getName().trim() + ";" + emailUtente.getEmail().trim() + ";" + emailUtente.getSelfieCoin() + ";" + emailUtente.getRegisteredAt().split(" ")[0] + "\n");
            }
            fileWriter.flush();
            fileWriter.close();

            FacesContext facesContext = FacesContext.getCurrentInstance();

            HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();

            response.reset();
            response.setHeader("Content-Type", "application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=" + file.getName().replace(" ", "_"));

            OutputStream responseOutputStream = response.getOutputStream();

            InputStream fileInputStream = new FileInputStream(file);

            byte[] bytesBuffer = new byte[2048];
            int bytesRead;
            while ((bytesRead = fileInputStream.read(bytesBuffer)) > 0) {
                responseOutputStream.write(bytesBuffer, 0, bytesRead);
            }

            responseOutputStream.flush();

            fileInputStream.close();
            responseOutputStream.close();

            facesContext.responseComplete();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Errore durante l'esportazione"));

        }
    }

    public void delete(TipoRegistrazione tipoRegistrazione) {
        try {
            this.elencoTipiRegistrazione.remove(tipoRegistrazione);
            NewsLetterDAO.deleteTipoRegistrazione(DBConnection.getNewsletterConnection(), tipoRegistrazione);
        } catch (SQLException | ClassNotFoundException ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Qualcosa è andato storto durante l'aggiornamento. Ricontrollare i dati inseriti"));

        }
    }

    /**
     * @return the elencoTipiRegistrazione
     */
    public List<TipoRegistrazione> getElencoTipiRegistrazione() {
        return elencoTipiRegistrazione;
    }

    /**
     * @param elencoTipiRegistrazione the elencoTipiRegistrazione to set
     */
    public void setElencoTipiRegistrazione(List<TipoRegistrazione> elencoTipiRegistrazione) {
        this.elencoTipiRegistrazione = elencoTipiRegistrazione;
    }

    /**
     * @return the tipoRegistrazioneSelezionato
     */
    public TipoRegistrazione getTipoRegistrazioneSelezionato() {
        return tipoRegistrazioneSelezionato;
    }

    /**
     * @param tipoRegistrazioneSelezionato the tipoRegistrazioneSelezionato to
     * set
     */
    public void setTipoRegistrazioneSelezionato(TipoRegistrazione tipoRegistrazioneSelezionato) {
        this.tipoRegistrazioneSelezionato = tipoRegistrazioneSelezionato;
    }
}
