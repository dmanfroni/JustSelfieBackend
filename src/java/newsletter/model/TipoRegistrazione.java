/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsletter.model;

import java.io.Serializable;

/**
 *
 * @author Davide
 */
public class TipoRegistrazione implements Serializable{
    
    private int id;
    private String name;
    private int totale;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the totale
     */
    public int getTotale() {
        return totale;
    }

    /**
     * @param totale the totale to set
     */
    public void setTotale(int totale) {
        this.totale = totale;
    }
    
}
