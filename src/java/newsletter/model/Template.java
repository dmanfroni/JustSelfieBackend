/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsletter.model;

import java.io.Serializable;

/**
 *
 * @author Davide
 */
public class Template implements Serializable{
    private int id;
    private String title;
    private String text;
    private String subject;
    private String sendFrom;
    private boolean spotUser;
    private boolean spotMerchant;
    
    public Template(){
        
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the spotUser
     */
    public boolean getSpotUser() {
        return spotUser;
    }

    /**
     * @param spotUser the spotUser to set
     */
    public void setSpotUser(boolean spotUser) {
        this.spotUser = spotUser;
    }

    /**
     * @return the spotMerchant
     */
    public boolean getSpotMerchant() {
        return spotMerchant;
    }

    /**
     * @param spotMerchant the spotMerchant to set
     */
    public void setSpotMerchant(boolean spotMerchant) {
        this.spotMerchant = spotMerchant;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the sendFrom
     */
    public String getSendFrom() {
        return sendFrom;
    }

    /**
     * @param sendFrom the sendFrom to set
     */
    public void setSendFrom(String sendFrom) {
        this.sendFrom = sendFrom;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
}
