/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsletter.model;

/**
 *
 * @author Davide
 */
public class EmailUtente {
    private int id;
    private String name;
    private String email;
    private String categories;
    private int selfieCoin;
    private String registeredAt;
    private int registrationType;
    private String registrationTypeString;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the categories
     */
    public String getCategories() {
        return categories;
    }

    /**
     * @param categories the categories to set
     */
    public void setCategories(String categories) {
        this.categories = categories;
    }

    /**
     * @return the selfieCoin
     */
    public int getSelfieCoin() {
        return selfieCoin;
    }

    /**
     * @param selfieCoin the selfieCoin to set
     */
    public void setSelfieCoin(int selfieCoin) {
        this.selfieCoin = selfieCoin;
    }

    /**
     * @return the registeredAt
     */
    public String getRegisteredAt() {
        return registeredAt;
    }

    /**
     * @param registeredAt the registeredAt to set
     */
    public void setRegisteredAt(String registeredAt) {
        this.registeredAt = registeredAt;
    }

    /**
     * @return the registrationType
     */
    public int getRegistrationType() {
        return registrationType;
    }

    /**
     * @param registrationType the registrationType to set
     */
    public void setRegistrationType(int registrationType) {
        this.registrationType = registrationType;
    }

    /**
     * @return the registrationTypeString
     */
    public String getRegistrationTypeString() {
        return registrationTypeString;
    }

    /**
     * @param registrationTypeString the registrationTypeString to set
     */
    public void setRegistrationTypeString(String registrationTypeString) {
        this.registrationTypeString = registrationTypeString;
    }
    
}
