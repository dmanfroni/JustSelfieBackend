/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsletter.api;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import util.Costanti;
import util.DBConnection;
import util.Mailin;

/**
 *
 * @author Davide
 */
@Path("newsletter")
public class NewsletterWS {

    public NewsletterWS() {

    }

    @POST
    @Path("registraUtente")
    public Response registraUtente(
            @DefaultValue("") @FormParam("nome") String name,
            @DefaultValue("") @FormParam("email") String email,
            @DefaultValue("") @FormParam("categorie") String categorie,
            @DefaultValue("") @FormParam("tipo_registrazione") String tipoRegistrazione,
            @DefaultValue("") @FormParam("telefono") String telefono) {

        int selfieCoin = 0;
        String registratoIl = "";
        try {

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date endDate = myFormat.parse(Costanti.dayPromoEnd);
            Date currentDate = new Date();
            registratoIl = myFormat.format(currentDate);
            long diff = endDate.getTime() - currentDate.getTime();
            selfieCoin = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) * Costanti.factorySelfieCoin;

        } catch (Exception ex) {
            return Response.status(Response.Status.OK).entity("Errore durante l'erogazione dei selfie coin").build();
        }
        
        if(selfieCoin < 0 )
            selfieCoin = 0;

        try {

            String query = "INSERT INTO email_utente "
                    + "(nome, email, telefono, categorie, selfie_coin, registrato_il, tipo_registrazione) VALUES (?, ?, ?, ?, ?, ?, ?)";

            Connection con = DBConnection.getNewsletterConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, name.trim());
            ps.setString(2, email.trim());
            ps.setString(3, telefono.trim());
            ps.setString(4, categorie);
            ps.setInt(5, selfieCoin);
            ps.setString(6, registratoIl);
            if (tipoRegistrazione == null || tipoRegistrazione.isEmpty()) {
                tipoRegistrazione = "0";
            }
            ps.setInt(7, Integer.parseInt(tipoRegistrazione));
            ps.executeUpdate();
            ps.close();
            con.close();

        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.OK).entity("Errore durante la registrazione dell'utente").build();
        }

        String title = "";
        String text = "";
        String sendFrom = "";
        String subject = "";
        try {
            String query = "SELECT * FROM newsletter_template WHERE spot_user=1";

            Connection con = DBConnection.getNewsletterConnection();
            ResultSet resultSet = con.createStatement().executeQuery(query);
            if (resultSet.first()) {
                title = resultSet.getString("title");
                text = resultSet.getString("text");
                sendFrom = resultSet.getString("subject");
                subject = resultSet.getString("send_from");
            }
            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.OK).entity("Errore durante la creazione della mail").build();
        }

        if (text.contains("#SELFIECOIN")) {
            text = text.replace("#SELFIECOIN", selfieCoin + "");
        }
        if (text.contains("#NOMEUTENTE")) {
            text = text.replace("#NOMEUTENTE", name + "");
        }
        ClassLoader classLoader = getClass().getClassLoader();
        File input = new File(classLoader.getResource("/newsletter/properties/user_acquisition_email.html").getFile());
        StringBuilder result = new StringBuilder();
        try (Scanner scanner = new Scanner(input)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.contains("#TEXT")) {
                    line = line.replace("#TEXT", text);
                }
                if (line.contains("#TITLE")) {
                    line = line.replace("#TITLE", title);
                }
                result.append(line);
            }
            scanner.close();
            Mailin http = new Mailin("https://api.sendinblue.com/v2.0", "wr8g3YFWtKpNcfXT");
            Map< String, Object> to = new HashMap< String, Object>();
            to.put(email.trim(), name.trim());
            Map< String, Object> data = new HashMap< String, Object>();
            data.put("to", to);
            data.put("html", result.toString());
            data.put("subject", subject);
            data.put("from", new String[]{"info@justselfie.it", sendFrom});

            String str = http.send_email(data);
            System.out.println(str);

            return Response.status(Response.Status.OK).entity("").build();
            //return Response.temporaryRedirect(URI.create("http://justselfie.it/promo?success=true")).build();

        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.OK).entity(ex.getMessage()).build();
        }

    }

    @POST
    @Path("registraEsercente")
    public Response registraEsercente(
            @DefaultValue("") @FormParam("nome") String nome,
            @DefaultValue("") @FormParam("email") String email,
            @DefaultValue("") @FormParam("attivita") String attivita,
            @DefaultValue("") @FormParam("tipo_registrazione") String tipoRegistrazione,
            @DefaultValue("") @FormParam("telefono") String telefono) {
        
        String registratoIl = "";
        try {

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            registratoIl = myFormat.format(new Date());
          } catch (Exception ex) {
            return Response.status(Response.Status.OK).entity("Errore durante l'ottenimento della data").build();
        }

        try {

            String query = "INSERT INTO email_esercente "
                    + "(nome, attivita, email, telefono, registrato_il, tipo_registrazione) VALUES (?, ?, ?, ?, ?, ?)";

            Connection con = DBConnection.getNewsletterConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, nome);
            ps.setString(2, attivita);
            ps.setString(3, email);
            ps.setString(4, telefono);
            ps.setString(5, registratoIl);
            if (tipoRegistrazione == null || tipoRegistrazione.isEmpty()) {
                tipoRegistrazione = "0";
            }
            ps.setInt(6, Integer.parseInt(tipoRegistrazione));
            ps.executeUpdate();
            ps.close();
            con.close();

        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.OK).entity("Errore durante la registrazione dell'esercente").build();
        }

        String title = "";
        String text = "";
        String sendFrom = "";
        String subject = "";
        try {
            String query = "SELECT * FROM newsletter_template WHERE spot_merchant=1";

            Connection con = DBConnection.getNewsletterConnection();
            ResultSet resultSet = con.createStatement().executeQuery(query);
            if (resultSet.first()) {
                title = resultSet.getString("title");
                text = resultSet.getString("text");
                sendFrom = resultSet.getString("subject");
                subject = resultSet.getString("send_from");
            }
            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.OK).entity("Errore durante la creazione della mail").build();
        }

        if (text.contains("#NOMEUTENTE")) {
            text = text.replace("#NOMEUTENTE", nome + "");
        }
        ClassLoader classLoader = getClass().getClassLoader();
        File input = new File(classLoader.getResource("/newsletter/properties/user_acquisition_email.html").getFile());
        StringBuilder result = new StringBuilder();
        try (Scanner scanner = new Scanner(input)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.contains("#TEXT")) {
                    line = line.replace("#TEXT", text);
                }
                if (line.contains("#TITLE")) {
                    line = line.replace("#TITLE", title);
                }
                result.append(line);
            }
            scanner.close();
            Mailin http = new Mailin("https://api.sendinblue.com/v2.0", "wr8g3YFWtKpNcfXT");
            Map< String, Object> to = new HashMap< String, Object>();
            to.put(email, nome);
            Map< String, Object> data = new HashMap< String, Object>();
            data.put("to", to);
            data.put("html", result.toString());
            data.put("subject", subject);
            data.put("from", new String[]{"info@justselfie.it", sendFrom});

            String str = http.send_email(data);
            System.out.println(str);

            return Response.status(Response.Status.OK).entity("").build();
           
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.OK).entity(ex.getMessage()).build();
        }

    }

    @POST
    @Path("registerUser")
    public Response registerUser(
            @DefaultValue("") @FormParam("nome") String name,
            @DefaultValue("") @FormParam("email") String email,
            @DefaultValue("") @FormParam("categorie") String categorie,
            @DefaultValue("") @FormParam("tipo_registrazione") String tipoRegistrazione,
            @DefaultValue("") @FormParam("telefono") String telefono) {

        int selfieCoin = 0;
        String registratoIl = "";
        try {
            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date endDate = myFormat.parse(Costanti.dayPromoEnd);
            Date currentDate = new Date();
            registratoIl = myFormat.format(currentDate);
            long diff = endDate.getTime() - currentDate.getTime();
            selfieCoin = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) * Costanti.factorySelfieCoin;
        } catch (Exception ex) {
            return Response.status(Response.Status.OK).entity("Errore durante l'erogazione dei selfie coin").build();
        }

        try {

            String query = "INSERT INTO email_utente "
                    + "(nome, email, telefono, categorie, selfie_coin, registrato_il, tipo_registrazione) VALUES (?, ?, ?, ?, ?, ?, ?)";

            Connection con = DBConnection.getNewsletterConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, name);
            ps.setString(2, email);
            ps.setString(3, telefono);
            ps.setString(4, categorie);
            ps.setInt(5, selfieCoin);
            ps.setString(6, registratoIl);
            if (tipoRegistrazione == null || tipoRegistrazione.isEmpty()) {
                tipoRegistrazione = "0";
            }
            ps.setInt(7, Integer.parseInt(tipoRegistrazione));
            ps.executeUpdate();
            ps.close();
            con.close();

        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.OK).entity("Errore durante la registrazione dell'utente").build();
        }

        String title = "";
        String text = "";
        String sendFrom = "";
        String subject = "";
        try {
            String query = "SELECT * FROM newsletter_template WHERE spot_user=1";

            Connection con = DBConnection.getNewsletterConnection();
            ResultSet resultSet = con.createStatement().executeQuery(query);
            if (resultSet.first()) {
                title = resultSet.getString("title");
                text = resultSet.getString("text");
                sendFrom = resultSet.getString("subject");
                subject = resultSet.getString("send_from");
            }
            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.OK).entity("Errore durante la creazione della mail").build();
        }

        if (text.contains("#SELFIECOIN")) {
            text = text.replace("#SELFIECOIN", selfieCoin + "");
        }
        if (text.contains("#NOMEUTENTE")) {
            text = text.replace("#NOMEUTENTE", name + "");
        }
        ClassLoader classLoader = getClass().getClassLoader();
        File input = new File(classLoader.getResource("/newsletter/properties/user_acquisition_email.html").getFile());
        StringBuilder result = new StringBuilder();
        try (Scanner scanner = new Scanner(input)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.contains("#TEXT")) {
                    line = line.replace("#TEXT", text);
                }
                if (line.contains("#TITLE")) {
                    line = line.replace("#TITLE", title);
                }
                result.append(line);
            }
            scanner.close();
            Mailin http = new Mailin("https://api.sendinblue.com/v2.0", "wr8g3YFWtKpNcfXT");
            Map< String, Object> to = new HashMap< String, Object>();
            to.put(email, name);
            Map< String, Object> data = new HashMap< String, Object>();
            data.put("to", to);
            data.put("html", result.toString());
            data.put("subject", subject);
            data.put("from", new String[]{"info@justselfie.it", sendFrom});

            String str = http.send_email(data);
            System.out.println(str);

            //return Response.status(Response.Status.OK).entity("").build();
            return Response.temporaryRedirect(URI.create("http://justselfie.it/promo?success=true")).build();

        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.OK).entity("Errore durante l'invio della mail").build();
        }
    }
}
