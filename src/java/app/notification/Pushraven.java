package app.notification;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * This method allows for simple and modular NotificationRaven creation. Notifications can then be pushed to clients
 * over FCM using the push() method.
 * @author Raudius
 *
 */
public class Pushraven {
	private final static String API_URL = "https://fcm.googleapis.com/fcm/send";
	private static String FIREBASE_SERVER_KEY;
	public static NotificationRaven notification;
	
	// static initialization
	static {
		notification = new NotificationRaven();
	}
	
	
	/**
	* Set the API Server Key.
	*/
	public static void setKey(String key){
		FIREBASE_SERVER_KEY = key;
	}
	
	
	/** 
	 * Set new NotificationRaven object
	 */
	public static void setNotification(NotificationRaven notification){
		Pushraven.notification = notification;
	}
	
	
	/**
	 * Messages sent to targets.
	 * This class interfaces with the FCM server by sending the NotificationRaven over HTTP-POST JSON.
	 * @return FcmResponse object containing HTTP response info.
	 */
	public static FcmResponse push(NotificationRaven n) {	
		if(FIREBASE_SERVER_KEY == null){
			System.err.println("No Server-Key has been defined for Pushraven.");
			return null;
		}
		
		HttpsURLConnection con = null;
		try{
			String url = API_URL;
			
			URL obj = new URL(url);
			con = (HttpsURLConnection) obj.openConnection();
	
			con.setRequestMethod("POST");
	
			// Set POST headers
			con.setRequestProperty("Authorization", "key="+FIREBASE_SERVER_KEY);
			con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
	
			
			// Send POST body
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr, "UTF-8"));
                        String json = n.toJSON();

			writer.write(json);
			writer.close();
			wr.close();

			wr.flush();
			wr.close();
			
			con.getResponseCode();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return new FcmResponse(con);
	}


	/**
	 * Messages sent to targets.
	 * This class interfaces with the FCM server by sending the NotificationRaven over HTTP-POST JSON.
	 * @return FcmResponse object containing HTTP response info.
	 */
	public static FcmResponse push() {
		return push(notification);
	}
	
}