/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Davide
 */
public class Selfie implements Serializable{
    
    private int id;
    private Esercente esercente;
    private int idAcquisto;
    private int idSconto;
    private String selfie;
    private String testo;
    private String registratoIl;
    private int tipoCondivisione;
    private Utente utente;
    private List<Utente> amiciTaggatiFB;

    public Selfie(){
        esercente = new Esercente();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getIdAcquisto() {
        return idAcquisto;
    }

    public void setIdAcquisto(int idAcquisto) {
        this.idAcquisto = idAcquisto;
    }

    public int getIdSconto() {
        return idSconto;
    }

    public void setIdSconto(int idSconto) {
        this.idSconto = idSconto;
    }

    public String getSelfie() {
        return selfie;
    }

    public void setSelfie(String selfie) {
        this.selfie = selfie;
    }

    public String getTesto() {
        return testo;
    }

    public void setTesto(String testo) {
        this.testo = testo;
    }

    public String getRegistratoIl() {
        return registratoIl;
    }

    public void setRegistratoIl(String registratoIl) {
        this.registratoIl = registratoIl;
    }

    public int getTipoCondivisione() {
        return tipoCondivisione;
    }

    public void setTipoCondivisione(int tipo_condivisione) {
        this.tipoCondivisione = tipo_condivisione;
    }

    /**
     * @return the utente
     */
    public Utente getUtente() {
        return utente;
    }

    /**
     * @param utente the utente to set
     */
    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    /**
     * @return the utentiTaggati
     */
    public List<Utente> getUtentiTaggati() {
        return amiciTaggatiFB;
    }

    /**
     * @param utentiTaggati the utentiTaggati to set
     */
    public void setUtentiTaggati(List<Utente> utentiTaggati) {
        this.amiciTaggatiFB = utentiTaggati;
    }

    /**
     * @return the esercente
     */
    public Esercente getEsercente() {
        return esercente;
    }

    /**
     * @param esercente the esercente to set
     */
    public void setEsercente(Esercente esercente) {
        this.esercente = esercente;
    }
    
}
