/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

import java.io.Serializable;

/**
 *
 * @author Davide
 */
public class Errore implements Serializable{
    private String messaggio;
    
    public Errore(){
        
    }
    
    public Errore(String messaggio){
        this.messaggio = messaggio;
    }

    /**
     * @return the messaggio
     */
    public String getMessaggio() {
        return messaggio;
    }

    /**
     * @param messaggio the messaggio to set
     */
    public void setMessaggio(String messaggio) {
        this.messaggio = messaggio;
    }
    
}
