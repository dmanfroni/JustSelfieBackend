/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

import java.io.Serializable;

/**
 *
 * @author Davide
 */
public class Offerta implements Serializable{
    
    private int id;
    private String nome;
    private String descrizione;
    private String immagine;
    private double prezzoPieno;
    private double prezzoScontato;
    private int sconto;
    private Esercente esercente;
    
    public Offerta(){
        
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the descrizione
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * @param descrizione the descrizione to set
     */
    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    /**
     * @return the immagine
     */
    public String getImmagine() {
        return immagine;
    }

    /**
     * @param immagine the immagine to set
     */
    public void setImmagine(String immagine) {
        this.immagine = immagine;
    }

    /**
     * @return the prezzoPieno
     */
    public double getPrezzoPieno() {
        return prezzoPieno;
    }

    /**
     * @param prezzoPieno the prezzoPieno to set
     */
    public void setPrezzoPieno(double prezzoPieno) {
        this.prezzoPieno = prezzoPieno;
    }

    /**
     * @return the prezzoScontato
     */
    public double getPrezzoScontato() {
        return prezzoScontato;
    }

    /**
     * @param prezzoScontato the prezzoScontato to set
     */
    public void setPrezzoScontato(double prezzoScontato) {
        this.prezzoScontato = prezzoScontato;
    }

    /**
     * @return the sconto
     */
    public int getSconto() {
        return sconto;
    }

    /**
     * @param sconto the sconto to set
     */
    public void setSconto(int sconto) {
        this.sconto = sconto;
    }

    /**
     * @return the esercente
     */
    public Esercente getEsercente() {
        return esercente;
    }

    /**
     * @param esercente the esercente to set
     */
    public void setEsercente(Esercente esercente) {
        this.esercente = esercente;
    }

  
    
}
