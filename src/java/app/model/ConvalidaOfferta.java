/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Davide
 */
public class ConvalidaOfferta implements Serializable{
    
   
    private Acquisto acquisto;
    private Selfie selfie;
    private String immagineBase64;
    private List<Utente> utentiTagOfferta;
    
    public ConvalidaOfferta(){
        
    }


  
    /**
     * @return the acquisto
     */
    public Acquisto getAcquisto() {
        return acquisto;
    }

    /**
     * @param acquisto the acquisto to set
     */
    public void setAcquisto(Acquisto acquisto) {
        this.acquisto = acquisto;
    }

    /**
     * @return the immagineBase64
     */
    public String getImmagineBase64() {
        return immagineBase64;
    }

    /**
     * @param immagineBase64 the immagineBase64 to set
     */
    public void setImmagineBase64(String immagineBase64) {
        this.immagineBase64 = immagineBase64;
    }

    /**
     * @return the selfie
     */
    public Selfie getSelfie() {
        return selfie;
    }

    /**
     * @param selfie the selfie to set
     */
    public void setSelfie(Selfie selfie) {
        this.selfie = selfie;
    }

    /**
     * @return the utentiTagOfferta
     */
    public List<Utente> getUtentiTagOfferta() {
        return utentiTagOfferta;
    }

    /**
     * @param utentiTagOfferta the utentiTagOfferta to set
     */
    public void setUtentiTagOfferta(List<Utente> utentiTagOfferta) {
        this.utentiTagOfferta = utentiTagOfferta;
    }

  
}
