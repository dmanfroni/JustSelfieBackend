/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

import java.util.List;
import model.DatiFatturazione;

/**
 *
 * @author Davide
 */
public class Esercente {

    /**
     * @return the scontoMinimo
     */
    public int getScontoMinimo() {
        return scontoMinimo;
    }

    /**
     * @param scontoMinimo the scontoMinimo to set
     */
    public void setScontoMinimo(int scontoMinimo) {
        this.scontoMinimo = scontoMinimo;
    }
    
    
    private int id;
    private String nome;
    private String email;
    private String pec;
    private String telefono;
    private String indirizzo;
    private double latitudine;
    private double longitudine;
    private String password;
    private int scontoMinimo;
    
    private Categoria categoria;
    private Contratto contratto;
    private DatiFatturazione datiFatturazione;
    
    private String immagineProfilo;
    private String immagineCopertina;
    private String descrizione;
   
    private String idFacebook;
    private String idInstagram;
    
    private String beaconUUID;
    
    private List<Campagna> campagneBenvenuto;
    
    
    
    private int statoBackend;
    private int statoApp;
    

    public Esercente(){
        contratto = new Contratto();
        categoria = new Categoria();
        datiFatturazione = new DatiFatturazione();
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the indirizzo
     */
    public String getIndirizzo() {
        return indirizzo;
    }

    /**
     * @param indirizzo the indirizzo to set
     */
    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    /**
     * @return the latitudine
     */
    public double getLatitudine() {
        return latitudine;
    }

    /**
     * @param latitudine the latitudine to set
     */
    public void setLatitudine(double latitudine) {
        this.latitudine = latitudine;
    }

    /**
     * @return the longitudine
     */
    public double getLongitudine() {
        return longitudine;
    }

    /**
     * @param longitudine the longitudine to set
     */
    public void setLongitudine(double longitudine) {
        this.longitudine = longitudine;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the categoria
     */
    public Categoria getCategoria() {
        return categoria;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    /**
     * @return the regole
     */
    public Contratto getContratto() {
        return contratto;
    }

    /**
     * @param regole the regole to set
     */
    public void setContratto(Contratto contratto) {
        this.contratto = contratto;
    }

    /**
     * @return the idFacebook
     */
    public String getIdFacebook() {
        return idFacebook;
    }

    /**
     * @param idFacebook the idFacebook to set
     */
    public void setIdFacebook(String idFacebook) {
        this.idFacebook = idFacebook;
    }

    /**
     * @return the idInstagram
     */
    public String getIdInstagram() {
        return idInstagram;
    }

    /**
     * @param idInstagram the idInstagram to set
     */
    public void setIdInstagram(String idInstagram) {
        this.idInstagram = idInstagram;
    }


    /**
     * @return the campagneBenvenuto
     */
    public List<Campagna> getCampagneBenvenuto() {
        return campagneBenvenuto;
    }

    /**
     * @param campagneBenvenuto the campagneBenvenuto to set
     */
    public void setCampagneBenvenuto(List<Campagna> campagneBenvenuto) {
        this.campagneBenvenuto = campagneBenvenuto;
    }

    /**
     * @return the immagineProfilo
     */
    public String getImmagineProfilo() {
        return immagineProfilo;
    }

    /**
     * @param immagineProfilo the immagineProfilo to set
     */
    public void setImmagineProfilo(String immagineProfilo) {
        this.immagineProfilo = immagineProfilo;
    }

    /**
     * @return the immagineCopertina
     */
    public String getImmagineCopertina() {
        return immagineCopertina;
    }

    /**
     * @param immagineCopertina the immagineCopertina to set
     */
    public void setImmagineCopertina(String immagineCopertina) {
        this.immagineCopertina = immagineCopertina;
    }

    /**
     * @return the descrizione
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * @param descrizione the descrizione to set
     */
    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    /**
     * @return the statoBackend
     */
    public int getStatoBackend() {
        return statoBackend;
    }

    /**
     * @param statoBackend the statoBackend to set
     */
    public void setStatoBackend(int statoBackend) {
        this.statoBackend = statoBackend;
    }

    /**
     * @return the statoApp
     */
    public int getStatoApp() {
        return statoApp;
    }

    /**
     * @param statoApp the statoApp to set
     */
    public void setStatoApp(int statoApp) {
        this.statoApp = statoApp;
    }

   

    /**
     * @return the datiFatturazione
     */
    public DatiFatturazione getDatiFatturazione() {
        return datiFatturazione;
    }

    /**
     * @param datiFatturazione the datiFatturazione to set
     */
    public void setDatiFatturazione(DatiFatturazione datiFatturazione) {
        this.datiFatturazione = datiFatturazione;
    }

    /**
     * @return the pec
     */
    public String getPec() {
        return pec;
    }

    /**
     * @param pec the pec to set
     */
    public void setPec(String pec) {
        this.pec = pec;
    }

    /**
     * @return the beaconUUID
     */
    public String getBeaconUUID() {
        return beaconUUID;
    }

    /**
     * @param beaconUUID the beaconUUID to set
     */
    public void setBeaconUUID(String beaconUUID) {
        this.beaconUUID = beaconUUID;
    }
    
    public String getStatoBackendHtml(){
        if(getStatoBackend()== 0){
            return "<i class=\"fa fa-times-circle fa-lg text-danger\" aria-hidden=\"true\"></i>";
        }else if(getStatoBackend() == 1){
            return "<i class=\"fa fa-check-circle fa-lg text-success\" aria-hidden=\"true\"></i>";
        }else{
            return "<i class=\"fa fa-question-circle fa-lg text-warning\" aria-hidden=\"true\"></i>";
        }
    }
    
     public String getStatoAppHtml(){
        if(getStatoApp()== 0){
            return "<i class=\"fa fa-times-circle fa-lg text-danger\" aria-hidden=\"true\"></i>";
        }else if(getStatoApp() == 1){
            return "<i class=\"fa fa-check-circle fa-lg text-success\" aria-hidden=\"true\"></i>";
        }else{
            return "<i class=\"fa fa-question-circle fa-lg text-warning\" aria-hidden=\"true\"></i>";
        }
    }
     
      public String getStatoBeaconHtml(){
        if(getBeaconUUID() == null || getBeaconUUID().equals("0")){
            return "<i class=\"fa fa-times-circle fa-lg text-danger\" aria-hidden=\"true\"></i>";
        }else {
            return "<i class=\"fa fa-check-circle fa-lg text-success\" aria-hidden=\"true\"></i>";
        }
    }

}
