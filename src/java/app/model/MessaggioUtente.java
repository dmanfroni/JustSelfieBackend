/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

import java.io.Serializable;

/**
 *
 * @author Davide
 */
public class MessaggioUtente implements Serializable{
    
    private int id;
    private String messaggio;
    private int livello;
    private String dataMessaggio;

    
    public MessaggioUtente(){
        
    }
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the messaggio
     */
    public String getMessaggio() {
        return messaggio;
    }

    /**
     * @param messaggio the messaggio to set
     */
    public void setMessaggio(String messaggio) {
        this.messaggio = messaggio;
    }

    /**
     * @return the livello
     */
    public int getLivello() {
        return livello;
    }

    /**
     * @param livello the livello to set
     */
    public void setLivello(int livello) {
        this.livello = livello;
    }

    /**
     * @return the dataMessaggio
     */
    public String getDataMessaggio() {
        return dataMessaggio;
    }

    /**
     * @param dataMessaggio the dataMessaggio to set
     */
    public void setDataMessaggio(String dataMessaggio) {
        this.dataMessaggio = dataMessaggio;
    }
    
}
