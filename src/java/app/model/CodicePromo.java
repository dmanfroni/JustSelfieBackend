/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

import java.io.Serializable;

/**
 *
 * @author Davide
 */
public class CodicePromo implements Serializable{
    public int id;
    private String codice;
    private int selfieCoin;
    private String dataInizio;
    private String dataFine;
    
    public CodicePromo(){
        
    }

    /**
     * @return the codice
     */
    public String getCodice() {
        return codice;
    }

    /**
     * @param codice the codice to set
     */
    public void setCodice(String codice) {
        this.codice = codice;
    }

    /**
     * @return the selfieCoin
     */
    public int getId() {
        return id;
    }

    /**
     * @param selfieCoin the selfieCoin to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the dataInizio
     */
    public String getDataInizio() {
        return dataInizio;
    }

    /**
     * @param dataInizio the dataInizio to set
     */
    public void setDataInizio(String dataInizio) {
        this.dataInizio = dataInizio;
    }

    /**
     * @return the dataFine
     */
    public String getDataFine() {
        return dataFine;
    }

    /**
     * @param dataFine the dataFine to set
     */
    public void setDataFine(String dataFine) {
        this.dataFine = dataFine;
    }

    /**
     * @return the id
     */
    public String getSelfieCoin() {
        return selfieCoin + "";
    }

    /**
     * @param id the id to set
     */
    public void setSelfieCoin(String selfieCoin) {
        this.selfieCoin = Integer.parseInt(selfieCoin);
    }
    
}
