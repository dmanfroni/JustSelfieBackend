/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Davide
 */
public class Acquisto implements Serializable{
     private Utente utente;
    private List<AcquistoItem> offerteAcquistate;
    private Campagna campagna;
    private Esercente esercente;
    public int selfieCoinUsati;
    
    public Acquisto(){
        
    }

    /**
     * @return the utente
     */
    public Utente getUtente() {
        return utente;
    }

    /**
     * @param utente the utente to set
     */
    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    /**
     * @return the offerteAcquistate
     */
    public List<AcquistoItem> getOfferteAcquistate() {
        return offerteAcquistate;
    }

    /**
     * @param offerteAcquistate the offerteAcquistate to set
     */
    public void setOfferteAcquistate(List<AcquistoItem> offerteAcquistate) {
        this.offerteAcquistate = offerteAcquistate;
    }

    /**
     * @return the campagna
     */
    public Campagna getCampagna() {
        return campagna;
    }

    /**
     * @param campagna the campagna to set
     */
    public void setCampagna(Campagna campagna) {
        this.campagna = campagna;
    }

    /**
     * @return the esercente
     */
    public Esercente getEsercente() {
        return esercente;
    }

    /**
     * @param esercente the esercente to set
     */
    public void setEsercente(Esercente esercente) {
        this.esercente = esercente;
    }

    /**
     * @return the selfieCoinUsati
     */
    public int getSelfieCoinUsati() {
        return selfieCoinUsati;
    }

    /**
     * @param selfieCoinUsati the selfieCoinUsati to set
     */
    public void setSelfieCoinUsati(int selfieCoinUsati) {
        this.selfieCoinUsati = selfieCoinUsati;
    }
    
   
}
