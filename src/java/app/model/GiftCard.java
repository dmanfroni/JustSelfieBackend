/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

/**
 *
 * @author Davide
 */
public class GiftCard {
    private int idUtente;
    private String beaconUUID;
    private String valoreEuro;
    private String valoreSelfieCoin;
    
    public GiftCard(){
        
    }

    /**
     * @return the idUtente
     */
    public int getIdUtente() {
        return idUtente;
    }

    /**
     * @param idUtente the idUtente to set
     */
    public void setIdUtente(int idUtente) {
        this.idUtente = idUtente;
    }

    /**
     * @return the beaconUUID
     */
    public String getBeaconUUID() {
        return beaconUUID;
    }

    /**
     * @param beaconUUID the beaconUUID to set
     */
    public void setBeaconUUID(String beaconUUID) {
        this.beaconUUID = beaconUUID;
    }

    /**
     * @return the valoreEuro
     */
    public String getValoreEuro() {
        return valoreEuro;
    }

    /**
     * @param valoreEuro the valoreEuro to set
     */
    public void setValoreEuro(String valoreEuro) {
        this.valoreEuro = valoreEuro;
    }

    /**
     * @return the valoreSelfieCoin
     */
    public String getValoreSelfieCoin() {
        return valoreSelfieCoin;
    }

    /**
     * @param valoreSelfieCoin the valoreSelfieCoin to set
     */
    public void setValoreSelfieCoin(String valoreSelfieCoin) {
        this.valoreSelfieCoin = valoreSelfieCoin;
    }
    
}
