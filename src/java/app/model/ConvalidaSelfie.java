/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

/**
 *
 * @author Davide
 */
public class ConvalidaSelfie {
     private Selfie selfie;
    private String immagineBase64;
    
    public ConvalidaSelfie(){
        
    }

    /**
     * @return the selfie
     */
    public Selfie getSelfie() {
        return selfie;
    }

    /**
     * @param selfie the selfie to set
     */
    public void setSelfie(Selfie selfie) {
        this.selfie = selfie;
    }

    /**
     * @return the immagineBase64
     */
    public String getImmagineBase64() {
        return immagineBase64;
    }

    /**
     * @param immagineBase64 the immagineBase64 to set
     */
    public void setImmagineBase64(String immagineBase64) {
        this.immagineBase64 = immagineBase64;
    }
    
}
