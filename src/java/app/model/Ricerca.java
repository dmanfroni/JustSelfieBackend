/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

import java.io.Serializable;

/**
 *
 * @author Davide
 */
public class Ricerca implements Serializable{
    
    private String ricercaTesto;
    private int idCategoria;
    private int idUtente;
    private int idEsercente;
    private int idSelfie;
    
    public Ricerca(){
        
    }

    /**
     * @return the ricercaTesto
     */
    public String getRicercaTesto() {
        return ricercaTesto;
    }

    /**
     * @param ricercaTesto the ricercaTesto to set
     */
    public void setRicercaTesto(String ricercaTesto) {
        this.ricercaTesto = ricercaTesto;
    }

    /**
     * @return the idCategoria
     */
    public int getIdCategoria() {
        return idCategoria;
    }

    /**
     * @param idCategoria the idCategoria to set
     */
    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

   
    /**
     * @return the idUtente
     */
    public int getIdUtente() {
        return idUtente;
    }

    /**
     * @param idUtente the idUtente to set
     */
    public void setIdUtente(int idUtente) {
        this.idUtente = idUtente;
    }

    /**
     * @return the idEsercente
     */
    public int getIdEsercente() {
        return idEsercente;
    }

    /**
     * @param idEsercente the idEsercente to set
     */
    public void setIdEsercente(int idEsercente) {
        this.idEsercente = idEsercente;
    }

    /**
     * @return the idSelfie
     */
    public int getIdSelfie() {
        return idSelfie;
    }

    /**
     * @param idSelfie the idSelfie to set
     */
    public void setIdSelfie(int idSelfie) {
        this.idSelfie = idSelfie;
    }
}
