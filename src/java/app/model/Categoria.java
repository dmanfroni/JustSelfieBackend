/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Davide
 */
public class Categoria implements Serializable{
    private int id;
    private String nome;
    private String icona;
    public String colore;
    
    public Categoria(){
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        
        this.nome = nome;
    }

    /**
     * @return the icona
     */
    public String getIcona() {
        return icona;
    }

    /**
     * @param icona the icona to set
     */
    public void setIcona(String icona) {
        this.icona = icona;
    }

    /**
     * @return the colore
     */
    public String getColore() {
        return colore;
    }

    /**
     * @param colore the colore to set
     */
    public void setColore(String colore) {
        this.colore = colore;
    }


   
}
