/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.api;

import app.dao.DAOCodicePromo;
import app.dao.DAOUtente;
import app.model.Utente;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.primefaces.json.JSONObject;
import util.DBConnection;

/**
 *
 * @author Davide
 */
@Path("utente")
public class UtenteWS {

    @POST
    @Path("autenticazioneJS")
    @Produces("application/json")
    @Consumes("application/json")
    public Response autenticazioneJS(InputStream requestBody) {

        Object object = new Object();
        boolean success = true;

        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(requestBody));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            Utente utente = new Gson().fromJson(out.toString(), Utente.class);
            object = DAOUtente.autenticazioneJS(connection, utente);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }
        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", object);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();
    }

    @POST
    @Path("autenticazioneFB")
    @Produces("application/json")
    @Consumes("application/json")
    public Response autenticazioneFB(InputStream requestBody) {

        /*
        *TODO se l'account fb è gia esistente ma è associato ad un altra mail è un anomalia. da gestire
         */
        Object object = new Object();
        boolean success = true;
        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(requestBody));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            Utente utente = new Gson().fromJson(out.toString(), Utente.class);
            object = DAOUtente.autenticazioneFB(connection, utente);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }

        /**
         * Solo per beta user newsletter, da rimuovere prima o poi
         */
        if (object instanceof HashMap) {
            try {
                int idUtente = ((Utente) ((HashMap<String, Object>) object).get("utente")).getId();
                String email = ((Utente) ((HashMap<String, Object>) object).get("utente")).getEmail();
                int oldSelfieCoin = ((Utente) ((HashMap<String, Object>) object).get("utente")).getSelfieCoin();
                int nuovoUtente = (Integer) ((HashMap<String, Object>) object).get("nuovoUtente");
                if (nuovoUtente == 1 && idUtente > 0 && (email != null && email.length() > 0)) {
                    Connection connection = DBConnection.getNewsletterConnection();
                    String query = "SELECT * FROM email_utente WHERE email=?";
                    PreparedStatement preparedStatement = connection.prepareStatement(query);
                    preparedStatement.setString(1, email);
                    ResultSet resultSet = preparedStatement.executeQuery();
                    int selfieCoin = 0;
                    while (resultSet.next()) {
                        selfieCoin = resultSet.getInt("selfie_coin");
                    }
                    connection.close();
                    if (selfieCoin > 0) {
                        oldSelfieCoin = oldSelfieCoin + selfieCoin;
                        connection = DBConnection.getJustSelfieConnection();
                        query = "UPDATE utente SET selfie_coin = " + oldSelfieCoin + " WHERE id=" + idUtente;
                        connection.createStatement().executeUpdate(query);
                        query = "INSERT INTO utente_messaggi_selfiecoin (id_utente, messaggio, livello, quantita, data_messaggio)"
                                + " VALUES (?, ?, ?, ?, ?)";
                        preparedStatement = connection.prepareStatement(query);
                        preparedStatement.setInt(1, idUtente);
                        preparedStatement.setString(2, "Hai ricevuto " + selfieCoin + " selfie coin per esserti iscritto alla newsletter!");
                        preparedStatement.setInt(3, 1);
                        preparedStatement.setInt(4, selfieCoin);
                        preparedStatement.setString(5, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                        preparedStatement.executeUpdate();
                    }
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                success = false;
                object = ex.getMessage();
            }
        }

        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", object);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();
    }

    @POST
    @Path("collegaFB")
    @Produces("application/json")
    @Consumes("application/json")
    public Response collegaFB(InputStream requestBody) {

        /*
        *TODO se l'account fb è gia esistente ma è associato ad un altra mail è un anomalia. da gestire
         */
        Object object = new Object();
        boolean success = true;
        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(requestBody));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            Utente utente = new Gson().fromJson(out.toString(), Utente.class);
            object = DAOUtente.collegaFB(connection, utente);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }

        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", object);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();
    }

    @POST
    @Path("registrazioneJS")
    @Produces("application/json")
    @Consumes("application/json")
    public Response registrazioneJS(InputStream requestBody) {

        Object object = new Object();
        boolean success = true;

        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(requestBody));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            Utente utente = new Gson().fromJson(out.toString(), Utente.class);
            object = DAOUtente.registrazioneJS(connection, utente);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }
        
        if (object instanceof HashMap) {
            try {
                int idUtente = ((Utente) ((HashMap<String, Object>) object).get("utente")).getId();
                String email = ((Utente) ((HashMap<String, Object>) object).get("utente")).getEmail();
                int oldSelfieCoin = ((Utente) ((HashMap<String, Object>) object).get("utente")).getSelfieCoin();
                int nuovoUtente = (Integer) ((HashMap<String, Object>) object).get("nuovoUtente");
                if (nuovoUtente == 1 && idUtente > 0 && (email != null && email.length() > 0)) {
                    Connection connection = DBConnection.getNewsletterConnection();
                    String query = "SELECT * FROM email_utente WHERE email=?";
                    PreparedStatement preparedStatement = connection.prepareStatement(query);
                    preparedStatement.setString(1, email);
                    ResultSet resultSet = preparedStatement.executeQuery();
                    int selfieCoin = 0;
                    while (resultSet.next()) {
                        selfieCoin = resultSet.getInt("selfie_coin");
                    }
                    connection.close();
                    if (selfieCoin > 0) {
                        oldSelfieCoin = oldSelfieCoin + selfieCoin;
                        connection = DBConnection.getJustSelfieConnection();
                        query = "UPDATE utente SET selfie_coin = " + oldSelfieCoin + " WHERE id=" + idUtente;
                        connection.createStatement().executeUpdate(query);
                        query = "INSERT INTO utente_messaggi_selfiecoin (id_utente, messaggio, livello, quantita, data_messaggio)"
                                + " VALUES (?, ?, ?, ?, ?)";
                        preparedStatement = connection.prepareStatement(query);
                        preparedStatement.setInt(1, idUtente);
                        preparedStatement.setString(2, "Hai ricevuto " + selfieCoin + " selfie coin per esserti iscritto alla newsletter!");
                        preparedStatement.setInt(3, 1);
                        preparedStatement.setInt(4, selfieCoin);
                        preparedStatement.setString(5, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                        preparedStatement.executeUpdate();
                    }
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                success = false;
                object = ex.getMessage();
            }
        }

        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", object);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();
    }

    @POST
    @Path("aggiorna")
    @Produces("application/json")
    @Consumes("application/json")
    public Response aggiorna(InputStream requestBody) {

        Object object = new Object();
        boolean success = true;

        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(requestBody));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            Utente utente = new Gson().fromJson(out.toString(), Utente.class);
            object = DAOUtente.aggiorna(connection, utente);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }

        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", object);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();
    }

    @POST
    @Path("scheda")
    @Produces("application/json")
    @Consumes("application/json")
    public Response scheda(InputStream requestBody) {
        Object object = new Object();
        boolean success = true;

        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(requestBody));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            Utente utente = new Gson().fromJson(out.toString(), Utente.class);
            object = DAOUtente.scheda(connection, utente);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }

        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", object);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();
    }

    @POST
    @Path("checkEmail")
    @Produces("application/json")
    public Response checkEmail(@DefaultValue("") @FormParam("userIdFB") String userIdFB) {
        Object object = new Object();
        boolean success = true;

        try (Connection connection = DBConnection.getJustSelfieConnection()) {

            String query = "SELECT email FROM utente WHERE fb_user_id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, Long.parseLong(userIdFB));
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                object = resultSet.getString("email");
            } else {
                object = "";
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }

        HashMap<String, Object> response = new HashMap<>();
        HashMap<String, Object> key = new HashMap<>();
        key.put("email", object);
        response.put("success", success);
        response.put("response", key);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();
    }

    @POST
    @Path("registraDevice")
    @Produces("application/json")
    public Response registraDevice(
            @DefaultValue("") @FormParam("idUtente") String idUtente,
            @DefaultValue("") @FormParam("idDevice") String idDevice,
            @DefaultValue("") @FormParam("token") String token,
            @DefaultValue("") @FormParam("tipoDispositivo") String tipoDispositivo) {
        Object object = new Object();
        boolean success = true;

        try (Connection connection = DBConnection.getJustSelfieConnection()) {

            object = DAOUtente.registraDevice(connection, Integer.parseInt(idUtente), idDevice, token, Integer.parseInt(tipoDispositivo));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }

        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", object);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();
    }

    @POST
    @Path("score")
    @Produces("application/json")
    @Consumes("application/json")
    public Response score(InputStream requestBody) {

        Object object = new Object();
        boolean success = true;

        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(requestBody));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            Utente utente = new Gson().fromJson(out.toString(), Utente.class);
            object = DAOUtente.score(connection, utente);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }
        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", object);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();
    }
    
    @POST
    @Path("codicePromo")
    @Produces("application/json")
    @Consumes("application/json")
    public Response codicePromo(InputStream requestBody) {

        Object object = new Object();
        boolean success = true;

        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(requestBody));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            JSONObject jsonObject = new JSONObject(out.toString());
            object = DAOCodicePromo.codicePromo(connection, jsonObject.getInt("idUtente"), jsonObject.getString("codicePromo"));

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }
        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", object);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();
    }

    @POST
    @Path("checkEmailJson")
    @Produces("application/json")
    @Consumes("application/json")
    public Response checkEmail(InputStream requestBody) {

        Object object = new Object();
        boolean success = true;

        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(requestBody));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            Utente utente = new Gson().fromJson(out.toString(), Utente.class);
            String query = "SELECT email FROM utente WHERE fb_user_id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, utente.getUserIdFB());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                object = resultSet.getString("email");
            } else {
                object = "";
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }

        HashMap<String, Object> response = new HashMap<>();
        HashMap<String, Object> key = new HashMap<>();
        key.put("email", object);
        response.put("success", success);
        response.put("response", key);
        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();
    }

    @POST
    @Path("registraDeviceJson")
    @Produces("application/json")
    @Consumes("application/json")
    public Response registraDevice(InputStream requestBody) {
        Object object = new Object();
        boolean success = true;

        try (Connection connection = DBConnection.getJustSelfieConnection()) {

            BufferedReader reader = new BufferedReader(new InputStreamReader(requestBody));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            JSONObject jsonObject = new JSONObject(out.toString());
            object = DAOUtente.registraDevice(
                    connection,
                    jsonObject.getInt("idUtente"),
                    jsonObject.getString("idDevice"),
                    jsonObject.getString("token"),
                    jsonObject.getInt("tipoDispositivo"));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }

        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", object);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();
    }
}
