/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.api;

import app.dao.DAOEsercente;
import app.dao.DAOSelfie;
import app.dao.DAOWall;
import app.model.Ricerca;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import util.DBConnection;

/**
 *
 * @author Davide
 */
@Path("wall")
public class WallWS {
    
     @POST
    @Path("lista")
    @Produces("application/json")
    @Consumes("application/json")
    public Response lista(InputStream requestBody) {

        Object object = new Object();
        boolean success = true;
        try (Connection connection = DBConnection.getJustSelfieConnection()) {
             object = DAOWall.readWallFiltratoUtente(connection);
        }  catch (Exception ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }
        //
        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", object);
        
        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();
    }

    @POST
    @Path("scheda")
    @Produces("application/json")
    @Consumes("application/json")
    public Response scheda(InputStream requestBody)  {

        Object object = new Object();
        boolean success = true;
        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(requestBody));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            Ricerca ricerca = new Gson().fromJson(out.toString(), Ricerca.class);
            if(ricerca == null)
                throw new ClassNotFoundException("Errore");
            object = DAOWall.readSelfie(connection, ricerca.getIdSelfie());
        } catch (IOException | SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }
        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", object);
        
        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();
    }
    
}
