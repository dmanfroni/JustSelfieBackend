/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.api;

import app.dao.DAOConvalida;
import app.model.ConvalidaGiftCard;
import app.model.ConvalidaOfferta;
import app.model.ConvalidaSconto;
import app.model.ConvalidaSelfie;
import app.model.Utente;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import org.primefaces.json.JSONObject;
import util.DBConnection;

/**
 *
 * @author Davide
 */
@Path("convalida")
public class ConvalidaWS {

    @POST
    @Consumes("application/json")
    @Path("offerta")
    public Response offerta(InputStream requestBody) {

        Object object = new Object();
        boolean success = true;
        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(requestBody));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
             System.out.println(out.toString());
            ConvalidaOfferta convalidaOfferta = new Gson().fromJson(out.toString(), ConvalidaOfferta.class);
            object = DAOConvalida.convalidaOfferta(connection, convalidaOfferta);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }
        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", object);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();

    }

    @POST
    @Path("sconto")
    public Response sconto(InputStream requestBody) {
        Object object = new Object();
        boolean success = true;
        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(requestBody));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
             System.out.println(out.toString());
            ConvalidaSconto convalidaSconto = new Gson().fromJson(out.toString(), ConvalidaSconto.class);
            object = DAOConvalida.convalidaSconto(connection, convalidaSconto);

        } catch (IOException | SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }
        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", object);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();

    }

    @POST
    @Path("selfie")
    public Response selfie(InputStream requestBody) {
        Object object = new Object();
        boolean success = true;
        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(requestBody));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
             System.out.println(out.toString());
            ConvalidaSelfie convalidaSelfie = new Gson().fromJson(out.toString(), ConvalidaSelfie.class);
            object = DAOConvalida.convalidaSelfie(connection, convalidaSelfie);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }
        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", object);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();

    }

    @POST
    @Path("giftcard")
    public Response giftcard(InputStream requestBody) {
        Object object = new Object();
        boolean success = true;
        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(requestBody));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
             System.out.println(out.toString());
            ConvalidaGiftCard convalidaGiftCard = new Gson().fromJson(out.toString(), ConvalidaGiftCard.class);
            object = DAOConvalida.convalidaGiftCard(connection, convalidaGiftCard);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }
        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", object);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();

    }
    
    @POST
    @Path("checkTag")
    public Response checkTag(InputStream requestBody) {
        Object object = new Object();
        boolean success = true;
         List<Utente> amiciRifiutati = new ArrayList<>();
        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(requestBody));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            JSONObject jsonObject = new JSONObject(out.toString());
            List<Utente> amiciTaggati = new Gson().fromJson(jsonObject.getJSONArray("utentiTaggati").toString(), new TypeToken<List<Utente>>(){}.getType());
            int idCampagna = jsonObject.getInt("idCampagna");
           
            for(int i  = 0; i < amiciTaggati.size(); i++){
                String query = "SELECT * FROM selfie WHERE id_utente=? AND id_campagna=?";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setInt(1, amiciTaggati.get(i).getId());
                preparedStatement.setInt(2, idCampagna);
                ResultSet resultSet = preparedStatement.executeQuery();
                while(resultSet.next()){
                    amiciRifiutati.add(amiciTaggati.get(i));
                }
            }
            
            object = amiciRifiutati;
            
        } catch (IOException | SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }
        HashMap<String, Object> amiciRejected = new HashMap<String, Object>();
        amiciRejected.put("amiciRifiutati", amiciRifiutati);
        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", amiciRejected);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();

    }

}
