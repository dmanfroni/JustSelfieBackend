/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.api;

import app.dao.DAOUtente;
import app.dao.DAOWom;
import app.model.Ricerca;
import app.model.Utente;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import util.DBConnection;
import util.NotificationManager;

/**
 *
 * @author Davide
 */
@Path("wom")
public class WomWS {
    
    @POST
    @Path("registra")
    @Consumes("application/json")
    @Produces("application/json")
    public Response registra(InputStream body){
         Object object = new Object();
        boolean success = true;

        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(body));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            Ricerca ricerca = new Gson().fromJson(out.toString(), Ricerca.class);
            object = DAOWom.registra(connection, ricerca.getIdUtente(), ricerca.getIdSelfie());

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            success = false;
            object = ex.getMessage();
        }
        HashMap<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("response", object);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(response)).build();
    }
    
    /*
    @POST
    @Path("registra")
    public Response registra(
    @DefaultValue("-1") @FormParam("idEsercente") int idEsercente,
    @DefaultValue("-1") @FormParam("idUtenteA") int idUtenteA,
    @DefaultValue("-1") @FormParam("idUtenteB") int idUtenteB){
        return Response.status(Response.Status.OK).entity("").build();
    }
    
    @POST
    @Path("conferma")
    public Response conferma(
    @DefaultValue("-1") @FormParam("idEsercente") int idEsercente,
    @DefaultValue("-1") @FormParam("idUtenteA") int idUtenteA,
    @DefaultValue("-1") @FormParam("idUtenteB") int idUtenteB){
        return Response.status(Response.Status.OK).entity("").build();
    }*/
    
}
