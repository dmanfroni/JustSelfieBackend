/**
 *
 * @author Davide
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import app.model.Contratto;
import model.ScaglioneSelfie;

/**
 *
 * @author Davide
 */
public class DAOContratti {

    public static List<Contratto> readContratto(Connection connection) throws SQLException {
        List<Contratto> elencoContratti = new ArrayList<>();
        String query = "SELECT * FROM contratto WHERE 1=1";
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        while (resultSet.next()) {
            Contratto contratto = new Contratto();
            contratto.setId(resultSet.getInt("id"));
            contratto.setNome(resultSet.getString("nome"));
            elencoContratti.add(contratto);
        }
        return elencoContratti;
    }

    public static Contratto readContratto(Connection connection, int id) throws SQLException {
        Contratto contratto = new Contratto();
        String query = "SELECT * FROM contratto WHERE id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.first()) {
            contratto.setId(resultSet.getInt("id"));
            contratto.setNome(resultSet.getString("nome"));
            contratto.setOffertaScontoMinimo(resultSet.getInt("offerta_sconto_minimo"));
            contratto.setOffertaSelfieCoin(resultSet.getInt("offerta_selfie_coin"));
            
            contratto.setScontoScontrinoMinimo(resultSet.getInt("sconto_scontrino_minimo"));
            contratto.setScontoScontrinoRiduzioneNoLink(resultSet.getInt("sconto_scontrino_riduzione_nolink"));
            contratto.setScontroScontrinoValidita(resultSet.getInt("sconto_scontrino_validita"));
            
            contratto.setSelfieCoin(resultSet.getInt("selfie_coin"));
            contratto.setSelfieCoinRiduzioneNoLink(resultSet.getInt("selfie_coin_riduzione_nolink"));
            
            contratto.setWomValidita(resultSet.getInt("wom_validita"));
            contratto.setWomSelfieCoinUtenteA(resultSet.getInt("wom_selfie_coin_a"));
            contratto.setWomSelfieCoinUtenteB(resultSet.getInt("wom_selfie_coin_b"));
            
            
            contratto.setNumeroSelfiePromo(resultSet.getInt("numero_selfie_promo"));
            contratto.setNumeroOffertePromo(resultSet.getInt("numero_offerte_promo"));
            
            contratto.setProvvigioneSelfie(resultSet.getDouble("provvigione_selfie"));
            contratto.setProvvigioneOfferta(resultSet.getDouble("provvigione_offerta"));
            contratto.setProvvigioneWom(resultSet.getDouble("provvigione_wom"));
            
            contratto.setProvvigioneSelfiePromo(resultSet.getDouble("provvigione_selfie_promo"));
            contratto.setProvvigioneOffertaPromo(resultSet.getDouble("provvigione_offerta_promo"));
        }
        resultSet.close();
        preparedStatement.close();

        return contratto;
    }

    public static void createContratto(Connection connection, Contratto contratto) throws SQLException {
        String query = "INSERT INTO contratto "
                + "(nome, offerta_sconto_minimo, offerta_selfie_coin, sconto_scontrino_minimo, sconto_scontrino_riduzione_nolink, sconto_scontrino_validita, "
                + "selfie_coin, selfie_coin_riduzione_nolink, wom_validita, wom_selfie_coin_a, wom_selfie_coin_b, "
                + "numero_selfie_promo, numero_offerte_promo, provvigione_selfie, provvigione_offerta, provvigione_wom, "
                + "provvigione_offerta_promo, provvigione_selfie_promo) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";

        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, contratto.getNome());
        preparedStatement.setInt(2, contratto.getOffertaScontoMinimo());
        preparedStatement.setInt(3, contratto.getOffertaSelfieCoin());
        preparedStatement.setInt(4, contratto.getScontoScontrinoMinimo());
        preparedStatement.setInt(5, contratto.getScontoScontrinoRiduzioneNoLink());
        preparedStatement.setInt(6, contratto.getScontroScontrinoValidita());
        preparedStatement.setInt(7, contratto.getSelfieCoin());
        preparedStatement.setInt(8, contratto.getSelfieCoinRiduzioneNoLink());
        preparedStatement.setInt(9, contratto.getWomValidita());
        preparedStatement.setInt(10, contratto.getWomSelfieCoinUtenteA());
        preparedStatement.setInt(11, contratto.getWomSelfieCoinUtenteB());
        preparedStatement.setInt(12, contratto.getNumeroSelfiePromo());
        preparedStatement.setInt(13, contratto.getNumeroOffertePromo());
        preparedStatement.setDouble(14, contratto.getProvvigioneSelfie());
        preparedStatement.setDouble(15, contratto.getProvvigioneOfferta());
        preparedStatement.setDouble(16, contratto.getProvvigioneWom());
        preparedStatement.setDouble(17, contratto.getProvvigioneOffertaPromo());
        preparedStatement.setDouble(18, contratto.getProvvigioneSelfiePromo());
        preparedStatement.executeUpdate();

       
        preparedStatement.close();

    }

    public static void updateContratto(Connection connection, Contratto contratto) throws SQLException {
        String query = "UPDATE contratto SET "
                + "nome=?, offerta_sconto_minimo=?, offerta_selfie_coin=?, sconto_scontrino_minimo=?, sconto_scontrino_riduzione_nolink=?, sconto_scontrino_validita=?, "
                + "selfie_coin=?, selfie_coin_riduzione_nolink=?, wom_validita=?, wom_selfie_coin_a=?, wom_selfie_coin_b=?, "
                + "numero_selfie_promo=?, numero_offerte_promo=?, provvigione_selfie=?, provvigione_offerta=?, provvigione_wom=?, "
                + "provvigione_offerta_promo=?, provvigione_selfie_promo=? WHERE id=?";

        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, contratto.getNome());
        preparedStatement.setInt(2, contratto.getOffertaScontoMinimo());
        preparedStatement.setInt(3, contratto.getOffertaSelfieCoin());
        preparedStatement.setInt(4, contratto.getScontoScontrinoMinimo());
        preparedStatement.setInt(5, contratto.getScontoScontrinoRiduzioneNoLink());
        preparedStatement.setInt(6, contratto.getScontroScontrinoValidita());
        preparedStatement.setInt(7, contratto.getSelfieCoin());
        preparedStatement.setInt(8, contratto.getSelfieCoinRiduzioneNoLink());
        preparedStatement.setInt(9, contratto.getWomValidita());
        preparedStatement.setInt(10, contratto.getWomSelfieCoinUtenteA());
        preparedStatement.setInt(11, contratto.getWomSelfieCoinUtenteB());
        preparedStatement.setInt(12, contratto.getNumeroSelfiePromo());
        preparedStatement.setInt(13, contratto.getNumeroOffertePromo());
        preparedStatement.setDouble(14, contratto.getProvvigioneSelfie());
        preparedStatement.setDouble(15, contratto.getProvvigioneOfferta());
        preparedStatement.setDouble(16, contratto.getProvvigioneWom());
        preparedStatement.setDouble(17, contratto.getProvvigioneOffertaPromo());
        preparedStatement.setDouble(18, contratto.getProvvigioneSelfiePromo());
        
        preparedStatement.setInt(19, contratto.getId());
        preparedStatement.executeUpdate();

       
        preparedStatement.close();

    }

    public static void deleteContratto(Connection connection, Contratto contratto) throws SQLException {
        String query = "DELETE FROM contratto WHERE id=? ";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, contratto.getId());
        preparedStatement.executeUpdate();
        
    }

}
