/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author Davide
 */
public class DAOWom {

    public static Object registra(Connection connection, int idUtenteB, int idSelfie) throws SQLException {
         HashMap<String, Object> registrato = new HashMap<>();
        /**
         * - Controllo che l'utente B non sia l'utente creatore o uno di quelli
         * taggati 
         * - Controllo che non ci sia un azione gia registrata tra
         * utente e post 
         * - Registro azione
         */
        String query = "SELECT * FROM selfie WHERE (id=? OR id_selfie=?) AND id_utente=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, idSelfie);
        preparedStatement.setInt(2, idSelfie);
        preparedStatement.setInt(3, idUtenteB);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            registrato.put("idEsercente", resultSet.getInt("id_esercente"));
            registrato.put("registrato", false);
            return registrato;
        }

        query = "SELECT * FROM wom WHERE id_selfie=? AND id_utente_b=?";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, idSelfie);
        preparedStatement.setInt(2, idUtenteB);
        resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            registrato.put("idEsercente", resultSet.getInt("id_esercente"));
            registrato.put("registrato", false);
            return registrato;
        }

        query = "SELECT selfie.*, contratto.wom_validita FROM selfie INNER JOIN esercente ON selfie.id_esercente = esercente.id INNER JOIN contratto ON contratto.id = esercente.id_contratto WHERE selfie.id=?";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, idSelfie);
        resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            int idEsercente = resultSet.getInt("id_esercente");
            int idUtenteA = resultSet.getInt("id_utente");

            int durataWom = resultSet.getInt("wom_validita");
            Date oggi = new Date();
            String dataWom = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(oggi);
            LocalDateTime localDateTime = oggi.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            localDateTime = localDateTime.plusHours(durataWom);
            Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
            String dataLimite = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentDatePlusOneDay);
            
            query = "INSERT INTO wom (id_esercente, id_selfie, id_utente_a, id_utente_b, data_wom, data_limite)"
                    + " VALUES (?, ?, ?, ?, ?, ?)";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, idEsercente);
            preparedStatement.setInt(2, idSelfie);
            preparedStatement.setInt(3, idUtenteA);
            preparedStatement.setInt(4, idUtenteB);
            preparedStatement.setString(5, dataWom);
            preparedStatement.setString(6, dataLimite);
            preparedStatement.executeUpdate();
            registrato.put("idEsercente", idEsercente);
            registrato.put("registrato", true);
            return registrato;
        }
        return registrato;
    }

}
