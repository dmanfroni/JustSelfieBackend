/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.dao;

import app.model.Esercente;
import app.model.MessaggioUtente;
import app.model.Sconto;
import app.model.Selfie;
import app.model.Utente;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import util.Costanti;

/**
 *
 * @author Davide
 */
public class DAOUtente {

    public static Object registrazioneJS(Connection connection, Utente utente) throws SQLException {
        int auth = 0;
        int idJS = 0;
        int nuovoUtente = 0;

        String query = "SELECT id FROM utente WHERE email=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, utente.getEmail());
        ResultSet resultSet = preparedStatement.executeQuery();
        if (!resultSet.next()) {
            String dataRegistrazione = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            query = "INSERT INTO utente "
                    + "(nome, email, password, selfie_coin, affidabilita, registrato_il) "
                    + "VALUES (?, ?, ?, 500, 100.00, ?)";
            preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, utente.getNome());
            preparedStatement.setString(2, utente.getEmail());
            preparedStatement.setString(3, utente.getPassword());
            preparedStatement.setString(4, dataRegistrazione);
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.first()) {
                idJS = generatedKeys.getInt(1);
            }
            preparedStatement.close();

            if (Costanti.produzione) {
                try {
                    URL obj = new URL("http://www.justselfie.it/my_mc_connection.php");
                    String urlParameters = "email=" + utente.getEmail() + "&name=" + utente.getNome();
                    byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("POST");
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                        wr.write(postData);
                    }

                    int g = con.getResponseCode();
                    try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                        String inputLine;
                        while ((inputLine = in.readLine()) != null) {
                            System.out.println(inputLine);
                        }
                    }
                    System.out.println(g + "");
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
            nuovoUtente = 1;
            auth = 1;

        }

        HashMap<String, Object> values = new HashMap<String, Object>();

        values.put("nuovoUtente", nuovoUtente);
        values.put("auth", auth);
        if (auth > 0) {
            values.put("utente", read(connection, idJS, utente.getAmiciFB()));
        } else {
            values.put("utente", new Utente());
        }

        return values;

    }

    public static Object autenticazioneJS(Connection connection, Utente utente) throws SQLException, Exception {

        int auth = -1;
        int idJS = 0;
        int nuovoUtente = 0;

        String query = "SELECT * FROM utente WHERE email LIKE ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, utente.getEmail());
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            auth = 0;

            String hashPasswordDB = resultSet.getString("password");
            if (!hashPasswordDB.isEmpty() && !utente.getPassword().isEmpty() && hashPasswordDB.equals(utente.getPassword())) {
                auth = 1;
                idJS = resultSet.getInt("id");
            }
        }
        HashMap<String, Object> values = new HashMap<String, Object>();
        if (auth > 0) {
            values.put("utente", read(connection, idJS, utente.getAmiciFB()));
        } else {
            values.put("utente", new Utente());
        }
        values.put("nuovoUtente", nuovoUtente);
        values.put("auth", auth);
        return values;
    }

    public static Object autenticazioneFB(Connection connection, Utente utente) throws SQLException {
        int idJS = 0;
        int nuovoUtente = 0;
        int auth = 1;
        List<Utente> amici = utente.getAmiciFB();

        String query = "SELECT * FROM utente WHERE fb_user_id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setLong(1, utente.getUserIdFB());
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            idJS = resultSet.getInt("id");
            if (!resultSet.getString("email").equalsIgnoreCase(utente.getEmail())) {
                auth = 0;
            }

        } else {

            query = "SELECT * FROM utente WHERE email=?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, utente.getEmail());
            resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) {
                nuovoUtente = 1;
                if (Costanti.produzione) {
                    try {
                        URL obj = new URL("http://www.justselfie.it/my_mc_connection.php");
                        String urlParameters = "email=" + utente.getEmail() + "&name=" + utente.getNome();
                        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
                        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                        con.setRequestMethod("POST");
                        con.setDoOutput(true);
                        con.setDoInput(true);
                        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                        try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                            wr.write(postData);
                        }

                        int g = con.getResponseCode();
                        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                            String inputLine;
                            while ((inputLine = in.readLine()) != null) {
                                System.out.println(inputLine);
                            }
                        }
                        System.out.println(g + "");
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
                String dataRegistrazione = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                query = "INSERT INTO utente "
                        + "(nome, email, sesso, immagine_profilo, fb_user_id, affidabilita, selfie_coin, registrato_il) "
                        + "VALUES (?, ?, ?, ?, ?, 100.00, 500, ?)";
                preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, utente.getNome());
                preparedStatement.setString(2, utente.getEmail());
                preparedStatement.setString(3, utente.getSesso());
                preparedStatement.setString(4, "https://graph.facebook.com/" + utente.getUserIdFB() + "/picture?type=large");
                preparedStatement.setLong(5, utente.getUserIdFB());
                preparedStatement.setString(6, dataRegistrazione);
                preparedStatement.executeUpdate();
                ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
                if (generatedKeys.first()) {
                    idJS = generatedKeys.getInt(1);
                }
                preparedStatement.close();

            } else {
                auth = -1;
            }

        }
        HashMap<String, Object> values = new HashMap<String, Object>();
        if (auth > 0) {
            Utente ut = read(connection, idJS, amici);
            ut.setAmiciFB(eliminaAmiciSeInesistenti(connection, amici));
            values.put("utente", ut);
        } else {
            values.put("utente", new Utente());
        }
        values.put("auth", auth);
        values.put("nuovoUtente", nuovoUtente);
        return values;
    }

    public static Object collegaFB(Connection connection, Utente utente) throws SQLException {
        String query = "UPDATE utente SET fb_user_id=?, sesso=?, immagine_profilo=? WHERE id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setLong(1, utente.getUserIdFB());
        preparedStatement.setString(2, utente.getSesso());
        preparedStatement.setString(3, "https://graph.facebook.com/" + utente.getUserIdFB() + "/picture?type=large");
        preparedStatement.setInt(4, utente.getId());
        preparedStatement.executeUpdate();

        List<Utente> amici = utente.getAmiciFB();
        HashMap<String, Object> values = new HashMap<>();
        Utente ut = read(connection, utente.getId(), utente.getAmiciFB());
        ut.setAmiciFB(eliminaAmiciSeInesistenti(connection, amici));
        values.put("utente", ut);
        return values;
    }

    public static List<Utente> eliminaAmiciSeInesistenti(Connection connection, List<Utente> amici) throws SQLException {
        for (int i = 0; i < amici.size(); i++) {
            Utente amico = amici.get(i);
            String query = "SELECT * FROM utente WHERE fb_user_id=" + amico.getUserIdFB();
            ResultSet result = connection.createStatement().executeQuery(query);
            if (!result.next()) {
                amici.remove(i);
            }else{
                amici.get(i).setNome(result.getString("nome"));
                amici.get(i).setImmagineProfilo(result.getString("immagine_profilo"));
                amici.get(i).setId(result.getInt("id"));
            }
        }
        return amici;
    }

    public static Utente read(Connection connection, int id, List<Utente> amici) throws SQLException {
        Utente utente = new Utente();
        String query = "SELECT * FROM utente WHERE id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.first()) {
            utente.setId(resultSet.getInt("id"));
            utente.setNome(resultSet.getString("nome"));
            utente.setEmail(resultSet.getString("email"));
            utente.setTelefono(resultSet.getString("telefono"));
            utente.setSesso(resultSet.getString("sesso"));
            utente.setImmagineProfilo(resultSet.getString("immagine_profilo"));
            utente.setUserIdFB(resultSet.getLong("fb_user_id"));
            utente.setUserIdINS(resultSet.getLong("ins_user_id"));
            utente.setSelfieCoin(resultSet.getInt("selfie_coin"));
            utente.setAffidabilita(resultSet.getDouble("affidabilita"));
        }
        /*query = "SELECT utente.* FROM utente INNER JOIN utente_amicizie_fb ON ( fb_user_id_b = fb_user_id ) WHERE fb_user_id_a = " + utente.getUserIdFB();
        resultSet = connection.createStatement().executeQuery(query);
        // leggo amicizie fb da a
        while (resultSet.next()) {
            Utente amico = new Utente();
            amico.setNome(resultSet.getString("nome"));
            amico.setId(resultSet.getInt("id"));
            amico.setImmagineProfilo(resultSet.getString("immagine_profilo"));
            amico.setUserIdFB(resultSet.getLong("fb_user_id"));
            utente.getAmiciFB().add(amico);
        }
        // amicizie fb a da
        query = "SELECT utente.* FROM utente INNER JOIN utente_amicizie_fb ON ( fb_user_id_a = fb_user_id ) WHERE fb_user_id_b = " + utente.getUserIdFB();
        resultSet = connection.createStatement().executeQuery(query);
        while (resultSet.next()) {
            Utente amico = new Utente();
            amico.setNome(resultSet.getString("nome"));
            amico.setId(resultSet.getInt("id"));
            amico.setImmagineProfilo(resultSet.getString("immagine_profilo"));
            amico.setUserIdFB(resultSet.getLong("fb_user_id"));
            utente.getAmiciFB().add(amico);
        }*/
        
        utente.setAmiciFB(amici);

        return utente;

    }

    public static Object aggiorna(Connection connection, Utente utente) throws SQLException {
        int auth = 1;
        String query = "SELECT * FROM utente WHERE email=? AND id !=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, utente.getEmail());
        preparedStatement.setInt(2, utente.getId());
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            auth = 0;
        } else {
            query = "UPDATE utente SET nome=?, email=? WHERE id=?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, utente.getNome());
            preparedStatement.setString(2, utente.getEmail());
            preparedStatement.setInt(3, utente.getId());
            preparedStatement.executeUpdate();
        }
        preparedStatement.close();
        utente = read(connection, utente.getId(), utente.getAmiciFB());
        HashMap<String, Object> values = new HashMap<String, Object>();
        values.put("auth", auth);
        if (auth > 0) {
            values.put("utente", utente);
        } else {
            values.put("utente", new Utente());
        }
        return values;
    }

    public static Object scheda(Connection connection, Utente utente) throws SQLException {
        HashMap<String, Object> response = new HashMap<>();
        List<MessaggioUtente> messaggiAffidabilita = new ArrayList<>();
        List<MessaggioUtente> messaggiSelfieCoin = new ArrayList<>();

        String query = "SELECT * FROM utente_messaggi_selfiecoin WHERE id_utente=? ORDER BY data_messaggio DESC";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, utente.getId());
        ResultSet result = preparedStatement.executeQuery();
        while (result.next()) {
            MessaggioUtente messaggioUtente = new MessaggioUtente();
            messaggioUtente.setId(result.getInt("id"));
            messaggioUtente.setMessaggio(result.getString("messaggio"));
            messaggioUtente.setLivello(result.getInt("livello"));
            messaggioUtente.setDataMessaggio(result.getString("data_messaggio"));
            messaggiSelfieCoin.add(messaggioUtente);
        }

        query = "SELECT * FROM utente_messaggi_affidabilita WHERE id_utente=? ";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, utente.getId());
        result = preparedStatement.executeQuery();
        while (result.next()) {
            MessaggioUtente messaggioUtente = new MessaggioUtente();
            messaggioUtente.setId(result.getInt("id"));
            messaggioUtente.setMessaggio(result.getString("messaggio"));
            messaggioUtente.setLivello(result.getInt("livello"));
            messaggioUtente.setDataMessaggio(result.getString("data_messaggio"));
            messaggiAffidabilita.add(messaggioUtente);
        }

        response.put("messaggiSelfieCoin", messaggiSelfieCoin);
        response.put("messaggiAffidabilita", messaggiAffidabilita);
        response.put("utente", read(connection, utente.getId(), utente.getAmiciFB()));
        return response;

    }

    public static Void registraDevice(Connection connection, int idUtente, String idDevice, String token, int tipoDispositivo) throws SQLException {
        if (idUtente > 0) {
            String query = "SELECT * FROM utente_device WHERE id_device=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, idDevice);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                query = "UPDATE utente_device SET id_utente=?, token=? WHERE id=?";
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setInt(1, idUtente);
                preparedStatement.setString(2, token);
                preparedStatement.setInt(3, id);
                preparedStatement.executeUpdate();
            } else {
                query = "INSERT INTO utente_device (id_utente, id_device, token, tipo_dispositivo) VALUES (?, ?, ?, ?)";
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setInt(1, idUtente);
                preparedStatement.setString(2, idDevice);
                preparedStatement.setString(3, token);
                preparedStatement.setInt(4, tipoDispositivo);
                preparedStatement.executeUpdate();
            }
        } else {

            /*
        Solo per verificare che non arrivino piu id utente a 0
             */
            FileWriter filerWriter = null;
            try {
                File file = new File(Costanti.REPORT_FILE);
                //filerWriter = new FileWriter(file);
                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.append("#").append(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date())).append(": ");
                stringBuilder.append("Id utente a 0, device tipo ").append(tipoDispositivo == 1 ? "ios" : "android");
                stringBuilder.append("\n");
                Files.write(Paths.get(Costanti.REPORT_FILE), stringBuilder.toString().getBytes(), StandardOpenOption.APPEND);
                //filerWriter.flush();
                //filerWriter.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());

            }
        }

        return null;
    }

    public static Object score(Connection connection, Utente utente) throws SQLException {
        HashMap<String, Object> response = new HashMap<String, Object>();
        List<Sconto> sconti = new ArrayList<>();
        List<Selfie> selfie = new ArrayList<>();
        int selfieCoin = 0;
        String dataOggi = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        // leggo sconti
        String querySconti = "SELECT * FROM sconto WHERE id_utente=? AND data_scadenza > ? AND convalidato=0";
        PreparedStatement preparedStatementSconti = connection.prepareStatement(querySconti);
        preparedStatementSconti.setInt(1, utente.getId());
        preparedStatementSconti.setString(2, dataOggi);
        ResultSet resultSetSconti = preparedStatementSconti.executeQuery();
        while (resultSetSconti.next()) {
            Sconto sconto = new Sconto();
            sconto.setId(resultSetSconti.getInt("id"));
            sconto.setSconto(resultSetSconti.getInt("sconto"));
            sconto.setDataCreazione(resultSetSconti.getString("data_creazione"));
            sconto.setDataScadenza(resultSetSconti.getString("data_scadenza"));
            Esercente esercente = DAOEsercente.read(connection, resultSetSconti.getInt("id_esercente"));
            sconto.setEsercente(esercente);
            sconti.add(sconto);
        }

        String querySelfie = "SELECT * FROM selfie WHERE id_utente = ? AND convalidato=0";
        PreparedStatement preparedStatementSelfie = connection.prepareStatement(querySelfie);
        preparedStatementSelfie.setInt(1, utente.getId());
        ResultSet resultSetSelfie = preparedStatementSelfie.executeQuery();
        while (resultSetSelfie.next()) {
            Selfie sel = new Selfie();
            sel.setId(resultSetSelfie.getInt("id"));
            sel.setSelfie(resultSetSelfie.getString("selfie"));
            sel.setTipoCondivisione(resultSetSelfie.getInt("tipo_condivisione"));
            sel.setRegistratoIl(resultSetSelfie.getString("data_registrazione"));
            sel.setEsercente(DAOEsercente.read(connection, resultSetSelfie.getInt("id_esercente")));
            selfie.add(sel);
        }

        String querySelfieCoin = "SELECT * FROM utente WHERE id=?";
        PreparedStatement preparedStatementSelfieCoin = connection.prepareStatement(querySelfieCoin);
        preparedStatementSelfieCoin.setInt(1, utente.getId());
        ResultSet resultSetSelfieCoin = preparedStatementSelfieCoin.executeQuery();
        if (resultSetSelfieCoin.next()) {
            selfieCoin = resultSetSelfieCoin.getInt("selfie_coin");
        }

        response.put("selfieCoin", selfieCoin);
        response.put("sconti", sconti);
        response.put("selfie", selfie);

        return response;
    }
}
