/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.dao;

import app.model.CodicePromo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import app.model.Categoria;

/**
 *
 * @author Davide
 */
public class DAOCodicePromo {

    public static List<CodicePromo> readCodicePromo(Connection connection) throws SQLException {
        List<CodicePromo> elencoCodiciPromo = new ArrayList<>();
        String query = "SELECT * FROM codice_promo WHERE 1=1";
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        while (resultSet.next()) {
            CodicePromo codicePromo = new CodicePromo();
            codicePromo.setId(resultSet.getInt("id"));
            codicePromo.setCodice(resultSet.getString("codice_promo"));
            codicePromo.setSelfieCoin(resultSet.getInt("selfie_coin") + "");
            codicePromo.setDataInizio(resultSet.getString("data_inizio"));
            codicePromo.setDataFine(resultSet.getString("data_fine"));
            elencoCodiciPromo.add(codicePromo);
        }
        return elencoCodiciPromo;

    }

    public static CodicePromo readCodicePromo(Connection connection, int id) throws SQLException {
        CodicePromo codicePromo = new CodicePromo();
        String query = "SELECT * FROM codice_promo WHERE id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.first()) {
            codicePromo.setId(resultSet.getInt("id"));
            codicePromo.setCodice(resultSet.getString("codice_promo"));
            codicePromo.setSelfieCoin(resultSet.getInt("selfie_coin") + "");
            codicePromo.setDataInizio(resultSet.getString("data_inizio"));
            codicePromo.setDataFine(resultSet.getString("data_fine"));
        }
        resultSet.close();
        preparedStatement.close();
        return codicePromo;

    }

    public static void createCodicePromo(Connection connection, CodicePromo codicePromo) throws SQLException {
        String query = "INSERT INTO codice_promo (codice_promo, selfie_coin, data_inizio, data_fine) VALUES (?, ?, ?, ?) ";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, codicePromo.getCodice());
        preparedStatement.setInt(2, Integer.parseInt(codicePromo.getSelfieCoin()));
        preparedStatement.setString(3, codicePromo.getDataInizio());
        preparedStatement.setString(4, codicePromo.getDataFine());
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public static void updateCodicePromo(Connection connection, CodicePromo codicePromo) throws SQLException {
        String query = "UPDATE codice_promo SET codice_promo=?, selfie_coin=?, data_inizio=?, data_fine=? WHERE id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, codicePromo.getCodice());
        preparedStatement.setInt(2, Integer.parseInt(codicePromo.getSelfieCoin()));
        preparedStatement.setString(3, codicePromo.getDataInizio());
        preparedStatement.setString(4, codicePromo.getDataFine());
        preparedStatement.setInt(5, codicePromo.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();

    }

    public static void deleteCodicePromo(Connection connection, CodicePromo codicePromo) throws SQLException {
        String query = "DELETE FROM codice_promo WHERE id=? ";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, codicePromo.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public static Object codicePromo(Connection connection, int idUtente, String codicePromo) throws SQLException {
        HashMap<String, Object> response = new HashMap<>();
        int auth = 0;
        String messaggio = "";
        String query = "SELECT * FROM codice_promo WHERE codice_promo=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, codicePromo);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            query = "SELECT * FROM codice_promo WHERE codice_promo=? AND data_inizio <= ? AND data_fine >= ? ";
            /*
            * Codice promo esistente. Controllo se è valido nella data.
             */
            String dataOggi = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, codicePromo);
            preparedStatement.setString(2, dataOggi);
            preparedStatement.setString(3, dataOggi);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                /*
                * Codice promo valido. Controllo se è gia stato usato da quell'utente.
                 */
                int idCodicePromo = resultSet.getInt("id");
                int selfieCoin = resultSet.getInt("selfie_coin");
                query = "SELECT * FROM utente_codice_promo WHERE id_codice_promo=? AND id_utente=?";
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setInt(1, idCodicePromo);
                preparedStatement.setInt(2, idUtente);
                resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    /**
                     * L'utente ha gia utilizzato questo codice promo
                     */
                    messaggio = "Codice promo gia utilizzato";
                } else {
                    /**
                     * L'utente non ha gia utilizzato questo codice promo.
                     * Quindi inserisco l'utilizzo e accredito selfie coin
                     */
                    query = "INSERT INTO utente_codice_promo (id_codice_promo, id_utente, data_utilizzo) VALUES (?, ?, ?)";
                    preparedStatement = connection.prepareStatement(query);
                    preparedStatement.setInt(1, idCodicePromo);
                    preparedStatement.setInt(2, idUtente);
                    preparedStatement.setString(3, dataOggi);
                    preparedStatement.executeUpdate();

                    query = "INSERT INTO utente_messaggi_selfiecoin (id_utente, messaggio, livello, quantita, data_messaggio)"
                            + " VALUES (?, ?, ?, ?, ?)";
                    preparedStatement = connection.prepareStatement(query);
                    preparedStatement.setInt(1, idUtente);
                    preparedStatement.setString(2, "Hai ricevuto " + selfieCoin + " selfie coin per aver utilizzato un codice promo");
                    preparedStatement.setInt(3, 1);
                    preparedStatement.setInt(4, selfieCoin);
                    preparedStatement.setString(5, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                    preparedStatement.executeUpdate();

                    query = "SELECT * FROM utente WHERE id=?";
                    preparedStatement = connection.prepareStatement(query);
                    preparedStatement.setInt(1, idUtente);
                    resultSet = preparedStatement.executeQuery();
                    if (resultSet.next()) {
                        int oldSelfieCoin = resultSet.getInt("selfie_coin");
                        idUtente = resultSet.getInt("id");
                        oldSelfieCoin = oldSelfieCoin + selfieCoin;
                        query = "UPDATE utente SET selfie_coin = " + oldSelfieCoin + " WHERE id=" + idUtente;
                        connection.createStatement().executeUpdate(query);
                        auth = 1;
                        messaggio = "Codice promo valido. Hai guadagnato " + selfieCoin + " selfie coin!";
                    }

                }
            } else {
                messaggio = "Codice promo non valido.";
            }

        } else {
            messaggio = "Codice promo non esistente.";
        }
        response.put("auth", auth);
        response.put("messaggio", messaggio);
        return response;
    }
}
