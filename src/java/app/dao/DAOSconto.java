/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.dao;

import app.model.Sconto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Davide
 */
public class DAOSconto {
    
    public static Sconto read(Connection connection, int idEsercente, int idUtente){
        Sconto sconto = new Sconto();
        return sconto;
    }
    
    
     public static Sconto read(Connection connection, int id) throws SQLException{
        Sconto sconto = new Sconto();
        String query = "SELECT * FROM sconto WHERE id=" + id;
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        if(resultSet.first()){
            sconto.setId(resultSet.getInt("id"));
            sconto.setSconto(resultSet.getInt("sconto"));
            sconto.setDataCreazione(resultSet.getString("data_creazione"));
            sconto.setDataScadenza(resultSet.getString("data_scadenza"));
            sconto.setDataConvalida(resultSet.getString("data_convalida"));
            sconto.setConvalidato(resultSet.getInt("convalidato"));
        }
        return sconto;
    }
}
