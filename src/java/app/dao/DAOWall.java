/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.dao;

import app.model.Esercente;
import app.model.Selfie;
import app.model.Utente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Davide
 */
public class DAOWall {

    public static Object readWallFiltratoUtente(Connection connection) throws SQLException {
        HashMap<String, Object> results = new HashMap<String, Object>();
        List<Selfie> elencoSelfie = new ArrayList<Selfie>();
        String query = "SELECT selfie.*, utente.immagine_profilo FROM selfie INNER JOIN utente ON selfie.id_utente = utente.id WHERE id_selfie=0 AND convalidato=1 ORDER BY data_registrazione DESC";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Selfie selfie = new Selfie();
            selfie.setId(resultSet.getInt("id"));
            Utente utente = new Utente();
            utente.setImmagineProfilo(resultSet.getString("immagine_profilo"));
            selfie.setUtente(utente);
            selfie.setSelfie(resultSet.getString("selfie"));
            elencoSelfie.add(selfie);
        }
        results.put("selfies", elencoSelfie);
        return results;

    }

    public static List<Selfie> readWallFiltratoUtenteEsercente(Connection connection, int idUtente, int idEsercente) throws SQLException {
        List<Selfie> elencoSelfie = new ArrayList<Selfie>();
        String query = "SELECT * FROM selfie.*, utente.immagine_profilo FROM selfie "
                + "INNER JOIN utente ON selfie.id_utente = utente.id "
                + "WHERE id_sercente=? AND taggato = 0 AND convalidato=1";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, idUtente);
        preparedStatement.setInt(2, idEsercente);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Selfie selfie = new Selfie();
            selfie.setId(resultSet.getInt("id"));
            Utente utente = new Utente();
            utente.setId(resultSet.getInt("id_utente"));
            utente.setImmagineProfilo(resultSet.getString("immagine_profilo"));
            selfie.setUtente(utente);
            selfie.setSelfie(resultSet.getString("selfie"));
            elencoSelfie.add(selfie);
        }
        return elencoSelfie;
    }

    public static Object readSelfie(Connection connection, int idSelfie) throws SQLException {
        HashMap<String, Object> results = new HashMap<String, Object>();
        Selfie selfie = new Selfie();
        List<Utente> utentiTaggati = new ArrayList<Utente>();
        String query = "SELECT selfie.*, utente.nome, utente.immagine_profilo FROM selfie "
                + "INNER JOIN utente ON utente.id = id_utente "
                + "WHERE selfie.id = ? OR id_selfie = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, idSelfie);
        preparedStatement.setInt(2, idSelfie);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {

            Utente utente = new Utente();
            utente.setId(resultSet.getInt("id_utente"));
            utente.setNome(resultSet.getString("nome"));
            utente.setImmagineProfilo(resultSet.getString("immagine_profilo"));

            if (resultSet.getInt("id_selfie") > 0) {
                utentiTaggati.add(utente);
            } else {
                selfie.setSelfie(resultSet.getString("selfie"));
                selfie.setTesto(resultSet.getString("testo"));
                selfie.setTipoCondivisione(resultSet.getInt("tipo_condivisione"));
                selfie.setRegistratoIl(resultSet.getString("data_registrazione"));
                Esercente esercente = new Esercente();
                esercente.setId(resultSet.getInt("id_esercente"));
                selfie.setEsercente(esercente);
                selfie.setUtente(utente);
                selfie.setId(resultSet.getInt("id"));
            }

        }
        selfie.setEsercente(DAOEsercente.read(connection, selfie.getEsercente().getId()));
        selfie.setUtentiTaggati(utentiTaggati);
        results.put("selfie", selfie);
        return results;
    }
}
