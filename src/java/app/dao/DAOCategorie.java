/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import app.model.Categoria;
import app.model.Contratto;
import model.ScaglioneSelfie;
import newsletter.model.Template;

/**
 *
 * @author Davide
 */
public class DAOCategorie {

    public static List<Categoria> readCategoria(Connection connection) throws SQLException {
        List<Categoria> elencoCategorie = new ArrayList<>();
        String query = "SELECT * FROM categoria WHERE 1=1";
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        while (resultSet.next()) {
            Categoria categoria = new Categoria();
            categoria.setId(resultSet.getInt("id"));
            categoria.setNome(resultSet.getString("nome"));
            categoria.setIcona(resultSet.getString("icona"));
            categoria.setColore(resultSet.getString("colore"));
            elencoCategorie.add(categoria);
        }
        return elencoCategorie;

    }

    public static Categoria readCategoria(Connection connection, int id) throws SQLException {
        Categoria categoria = new Categoria();
        String query = "SELECT * FROM categoria WHERE id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.first()) {
            categoria.setId(resultSet.getInt("id"));
            categoria.setNome(resultSet.getString("nome")); 
            categoria.setIcona(resultSet.getString("icona"));
            categoria.setColore(resultSet.getString("colore"));
        }
        resultSet.close();
        preparedStatement.close();
        return categoria;

    }

    public static void createCategoria(Connection connection, Categoria categoria) throws SQLException {
        String query = "INSERT INTO categoria (nome, icona, colore) VALUES (?, ?, ?) ";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, categoria.getNome());
        preparedStatement.setString(2, categoria.getIcona());
        preparedStatement.setString(3, categoria.getColore());
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public static void updateCategoria(Connection connection, Categoria categoria) throws SQLException {
        String query = "UPDATE categoria SET nome=?, icona=?, colore=? WHERE id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, categoria.getNome());
        preparedStatement.setString(2, categoria.getIcona());
        preparedStatement.setString(3, categoria.getColore());
        preparedStatement.setInt(4, categoria.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();

    }

    public static void deleteCategoria(Connection connection, Categoria categoria) throws SQLException {
        String query = "DELETE FROM categoria WHERE id=? ";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, categoria.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

}
