/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;
import app.model.Selfie;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Davide
 */
public class DAOSelfie implements Serializable{
    
    
    public static List<Selfie> selfieEsercente(Connection connection, int idEsercente, int idUtente) throws SQLException{
        List<Selfie> elencoSelfie = new ArrayList<>();
        String querySelfie = "SELECT * FROM selfie WHERE id_esercente=? AND idUtente !=?";
        
        PreparedStatement preparedStatement = connection.prepareStatement(querySelfie);
        preparedStatement.setInt(1, idEsercente);
        preparedStatement.setInt(2, idUtente);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            Selfie selfie = new Selfie();
        }
        resultSet.close();
        preparedStatement.close();
        return elencoSelfie;
    }
    
     public static List<Selfie> wall(Connection connection, int idUtente) throws SQLException{
        List<Selfie> elencoSelfie = new ArrayList<>();
        String querySelfie = "SELECT * FROM selfie WHERE idUtente !=?";
        
        PreparedStatement preparedStatement = connection.prepareStatement(querySelfie);
        preparedStatement.setInt(1, idUtente);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            Selfie selfie = new Selfie();
        }
        resultSet.close();
        preparedStatement.close();
        return elencoSelfie;
    }
    
     public static Selfie schedaSelfie(Connection connection, int idSelfie) throws SQLException{
        String querySelfie = "SELECT * FROM selfie WHERE id=?";
        Selfie selfie = new Selfie();
        PreparedStatement preparedStatement = connection.prepareStatement(querySelfie);
        preparedStatement.setInt(1, idSelfie);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            
        }
        resultSet.close();
        preparedStatement.close();
        
        return selfie;
    }
}
