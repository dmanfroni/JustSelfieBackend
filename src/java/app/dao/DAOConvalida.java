/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.dao;

import app.model.AcquistoItem;
import app.model.ConvalidaGiftCard;
import app.model.ConvalidaOfferta;
import app.model.ConvalidaSconto;
import app.model.ConvalidaSelfie;
import app.model.Utente;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import javax.imageio.ImageIO;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import util.Costanti;
import util.NotificationManager;

/**
 *
 * @author Davide
 */
public class DAOConvalida {

    public static Object convalidaOfferta(Connection connection, ConvalidaOfferta convalidaOfferta) {

        HashMap<String, Object> risposta = new HashMap<String, Object>();
        try {
            /* Controllo se ha usato selfie coin e ne ha disponibili*/

            if (convalidaOfferta.getAcquisto().getSelfieCoinUsati() > 0) {
                String queryChck = "SELECT * FROM utente WHERE id=?";
                PreparedStatement preparedStatementChck = connection.prepareStatement(queryChck);
                preparedStatementChck.setInt(1, convalidaOfferta.getSelfie().getUtente().getId());
                ResultSet rs = preparedStatementChck.executeQuery();
                if (rs.next()) {
                    if (rs.getInt("selfie_coin") < convalidaOfferta.getAcquisto().getSelfieCoinUsati()) {
                        risposta.put("errore", "Non hai abbastanza selfie coin");
                    } else {
                        String query = "UPDATE utente SET selfie_coin=? WHERE id=?";
                        PreparedStatement preparedStatement = connection.prepareStatement(query);
                        preparedStatement.setInt(1, rs.getInt("selfie_coin") - convalidaOfferta.getAcquisto().getSelfieCoinUsati());
                        preparedStatement.setInt(2, convalidaOfferta.getSelfie().getUtente().getId());
                        preparedStatement.executeUpdate();
                        query = "INSERT INTO utente_gift_card "
                                + "(id_utente, id_esercente, valore_selfiecoin, valore_euro, data_utilizzo)"
                                + " VALUES (?, ?, ?, ?, ?)";
                        preparedStatement = connection.prepareStatement(query);
                        preparedStatement.setInt(1, convalidaOfferta.getSelfie().getUtente().getId());
                        preparedStatement.setInt(2, convalidaOfferta.getSelfie().getEsercente().getId());
                        preparedStatement.setInt(3, convalidaOfferta.getAcquisto().getSelfieCoinUsati());
                        preparedStatement.setInt(4, convalidaOfferta.getAcquisto().getSelfieCoinUsati() / 100);
                        preparedStatement.setString(5, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                        preparedStatement.executeUpdate();

                        /* Inserire il messaggio */
                        query = "INSERT INTO utente_messaggi_selfiecoin "
                                + "(id_utente, messaggio, livello, quantita, data_messaggio)"
                                + " VALUES (?, ?, ?, ?, ?)";
                        preparedStatement = connection.prepareStatement(query);
                        preparedStatement.setInt(1, convalidaOfferta.getSelfie().getUtente().getId());
                        preparedStatement.setString(2,
                                "Hai speso " + convalidaOfferta.getAcquisto().getSelfieCoinUsati() + " selfie coin per la gift card da " + (convalidaOfferta.getAcquisto().getSelfieCoinUsati() / 100) + "€");
                        preparedStatement.setInt(3, 0);
                        preparedStatement.setInt(4, -convalidaOfferta.getAcquisto().getSelfieCoinUsati());
                        preparedStatement.setString(5, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                        preparedStatement.executeUpdate();
                    }
                }
            }

            /*Salvo immagine sul file system e libero memoria*/
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            String nomeImmagineSelfie = calendar.getTimeInMillis() + "_" + convalidaOfferta.getSelfie().getUtente().getId() + ".jpg";
            byte[] decodedImg = Base64.getDecoder().decode(convalidaOfferta.getImmagineBase64().getBytes(StandardCharsets.UTF_8));
            Path destinationFile = Paths.get(Costanti.SELFIE_DIRECTORY, nomeImmagineSelfie);
            Files.write(destinationFile, decodedImg);

            BufferedImage src = ImageIO.read(new File(Costanti.SELFIE_DIRECTORY, nomeImmagineSelfie));

            int width = src.getWidth();
            int height = src.getHeight();
            String[] temp = nomeImmagineSelfie.split(".");

            BufferedImage out = src.getSubimage(0, (height - 630) / 2, 1200, 630);
            String nomeTempImmagine = nomeImmagineSelfie.replace(".jpg", "") + "_prv.jpg";

            ImageIO.write(out, "jpg", new File(Costanti.SELFIE_DIRECTORY, nomeTempImmagine));
            convalidaOfferta.setImmagineBase64(null);

            /**
             * Registra acquisto e acquisto offerte
             */
            String query = "INSERT INTO acquisto (id_campagna, id_utente, id_esercente, importo_totale, data_acquisto) "
                    + "VALUES (?, ?, ?, ?, ?)";

            String dataAcquisto = "";
            int idAcquisto = 0;
            double importo = 0.0;
            Date oggi = new Date();

            dataAcquisto = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(oggi);
            for (int i = 0; i < convalidaOfferta.getAcquisto().getOfferteAcquistate().size(); i++) {
                importo = importo + (convalidaOfferta.getAcquisto().getOfferteAcquistate().get(i).getOfferta().getPrezzoScontato() * convalidaOfferta.getAcquisto().getOfferteAcquistate().get(i).getQuantita());
            }

            PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, convalidaOfferta.getAcquisto().getCampagna().getId());
            preparedStatement.setInt(2, convalidaOfferta.getSelfie().getUtente().getId());
            preparedStatement.setInt(3, convalidaOfferta.getSelfie().getEsercente().getId());
            preparedStatement.setDouble(4, importo);
            preparedStatement.setString(5, dataAcquisto);
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.first()) {
                idAcquisto = generatedKeys.getInt(1);
            }
            preparedStatement.close();
            for (AcquistoItem acquistoItem : convalidaOfferta.getAcquisto().getOfferteAcquistate()) {
                query = "INSERT INTO acquisto_offerta (id_acquisto, id_offerta, quantita) VALUES (?, ?, ?)";
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setInt(1, idAcquisto);
                preparedStatement.setInt(2, acquistoItem.getOfferta().getId());
                preparedStatement.setInt(3, acquistoItem.getQuantita());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }

            /**
             * Inserisco selfie per tutti gli utenti taggati per le offerte
             */
            int idSelfie = 0;
            String querySelfie = "INSERT INTO selfie "
                    + "(id_esercente, id_acquisto, id_campagna, id_utente, selfie, testo, tipo_condivisione, convalidato, data_registrazione, data_convalida) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            preparedStatement = connection.prepareStatement(querySelfie, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, convalidaOfferta.getSelfie().getEsercente().getId());
            preparedStatement.setInt(2, idAcquisto);
            preparedStatement.setInt(3, convalidaOfferta.getAcquisto().getCampagna().getId());
            preparedStatement.setInt(4, convalidaOfferta.getSelfie().getUtente().getId());
            preparedStatement.setString(5, nomeImmagineSelfie);
            preparedStatement.setString(6, convalidaOfferta.getSelfie().getTesto());
            preparedStatement.setInt(7, convalidaOfferta.getSelfie().getTipoCondivisione());
            preparedStatement.setInt(8, 1);
            preparedStatement.setString(9, dataAcquisto);
            preparedStatement.setString(10, dataAcquisto);
            preparedStatement.executeUpdate();
            generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                idSelfie = generatedKeys.getInt(1);
            }

            for (Utente amicoTaggato : convalidaOfferta.getUtentiTagOfferta()) {
                querySelfie = "INSERT INTO selfie "
                        + "(id_esercente, id_acquisto, id_campagna, id_utente, id_selfie, selfie, testo, tipo_condivisione, convalidato, data_registrazione, data_convalida) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                preparedStatement = connection.prepareStatement(querySelfie, PreparedStatement.RETURN_GENERATED_KEYS);
                preparedStatement.setInt(1, convalidaOfferta.getSelfie().getEsercente().getId());
                preparedStatement.setInt(2, idAcquisto);
                preparedStatement.setInt(3, convalidaOfferta.getAcquisto().getCampagna().getId());
                preparedStatement.setInt(4, amicoTaggato.getId());
                preparedStatement.setInt(5, idSelfie);
                preparedStatement.setString(6, nomeImmagineSelfie);
                preparedStatement.setString(7, convalidaOfferta.getSelfie().getTesto());
                preparedStatement.setInt(8, convalidaOfferta.getSelfie().getTipoCondivisione());
                preparedStatement.setInt(9, 1);
                preparedStatement.setString(10, dataAcquisto);
                preparedStatement.setString(11, dataAcquisto);
                preparedStatement.executeUpdate();
            }

            /**
             * Crea sconto per tutti gli utenti taggati nelle offerte
             */
            double sconto = convalidaOfferta.getAcquisto().getCampagna().getSconto();

            if (convalidaOfferta.getSelfie().getTipoCondivisione() == 2) {
                sconto = ((sconto / 100) * (100 - convalidaOfferta.getSelfie().getEsercente().getContratto().getScontoScontrinoRiduzioneNoLink()));;
            }
            int durataSconto = convalidaOfferta.getSelfie().getEsercente().getContratto().getScontroScontrinoValidita();

            LocalDateTime localDateTime = oggi.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            localDateTime = localDateTime.plusDays(durataSconto);
            Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
            String dataScadenza = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentDatePlusOneDay);

            String querySconto = "INSERT INTO sconto "
                    + "(id_selfie_generato, id_utente, id_esercente, sconto, data_creazione, data_scadenza) "
                    + "VALUES (?, ?, ?, ?, ?, ?)";
            preparedStatement = connection.prepareStatement(querySconto, PreparedStatement.RETURN_GENERATED_KEYS);

            preparedStatement.setInt(1, idSelfie);
            preparedStatement.setInt(2, convalidaOfferta.getSelfie().getUtente().getId());
            preparedStatement.setInt(3, convalidaOfferta.getSelfie().getEsercente().getId());
            preparedStatement.setInt(4, (int) sconto);
            preparedStatement.setString(5, dataAcquisto);
            preparedStatement.setString(6, dataScadenza);
            preparedStatement.executeUpdate();

            for (Utente amicoTaggato : convalidaOfferta.getUtentiTagOfferta()) {
                querySconto = "INSERT INTO sconto "
                        + "(id_selfie_generato, id_utente, id_esercente, sconto, data_creazione, data_scadenza) "
                        + "VALUES (?, ?, ?, ?, ?, ?)";
                preparedStatement = connection.prepareStatement(querySconto, PreparedStatement.RETURN_GENERATED_KEYS);
                preparedStatement.setInt(1, idSelfie);
                preparedStatement.setInt(2, amicoTaggato.getId());
                preparedStatement.setInt(3, convalidaOfferta.getSelfie().getEsercente().getId());
                preparedStatement.setInt(4, (int) sconto);
                preparedStatement.setString(5, dataAcquisto);
                preparedStatement.setString(6, dataScadenza);
                preparedStatement.executeUpdate();
            }

            /**
             * Invio notifica agli utenti per lo sconto
             */
            String testoNotificaSconto = Costanti.NOTIFICA_SCONTO_DA_OFFERTA;
            testoNotificaSconto = testoNotificaSconto.replace("#SCONTO", ((int)sconto) + "");
            testoNotificaSconto = testoNotificaSconto.replace("#ESERCENTE", convalidaOfferta.getSelfie().getEsercente().getNome());
            testoNotificaSconto = testoNotificaSconto.replace("#GIORNIVALIDITA", durataSconto + "");
            NotificationManager notificationManager = new NotificationManager();
            notificationManager.startSessione();
            for (Utente amicoTaggato : convalidaOfferta.getUtentiTagOfferta()) {
                querySelfie = "SELECT * FROM utente_device WHERE id_utente=?";
                preparedStatement = connection.prepareStatement(querySelfie);
                preparedStatement.setInt(1, amicoTaggato.getId());
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    if (resultSet.getInt("tipo_dispositivo") == 1) {
                        notificationManager.inviaNotificaiOS(resultSet.getString("token"), "JustSelfie", testoNotificaSconto, Costanti.ACTION_SCONTI_VIEW, 0);
                    } else if (resultSet.getInt("tipo_dispositivo") == 2) {
                        notificationManager.inviaNotificaAndroid(resultSet.getString("token"), "JustSelfie", testoNotificaSconto, Costanti.ACTION_SCONTI_VIEW, 0);
                    }
                }
            }

            /**
             * Inserisco selfie per utenti taggati selfie coin
             */
            for (Utente amicoTaggato : convalidaOfferta.getSelfie().getUtentiTaggati()) {
                querySelfie = "INSERT INTO selfie "
                        + "(id_esercente, id_utente, id_selfie, selfie, testo, tipo_condivisione, data_registrazione) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?)";
                preparedStatement = connection.prepareStatement(querySelfie, PreparedStatement.RETURN_GENERATED_KEYS);
                preparedStatement.setInt(1, convalidaOfferta.getSelfie().getEsercente().getId());
                preparedStatement.setInt(2, amicoTaggato.getId());
                preparedStatement.setInt(3, idSelfie);
                preparedStatement.setString(4, nomeImmagineSelfie);
                preparedStatement.setString(5, convalidaOfferta.getSelfie().getTesto());
                preparedStatement.setInt(6, convalidaOfferta.getSelfie().getTipoCondivisione());
                preparedStatement.setString(7, dataAcquisto);
                preparedStatement.executeUpdate();
            }

            double selfieCoin = convalidaOfferta.getSelfie().getEsercente().getContratto().getSelfieCoin();
            if (convalidaOfferta.getSelfie().getTipoCondivisione() == 2) {
                selfieCoin = (selfieCoin / 100) * (100 - convalidaOfferta.getSelfie().getEsercente().getContratto().getSelfieCoinRiduzioneNoLink());
            }

            String testoNotificaSelfieCoin = Costanti.NOTIFICA_SELFIE_DA_CONVALIDARE;
            testoNotificaSelfieCoin = testoNotificaSelfieCoin.replace("#ESERCENTE", convalidaOfferta.getSelfie().getEsercente().getNome());
            testoNotificaSelfieCoin = testoNotificaSelfieCoin.replace("#SELFIECOIN", (int) selfieCoin + "");

            for (Utente amicoTaggato : convalidaOfferta.getSelfie().getUtentiTaggati()) {
                querySelfie = "SELECT * FROM utente_device WHERE id_utente=? ";
                preparedStatement = connection.prepareStatement(querySelfie);
                preparedStatement.setInt(1, amicoTaggato.getId());
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    if (resultSet.getInt("tipo_dispositivo") == 1) {
                        notificationManager.inviaNotificaiOS(resultSet.getString("token"), "JustSelfie", testoNotificaSelfieCoin, Costanti.ACTION_SELFIES_VIEW, 0);
                    } else if (resultSet.getInt("tipo_dispositivo") == 2) {
                        notificationManager.inviaNotificaAndroid(resultSet.getString("token"), "JustSelfie", testoNotificaSelfieCoin, Costanti.ACTION_SELFIES_VIEW, 0);
                    }
                }
            }

            notificationManager.pulisciDatabase();

            risposta.put("idSelfie", idSelfie);
            risposta.put("nomeSelfie", nomeImmagineSelfie);
            risposta.put("scontoSuccessivo", ((int) sconto) + "");
        } catch (Exception ex) {
            risposta.put("errore", ex.getMessage());
        }
        return risposta;
    }

    public static Object convalidaSconto(Connection connection, ConvalidaSconto convalidaSconto) throws SQLException, IOException {
        HashMap<String, Object> risposta = new HashMap<String, Object>();
        /*Salvo immagine sul file system e libero memoria*/
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        String nomeImmagineSelfie = calendar.getTimeInMillis() + "_" + convalidaSconto.getSelfie().getUtente().getId() + ".jpg";
        byte[] decodedImg = Base64.getDecoder().decode(convalidaSconto.getImmagineBase64().getBytes(StandardCharsets.UTF_8));
        Path destinationFile = Paths.get(Costanti.SELFIE_DIRECTORY, nomeImmagineSelfie);
        Files.write(destinationFile, decodedImg);
        BufferedImage src = ImageIO.read(new File(Costanti.SELFIE_DIRECTORY, nomeImmagineSelfie));

        int width = src.getWidth();
        int height = src.getHeight();
        String[] temp = nomeImmagineSelfie.split(".");

        BufferedImage out = src.getSubimage(0, (height - 630) / 2, 1200, 630);
        String nomeTempImmagine = nomeImmagineSelfie.replace(".jpg", "") + "_prv.jpg";

        ImageIO.write(out, "jpg", new File(Costanti.SELFIE_DIRECTORY, nomeTempImmagine));
        convalidaSconto.setImmagineBase64(null);

        /**
         * Convalida sconto
         */
        String dataConvalida = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        String query = "UPDATE sconto SET data_convalida=?, convalidato=1 WHERE id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, dataConvalida);
        preparedStatement.setInt(2, convalidaSconto.getSconto().getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();

        /**
         * Inserisco selfie dell'utente
         */
        int idSelfie = 0;

        String querySelfie = "INSERT INTO selfie "
                + "(id_esercente, id_sconto, id_utente, selfie, testo, tipo_condivisione, convalidato, data_registrazione, data_convalida) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        preparedStatement = connection.prepareStatement(querySelfie, PreparedStatement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, convalidaSconto.getSelfie().getEsercente().getId());
        preparedStatement.setInt(2, convalidaSconto.getSconto().getId());
        preparedStatement.setInt(3, convalidaSconto.getSelfie().getUtente().getId());
        preparedStatement.setString(4, nomeImmagineSelfie);
        preparedStatement.setString(5, convalidaSconto.getSelfie().getTesto());
        preparedStatement.setInt(6, convalidaSconto.getSelfie().getTipoCondivisione());
        preparedStatement.setInt(7, 1);
        preparedStatement.setString(8, dataConvalida);
        preparedStatement.setString(9, dataConvalida);
        preparedStatement.executeUpdate();
        ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
        if (generatedKeys.next()) {
            idSelfie = generatedKeys.getInt(1);
        }

        /**
         * Distribuisco selfie coin all'utente che ha convalidato
         */
        double selfieCoin = convalidaSconto.getSelfie().getEsercente().getContratto().getSelfieCoin();
        if (convalidaSconto.getSelfie().getTipoCondivisione() == 2) {
            selfieCoin = (selfieCoin / 100) * (100 - convalidaSconto.getSelfie().getEsercente().getContratto().getSelfieCoinRiduzioneNoLink());
        }

        String querySelfieCoin = "SELECT * FROM utente WHERE id=?";
        preparedStatement = connection.prepareStatement(querySelfieCoin);
        preparedStatement.setInt(1, convalidaSconto.getSelfie().getUtente().getId());
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            int oldSelfieCoin = resultSet.getInt("selfie_coin");
            oldSelfieCoin = oldSelfieCoin + (int) selfieCoin;
            querySelfieCoin = "UPDATE utente SET selfie_coin=? WHERE id = ?";
            preparedStatement = connection.prepareStatement(querySelfieCoin);
            preparedStatement.setInt(1, oldSelfieCoin);
            preparedStatement.setInt(2, convalidaSconto.getSelfie().getUtente().getId());
            preparedStatement.executeUpdate();
            /* Inserire il messaggio */
            querySelfieCoin = "INSERT INTO utente_messaggi_selfiecoin "
                    + "(id_utente, messaggio, livello, quantita, data_messaggio)"
                    + " VALUES (?, ?, ?, ?, ?)";
            preparedStatement = connection.prepareStatement(querySelfieCoin);
            preparedStatement.setInt(1, convalidaSconto.getSelfie().getUtente().getId());
            preparedStatement.setString(2,
                    "Hai gudagnato " + (int) selfieCoin + " selfie coin "
                    + "per lo sconto convalidato presso " + convalidaSconto.getSelfie().getEsercente().getNome() + "");
            preparedStatement.setInt(3, 1);
            preparedStatement.setInt(4, (int) selfieCoin);
            preparedStatement.setString(5, dataConvalida);
            preparedStatement.executeUpdate();
        }

        /**
         * Creo azione per gli utenti taggati
         */
        NotificationManager notificationManager = new NotificationManager();
        notificationManager.startSessione();
        for (Utente amicoTaggato : convalidaSconto.getSelfie().getUtentiTaggati()) {
            /**
             * 3 Casi:
             *
             * - Non ha mai usato niente, allora creo sconto, creo selfie gia
             * convalidato, mando notifica di sconto
             *
             * - Ha uno sconto in standby, quindi convalida lo sconto, creo
             * selfie da convalidare, mando notifica selfie coin
             *
             * - Ha gia usato uno sconto, creo selfie da convalidare, mando
             * notifica selfie coin
             */

            int selfieDaConvalidare = 0;
            int tipoNotifica = 0; // 1 sconto 2 selfiecoin

            query = "SELECT * FROM sconto WHERE id_utente =? AND id_esercente=?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, amicoTaggato.getId());
            preparedStatement.setInt(2, convalidaSconto.getSelfie().getEsercente().getId());
            resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) {
                /**
                 * Non ha mai usato niente
                 */
                Date oggi = new Date();
                int durataSconto = convalidaSconto.getSelfie().getEsercente().getContratto().getScontroScontrinoValidita();
                LocalDateTime localDateTime = oggi.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                localDateTime = localDateTime.plusDays(durataSconto);
                Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
                String dataScadenza = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentDatePlusOneDay);

                String querySconto = "INSERT INTO sconto "
                        + "(id_selfie_generato, id_utente, id_esercente, sconto, data_creazione, data_scadenza) "
                        + "VALUES (?, ?, ?, ?, ?, ?)";
                preparedStatement = connection.prepareStatement(querySconto, PreparedStatement.RETURN_GENERATED_KEYS);

                preparedStatement.setInt(1, idSelfie);
                preparedStatement.setInt(2, amicoTaggato.getId());
                preparedStatement.setInt(3, convalidaSconto.getSelfie().getEsercente().getId());
                preparedStatement.setInt(4, convalidaSconto.getSconto().getSconto());
                preparedStatement.setString(5, dataConvalida);
                preparedStatement.setString(6, dataScadenza);
                preparedStatement.executeUpdate();

                selfieDaConvalidare = 0;
                tipoNotifica = 1;

            } else if (resultSet.getInt("convalidato") == 0) {
                /**
                 * Ha uno sconto in sospeso
                 */
                int idScontoTemp = resultSet.getInt("id");
                query = "UPDATE sconto SET data_convalida=?, convalidato=1 WHERE id=?";
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, dataConvalida);
                preparedStatement.setInt(2, idScontoTemp);
                preparedStatement.executeUpdate();
                preparedStatement.close();
                selfieDaConvalidare = 1;
                tipoNotifica = 2;
            } else if (resultSet.getInt("convalidato") == 1) {
                selfieDaConvalidare = 1;
                tipoNotifica = 2;
            }

            /**
             * Inserisco il selfie
             */
            querySelfie = "INSERT INTO selfie "
                    + "(id_esercente, id_sconto, id_utente, id_selfie, selfie, testo, tipo_condivisione, data_registrazione, data_convalida, convalidato) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            preparedStatement = connection.prepareStatement(querySelfie, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, convalidaSconto.getSelfie().getEsercente().getId());
            preparedStatement.setInt(2, convalidaSconto.getSconto().getId());
            preparedStatement.setInt(3, amicoTaggato.getId());
            preparedStatement.setInt(4, idSelfie);
            preparedStatement.setString(5, nomeImmagineSelfie);
            preparedStatement.setString(6, convalidaSconto.getSelfie().getTesto());
            preparedStatement.setInt(7, convalidaSconto.getSelfie().getTipoCondivisione());
            preparedStatement.setString(8, dataConvalida);
            if (selfieDaConvalidare == 0) {
                preparedStatement.setString(9, null);
                preparedStatement.setInt(10, 0);
            } else {
                preparedStatement.setString(9, dataConvalida);
                preparedStatement.setInt(10, 1);
            }
            preparedStatement.executeUpdate();

            String testoNotifica = "";
            int tipoView = Costanti.ACTION_HOME_VIEW;
            if (tipoNotifica == 1) {
                testoNotifica = Costanti.NOTIFICA_SCONTO_DA_SCONTO;
                testoNotifica = testoNotifica.replace("#ESERCENTE", convalidaSconto.getSelfie().getEsercente().getNome());
                testoNotifica = testoNotifica.replace("#SCONTO", convalidaSconto.getSconto().getSconto() + "");
                tipoView = Costanti.ACTION_SCONTI_VIEW;
            } else if (tipoNotifica == 2) {
                testoNotifica = Costanti.NOTIFICA_SELFIE_DA_CONVALIDARE;
                testoNotifica = testoNotifica.replace("#ESERCENTE", convalidaSconto.getSelfie().getEsercente().getNome());
                testoNotifica = testoNotifica.replace("#SELFIECOIN", (int) selfieCoin + "");
                tipoView = Costanti.ACTION_SELFIES_VIEW;
            }

            querySelfie = "SELECT * FROM utente_device WHERE id_utente=?";
            preparedStatement = connection.prepareStatement(querySelfie);
            preparedStatement.setInt(1, amicoTaggato.getId());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getInt("tipo_dispositivo") == 1) {
                    notificationManager.inviaNotificaiOS(resultSet.getString("token"), "JustSelfie", testoNotifica, tipoView, 0);
                } else if (resultSet.getInt("tipo_dispositivo") == 2) {
                    notificationManager.inviaNotificaAndroid(resultSet.getString("token"), "JustSelfie", testoNotifica, tipoView, 0);
                }
            }
        }

        notificationManager.pulisciDatabase();

        risposta.put("idSelfie", idSelfie);
        risposta.put("nomeSelfie", nomeImmagineSelfie);
        risposta.put("selfieCoin", (int) selfieCoin);

        return risposta;
    }

    public static Object convalidaSelfie(Connection connection, ConvalidaSelfie convalidaSelfie) throws SQLException, IOException, ImageReadException, ImageWriteException {
        HashMap<String, Object> risposta = new HashMap<String, Object>();

        /**
         * Convalida selfie GESTIRE SE E' UNA CONVALIDA DI UN SELFIE GIA
         * ESISTENTE O L'INSERIMENTO DI UN NUOVO.
         *
         */
        String dataConvalida = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        int idSelfie = convalidaSelfie.getSelfie().getId();
        String nomeImmagineSelfie = "";
        double selfieCoin = convalidaSelfie.getSelfie().getEsercente().getContratto().getSelfieCoin();
        if (convalidaSelfie.getSelfie().getTipoCondivisione() == 2) {
            selfieCoin = (selfieCoin / 100) * (100 - convalidaSelfie.getSelfie().getEsercente().getContratto().getSelfieCoinRiduzioneNoLink());
        }

        if (idSelfie == 0) {

            /*Salvo immagine sul file system e libero memoria*/
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            nomeImmagineSelfie = calendar.getTimeInMillis() + "_" + convalidaSelfie.getSelfie().getUtente().getId() + ".jpg";
            byte[] decodedImg = Base64.getDecoder().decode(convalidaSelfie.getImmagineBase64().getBytes(StandardCharsets.UTF_8));
            Path destinationFile = Paths.get(Costanti.SELFIE_DIRECTORY, nomeImmagineSelfie);
            Files.write(destinationFile, decodedImg);

//            new ExifRewriter().removeExifMetadata(new File(Costanti.SELFIE_DIRECTORY, nomeImmagineSelfie), new FileOutputStream(new File(Costanti.SELFIE_DIRECTORY, nomeImmagineSelfie)));
            BufferedImage src = ImageIO.read(new File(Costanti.SELFIE_DIRECTORY, nomeImmagineSelfie));

            int width = src.getWidth();
            int height = src.getHeight();
            String[] temp = nomeImmagineSelfie.split(".");

            BufferedImage out = src.getSubimage(0, (height - 630) / 2, 1200, 630);
            String nomeTempImmagine = nomeImmagineSelfie.replace(".jpg", "") + "_prv.jpg";

            ImageIO.write(out, "jpg", new File(Costanti.SELFIE_DIRECTORY, nomeTempImmagine));
            convalidaSelfie.setImmagineBase64(null);

            /**
             * Inserisco selfie dell'utente
             */
            String querySelfie = "INSERT INTO selfie "
                    + "(id_esercente, id_utente, selfie, testo, tipo_condivisione, convalidato, data_registrazione, data_convalida) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(querySelfie, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, convalidaSelfie.getSelfie().getEsercente().getId());
            preparedStatement.setInt(2, convalidaSelfie.getSelfie().getUtente().getId());
            preparedStatement.setString(3, nomeImmagineSelfie);
            preparedStatement.setString(4, convalidaSelfie.getSelfie().getTesto());
            preparedStatement.setInt(5, convalidaSelfie.getSelfie().getTipoCondivisione());
            preparedStatement.setInt(6, 1);
            preparedStatement.setString(7, dataConvalida);
            preparedStatement.setString(8, dataConvalida);
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                idSelfie = generatedKeys.getInt(1);
            }

            /**
             * Creo selfie da convalidare per gli utenti taggati
             */
            for (Utente amicoTaggato : convalidaSelfie.getSelfie().getUtentiTaggati()) {
                querySelfie = "INSERT INTO selfie "
                        + "(id_esercente, id_utente, id_selfie, selfie, testo, tipo_condivisione, data_registrazione) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?)";
                preparedStatement = connection.prepareStatement(querySelfie, PreparedStatement.RETURN_GENERATED_KEYS);
                preparedStatement.setInt(1, convalidaSelfie.getSelfie().getEsercente().getId());
                preparedStatement.setInt(2, amicoTaggato.getId());
                preparedStatement.setInt(3, idSelfie);
                preparedStatement.setString(4, nomeImmagineSelfie);
                preparedStatement.setString(5, convalidaSelfie.getSelfie().getTesto());
                preparedStatement.setInt(6, convalidaSelfie.getSelfie().getTipoCondivisione());
                preparedStatement.setString(7, dataConvalida);
                preparedStatement.executeUpdate();
            }

            NotificationManager notificationManager = new NotificationManager();
            notificationManager.startSessione();

            String testoNotificaSelfieCoin = Costanti.NOTIFICA_SELFIE_DA_CONVALIDARE;
            testoNotificaSelfieCoin = testoNotificaSelfieCoin.replace("#ESERCENTE", convalidaSelfie.getSelfie().getEsercente().getNome());
            testoNotificaSelfieCoin = testoNotificaSelfieCoin.replace("#SELFIECOIN", (int) selfieCoin + "");

            for (Utente amicoTaggato : convalidaSelfie.getSelfie().getUtentiTaggati()) {
                querySelfie = "SELECT * FROM utente_device WHERE id_utente=?";
                preparedStatement = connection.prepareStatement(querySelfie);
                preparedStatement.setInt(1, amicoTaggato.getId());
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    if (resultSet.getInt("tipo_dispositivo") == 1) {
                        notificationManager.inviaNotificaiOS(resultSet.getString("token"), "JustSelfie", testoNotificaSelfieCoin, Costanti.ACTION_SELFIES_VIEW, 0);
                    } else if (resultSet.getInt("tipo_dispositivo") == 2) {
                        notificationManager.inviaNotificaAndroid(resultSet.getString("token"), "JustSelfie", testoNotificaSelfieCoin, Costanti.ACTION_SELFIES_VIEW, 0);
                    }
                }
            }

            notificationManager.pulisciDatabase();

        } else {
            String query = "UPDATE selfie SET data_convalida=?, convalidato=1 WHERE id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, dataConvalida);
            preparedStatement.setInt(2, convalidaSelfie.getSelfie().getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        }

        /**
         * Distribuisco selfie coin all'utente che ha convalidato
         */
        String querySelfieCoin = "SELECT * FROM utente WHERE id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(querySelfieCoin);
        preparedStatement.setInt(1, convalidaSelfie.getSelfie().getUtente().getId());
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            int oldSelfieCoin = resultSet.getInt("selfie_coin");
            oldSelfieCoin = oldSelfieCoin + (int) selfieCoin;
            querySelfieCoin = "UPDATE utente SET selfie_coin=? WHERE id = ?";
            preparedStatement = connection.prepareStatement(querySelfieCoin);
            preparedStatement.setInt(1, oldSelfieCoin);
            preparedStatement.setInt(2, convalidaSelfie.getSelfie().getUtente().getId());
            preparedStatement.executeUpdate();

            /* Inserire il messaggio */
            querySelfieCoin = "INSERT INTO utente_messaggi_selfiecoin "
                    + "(id_utente, messaggio, livello, quantita, data_messaggio)"
                    + " VALUES (?, ?, ?, ?, ?)";
            preparedStatement = connection.prepareStatement(querySelfieCoin);
            preparedStatement.setInt(1, convalidaSelfie.getSelfie().getUtente().getId());
            preparedStatement.setString(2,
                    "Hai gudagnato " + (int) selfieCoin + " selfie coin "
                    + "per il selfie convalidato presso " + convalidaSelfie.getSelfie().getEsercente().getNome() + "");
            preparedStatement.setInt(3, 1);
            preparedStatement.setInt(4, (int) selfieCoin);
            preparedStatement.setString(5, dataConvalida);
            preparedStatement.executeUpdate();
        }

        risposta.put("idSelfie", idSelfie);
        risposta.put("selfieCoin", (int) selfieCoin);
        risposta.put("nomeSelfie", nomeImmagineSelfie);

        return risposta;
    }

    public static Object convalidaGiftCard(Connection connection, ConvalidaGiftCard convalidaGiftCard) throws SQLException {
        HashMap<String, Object> risposta = new HashMap<>();
        String query = "SELECT * FROM esercente WHERE beacon_uuid=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, convalidaGiftCard.getBeaconUUID());
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            int idEsercente = 0;
            int selfieCoin = 0;
            String nomeEsercente = "";
            idEsercente = resultSet.getInt("id");
            nomeEsercente = resultSet.getString("nome");
            query = "SELECT * FROM utente WHERE id=?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, convalidaGiftCard.getIdUtente());
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                selfieCoin = resultSet.getInt("selfie_coin");
            }
            if (selfieCoin < convalidaGiftCard.getValoreSelfieCoin()) {
                throw new SQLException("Non hai abbastanza selfie coin");
            }
            selfieCoin = selfieCoin - convalidaGiftCard.getValoreSelfieCoin();
            query = "UPDATE utente SET selfie_coin=? WHERE id=?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, selfieCoin);
            preparedStatement.setInt(2, convalidaGiftCard.getIdUtente());
            preparedStatement.executeUpdate();
            query = "INSERT INTO utente_gift_card "
                    + "(id_utente, id_esercente, valore_selfiecoin, valore_euro, data_utilizzo)"
                    + " VALUES (?, ?, ?, ?, ?)";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, convalidaGiftCard.getIdUtente());
            preparedStatement.setInt(2, idEsercente);
            preparedStatement.setInt(3, convalidaGiftCard.getValoreSelfieCoin());
            preparedStatement.setInt(4, convalidaGiftCard.getValoreEuro());
            preparedStatement.setString(5, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            preparedStatement.executeUpdate();

            /* Inserire il messaggio */
            query = "INSERT INTO utente_messaggi_selfiecoin "
                    + "(id_utente, messaggio, livello, quantita, data_messaggio)"
                    + " VALUES (?, ?, ?, ?, ?)";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, convalidaGiftCard.getIdUtente());
            preparedStatement.setString(2,
                    "Hai speso " + convalidaGiftCard.getValoreSelfieCoin() + " selfie coin per la gift card da " + convalidaGiftCard.getValoreEuro() + "€");
            preparedStatement.setInt(3, 0);
            preparedStatement.setInt(4, -convalidaGiftCard.getValoreSelfieCoin());
            preparedStatement.setString(5, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            preparedStatement.executeUpdate();

            risposta.put("nomeEsercente", nomeEsercente);
            risposta.put("utente", DAOUtente.read(connection, convalidaGiftCard.getIdUtente(), new ArrayList<Utente>()));

        } else {
            throw new SQLException("Beacon non associato");
        }

        return risposta;
    }
}
