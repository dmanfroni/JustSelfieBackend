/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.dao;

import app.model.Campagna;
import app.model.Esercente;
import app.model.Ricerca;
import app.model.Sconto;
import app.model.Selfie;
import app.model.Utente;
import app.model.Contratto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import app.model.Categoria;
import model.DatiFatturazione;

/**
 *
 * @author Davide
 */
public class DAOEsercente {

    public static Object ricercaEsercenti(Connection connection, Ricerca ricerca) throws SQLException {
        HashMap<String, Object> risposta = new HashMap<>();
        List<Esercente> elencoEsercenti = new ArrayList<>();
        ResultSet resultSet = null;
        String query = "SELECT "
                + "esercente.*, "
                + "categoria.nome AS nome_categoria, categoria.icona, categoria.colore, contratto.offerta_sconto_minimo "
                + "FROM esercente "
                + "INNER JOIN categoria ON categoria.id = esercente.id_categoria "
                + "INNER JOIN contratto ON contratto.id = esercente.id_contratto "
                + "WHERE stato_app = 1 ";
        String filtriCategoria = "", filtriRicerca = "";
        if (ricerca.getIdCategoria() > 0) {
            filtriCategoria = " AND id_categoria=? ";
        }
        if (ricerca.getRicercaTesto() != null && !ricerca.getRicercaTesto().isEmpty()) {
            filtriRicerca = " AND (nome LIKE ? OR descrizione LIKE ? OR indirizzo LIKE ?)";
        }

        String filtro = filtriCategoria + filtriRicerca;
        if (filtro.isEmpty()) {
            resultSet = connection.createStatement().executeQuery(query);
        } else {
            query += filtro;
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            int indexParam = 1;
            if (!filtriCategoria.isEmpty()) {
                preparedStatement.setInt(indexParam, ricerca.getIdCategoria());
                indexParam++;
            }
            if (!filtriRicerca.isEmpty()) {
                preparedStatement.setString(indexParam, "%" + ricerca.getRicercaTesto() + "%");
                indexParam++;
                preparedStatement.setString(indexParam, "%" + ricerca.getRicercaTesto() + "%");
                indexParam++;
                preparedStatement.setString(indexParam, "%" + ricerca.getRicercaTesto() + "%");
                indexParam++;
            }
            resultSet = preparedStatement.executeQuery();
        }
        while (resultSet.next()) {
            Esercente esercente = new Esercente();
            esercente.setId(resultSet.getInt("id"));
            esercente.getCategoria().setId(resultSet.getInt("id_categoria"));
            esercente.getCategoria().setNome(resultSet.getString("nome_categoria"));
            esercente.getCategoria().setIcona(resultSet.getString("icona"));
            esercente.getCategoria().setColore(resultSet.getString("colore"));
            esercente.setNome(resultSet.getString("nome"));
            esercente.setDescrizione(resultSet.getString("descrizione"));
            esercente.setIndirizzo(resultSet.getString("indirizzo"));
            esercente.setEmail(resultSet.getString("email"));
            esercente.setScontoMinimo(resultSet.getInt("offerta_sconto_minimo"));
            esercente.setTelefono(resultSet.getString("telefono"));
            esercente.setImmagineProfilo(resultSet.getString("immagine_profilo"));
            esercente.setImmagineCopertina(resultSet.getString("immagine_copertina"));
            esercente.setLatitudine(resultSet.getDouble("latitudine"));
            esercente.setLongitudine(resultSet.getDouble("longitudine"));
            esercente.setContratto(null);
            esercente.setDatiFatturazione(null);
            elencoEsercenti.add(esercente);
        }
        risposta.put("elencoEsercenti", elencoEsercenti);
        return risposta;
    }

    public static Object schedaEsercente(Connection connection, Ricerca ricerca) throws SQLException {
        HashMap<String, Object> schedaEsercente = new HashMap<>();
        Esercente esercente = read(connection, ricerca.getIdEsercente());
        esercente.setDatiFatturazione(null);
        schedaEsercente.put("esercente", esercente);
        /**
         * Trovo id campagna attiva
         *
         * Se nella tabella selfie_tag non trovo l'associazione tra id utente e
         * id della campagna Allora leggo campagna
         *
         * Se nella tabella sconto trovo associazione id_utente e id_esercente e
         * validato=0 Allora leggo sconto
         *
         * Leggo selfie fatti presso l'esercente
         *
         */

        int idCampagna = 0;
        String queryCampagna = "SELECT id FROM campagna WHERE id_esercente=? AND attiva=1";
        PreparedStatement preparedStatement = connection.prepareStatement(queryCampagna);
        preparedStatement.setInt(1, ricerca.getIdEsercente());
        ResultSet resultCampagna = preparedStatement.executeQuery();
        if (resultCampagna.first()) {
            idCampagna = resultCampagna.getInt("id");
        }

        if (idCampagna > 0) {
            queryCampagna = "SELECT * FROM selfie WHERE id_utente=? AND (id_campagna=? OR (id_sconto!=0 AND id_esercente=?))";
            preparedStatement = connection.prepareStatement(queryCampagna);
            preparedStatement.setInt(1, ricerca.getIdUtente());
            preparedStatement.setInt(2, idCampagna);
            preparedStatement.setInt(3, ricerca.getIdEsercente());
            resultCampagna = preparedStatement.executeQuery();
            if (!resultCampagna.next()) {
                Campagna campagna = DAOCampagna.read(connection, idCampagna);
                schedaEsercente.put("campagna", campagna);
            }
        } else {

        }
        
        String dataOggi = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());

        String querySconto = "SELECT * FROM sconto WHERE id_esercente=? AND id_utente=? AND convalidato=0 AND data_scadenza >= ?";

        preparedStatement = connection.prepareStatement(querySconto);
        preparedStatement.setInt(1, ricerca.getIdEsercente());
        preparedStatement.setInt(2, ricerca.getIdUtente());
        preparedStatement.setString(3, dataOggi);
        ResultSet resultSconto = preparedStatement.executeQuery();
        if (resultSconto.next()) {
            Sconto sconto = new Sconto();
            sconto.setId(resultSconto.getInt("id"));
            sconto.setSconto(resultSconto.getInt("sconto"));
            sconto.setDataCreazione(resultSconto.getString("data_creazione"));
            sconto.setDataScadenza(resultSconto.getString("data_scadenza"));
            sconto.setDataConvalida(resultSconto.getString("data_convalida"));
            schedaEsercente.put("sconto", sconto);
        }
        

        String querySelfie = "SELECT selfie.*, utente.immagine_profilo FROM selfie INNER JOIN utente ON selfie.id_utente = utente.id WHERE id_esercente=?  AND id_selfie=0 AND convalidato=1";
        preparedStatement = connection.prepareStatement(querySelfie);
        preparedStatement.setInt(1, ricerca.getIdEsercente());
        ResultSet resultSetSelfie = preparedStatement.executeQuery();
        List<Selfie> elencoSelfie = new ArrayList<>();
        while (resultSetSelfie.next()) {
            Selfie selfie = new Selfie();
            selfie.setId(resultSetSelfie.getInt("id"));
            selfie.setSelfie(resultSetSelfie.getString("selfie"));
            selfie.setEsercente(null);
            Utente utente = new Utente();
            utente.setId(resultSetSelfie.getInt("id_utente"));
            utente.setImmagineProfilo(resultSetSelfie.getString("immagine_profilo"));
            selfie.setUtente(utente);
            elencoSelfie.add(selfie);
        }
        schedaEsercente.put("selfie", elencoSelfie);

        return schedaEsercente;
    }

    public static void update(Connection connection, Esercente esercente) throws SQLException {
        String dataContratto = null;

        String query = "UPDATE esercente SET "
                + "id_categoria=?, id_contratto=?, nome=?, indirizzo=?, email=?, pec=?, telefono=?, "
                + "immagine_profilo=?, immagine_copertina=?, id_facebook=?, id_instagram=?, latitudine=?, longitudine=?, "
                + "beacon_uuid=? WHERE id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, esercente.getCategoria().getId());
        preparedStatement.setInt(2, esercente.getContratto().getId());
        preparedStatement.setString(3, esercente.getNome());
        preparedStatement.setString(4, esercente.getIndirizzo());
        preparedStatement.setString(5, esercente.getEmail());
        preparedStatement.setString(6, esercente.getPec());
        preparedStatement.setString(7, esercente.getTelefono());
        preparedStatement.setString(8, esercente.getImmagineProfilo());
        preparedStatement.setString(9, esercente.getImmagineCopertina());
        preparedStatement.setString(10, esercente.getIdFacebook());
        preparedStatement.setString(11, esercente.getIdInstagram());
        preparedStatement.setDouble(12, esercente.getLatitudine());
        preparedStatement.setDouble(13, esercente.getLongitudine());
        preparedStatement.setString(14, esercente.getBeaconUUID());
        preparedStatement.setInt(15, esercente.getId());
        preparedStatement.executeUpdate();
       

        query = "UPDATE esercente_dati_fatturazione SET "
                + "ragione_sociale=?, partita_iva=?, codice_fiscale=?, "
                + "sede_legale=?, iban=?, data_contratto=?, contratto=? WHERE id_esercente=? AND id > 0";
        preparedStatement = connection.prepareCall(query);
        preparedStatement.setString(1, esercente.getDatiFatturazione().getRagioneSociale());
        preparedStatement.setString(2, esercente.getDatiFatturazione().getPartitaIva());
        preparedStatement.setString(3, esercente.getDatiFatturazione().getCodiceFiscale());
        preparedStatement.setString(4, esercente.getDatiFatturazione().getSedeLegale());
        preparedStatement.setString(5, esercente.getDatiFatturazione().getIban());
        preparedStatement.setString(6, esercente.getDatiFatturazione().getDataContratto());
        preparedStatement.setString(7, esercente.getDatiFatturazione().getPdfContratto());
        preparedStatement.setInt(8, esercente.getId());
        preparedStatement.executeUpdate();

    }

    public static Esercente create(Connection connection, Esercente esercente) throws SQLException {
        String dataContratto = null;

        String query = "INSERT INTO esercente (id_categoria, id_contratto, nome, descrizione, indirizzo, email, pec, telefono, "
                + "immagine_profilo, immagine_copertina, id_facebook, id_instagram, latitudine, longitudine, "
                + "stato_app, stato_backend, beacon_uuid, password) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, esercente.getCategoria().getId());
        preparedStatement.setInt(2, esercente.getContratto().getId());
        preparedStatement.setString(3, esercente.getNome());
        preparedStatement.setString(4, esercente.getDescrizione());
        preparedStatement.setString(5, esercente.getIndirizzo());
        preparedStatement.setString(6, esercente.getEmail());
        preparedStatement.setString(7, esercente.getPec());
        preparedStatement.setString(8, esercente.getTelefono());
        preparedStatement.setString(9, esercente.getImmagineProfilo());
        preparedStatement.setString(10, esercente.getImmagineCopertina());
        preparedStatement.setString(11, esercente.getIdFacebook());
        preparedStatement.setString(12, esercente.getIdInstagram());
        preparedStatement.setDouble(13, esercente.getLatitudine());
        preparedStatement.setDouble(14, esercente.getLongitudine());
        preparedStatement.setInt(15, 0);
        preparedStatement.setInt(16, 0);
        preparedStatement.setString(17, esercente.getBeaconUUID());
        preparedStatement.setString(18, esercente.getDatiFatturazione().getPartitaIva());
        preparedStatement.executeUpdate();
        ResultSet generatedKey = preparedStatement.getGeneratedKeys();
        if (generatedKey.next()) {
            esercente.setId(generatedKey.getInt(1));
        }

        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dataRegistrazione = myFormat.format(new Date());

        query = "INSERT INTO esercente_dati_fatturazione "
                + "(id_esercente, ragione_sociale, partita_iva, codice_fiscale, sede_legale, iban, data_registrazione, data_contratto, contratto)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        preparedStatement = connection.prepareCall(query);
        preparedStatement.setInt(1, esercente.getId());
        preparedStatement.setString(2, esercente.getDatiFatturazione().getRagioneSociale());
        preparedStatement.setString(3, esercente.getDatiFatturazione().getPartitaIva());
        preparedStatement.setString(4, esercente.getDatiFatturazione().getCodiceFiscale());
        preparedStatement.setString(5, esercente.getDatiFatturazione().getSedeLegale());
        preparedStatement.setString(6, esercente.getDatiFatturazione().getIban());
        preparedStatement.setString(7, dataRegistrazione);
        preparedStatement.setString(8, dataContratto);
        preparedStatement.setString(9, esercente.getDatiFatturazione().getPdfContratto());
        preparedStatement.executeUpdate();

        return esercente;

    }

    public static Esercente read(Connection connection, int id) throws SQLException {
        Esercente esercente = new Esercente();
        Contratto contratto = new Contratto();
        Categoria categoria = new Categoria();
        DatiFatturazione datiFatturazione = new DatiFatturazione();
        String query = "SELECT * FROM esercente WHERE id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.first()) {
            esercente.setId(resultSet.getInt("id"));
            categoria.setId(resultSet.getInt("id_categoria"));
            contratto.setId(resultSet.getInt("id_contratto"));
            esercente.setNome(resultSet.getString("nome"));
            esercente.setDescrizione(resultSet.getString("descrizione"));
            esercente.setIndirizzo(resultSet.getString("indirizzo"));
            esercente.setTelefono(resultSet.getString("telefono"));
            esercente.setEmail(resultSet.getString("email"));
            esercente.setPec(resultSet.getString("pec"));
            esercente.setImmagineProfilo(resultSet.getString("immagine_profilo"));
            esercente.setImmagineCopertina(resultSet.getString("immagine_copertina"));
            esercente.setIdFacebook(resultSet.getString("id_facebook"));
            esercente.setIdInstagram(resultSet.getString("id_instagram"));
            esercente.setLatitudine(resultSet.getDouble("latitudine"));
            esercente.setLongitudine(resultSet.getDouble("longitudine"));
            esercente.setStatoApp(resultSet.getInt("stato_app"));
            esercente.setStatoBackend(resultSet.getInt("stato_backend"));
            esercente.setBeaconUUID(resultSet.getString("beacon_uuid"));
        }

        preparedStatement.close();
        resultSet.close();

        query = "SELECT * FROM esercente_dati_fatturazione WHERE id=?";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, id);
        resultSet = preparedStatement.executeQuery();
        if (resultSet.first()) {
            datiFatturazione.setId(resultSet.getInt("id"));
            datiFatturazione.setIdEsercente(resultSet.getInt("id_esercente"));
            datiFatturazione.setRagioneSociale(resultSet.getString("ragione_sociale"));
            datiFatturazione.setPartitaIva(resultSet.getString("partita_iva"));
            datiFatturazione.setCodiceFiscale(resultSet.getString("codice_fiscale"));
            datiFatturazione.setSedeLegale(resultSet.getString("sede_legale"));
            datiFatturazione.setIban(resultSet.getString("iban"));
            datiFatturazione.setDataRegistrazione(resultSet.getString("data_registrazione"));
            String dataContratto = resultSet.getString("data_contratto");
            if(dataContratto != null && !dataContratto.isEmpty())
                dataContratto = dataContratto.split("-")[2] + "-" + dataContratto.split("-")[1] + "-" + dataContratto.split("-")[0];
            else
                dataContratto = "";
            datiFatturazione.setDataContratto(dataContratto);
            datiFatturazione.setPdfContratto(resultSet.getString("contratto"));
        }

        preparedStatement.close();
        resultSet.close();

        contratto = DAOContratti.readContratto(connection, contratto.getId());

        categoria = DAOCategorie.readCategoria(connection, categoria.getId());

        esercente.setCategoria(categoria);
        esercente.setContratto(contratto);
        esercente.setDatiFatturazione(datiFatturazione);

        return esercente;
    }

    public static List<Esercente> read(Connection connection) throws Exception {
        List<Esercente> elencoEsercenti = new ArrayList<>();

        String query = "SELECT * FROM esercente WHERE 1";
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        while (resultSet.next()) {
            Esercente esercente = new Esercente();
            esercente.setId(resultSet.getInt("id"));
            esercente.setNome(resultSet.getString("nome"));
            esercente.setStatoApp(resultSet.getInt("stato_app"));
            esercente.setStatoBackend(resultSet.getInt("stato_backend"));
            esercente.setLatitudine(resultSet.getDouble("latitudine"));
            esercente.setLongitudine(resultSet.getDouble("longitudine"));
            esercente.setBeaconUUID(resultSet.getString("beacon_uuid"));
            elencoEsercenti.add(esercente);
        }
        return elencoEsercenti;
    }

    public static void toggleStatoBackend(Connection connection, int idEsercente, int statoBackend) throws SQLException {
        String query = "UPDATE esercente SET stato_backend=? WHERE id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, statoBackend);
        preparedStatement.setInt(2, idEsercente);
        preparedStatement.executeUpdate();
    }

    public static void toggleStatoApp(Connection connection, int idEsercente, int statoApp) throws SQLException {
        String query = "UPDATE esercente SET stato_app=? WHERE id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, statoApp);
        preparedStatement.setInt(2, idEsercente);
        preparedStatement.executeUpdate();
    }

}
