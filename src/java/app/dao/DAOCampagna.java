/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.dao;

import app.model.Campagna;
import app.model.Esercente;
import app.model.Offerta;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import app.model.Categoria;

/**
 *
 * @author Davide
 */
public class DAOCampagna {
    
     public static Campagna read(Connection connection, int id) throws SQLException{
         Campagna campagna = new Campagna();
         String query = "SELECT * FROM campagna WHERE id=?";
         PreparedStatement preparedStatement = connection.prepareStatement(query);
         preparedStatement.setInt(1, id);
         ResultSet resultSet = preparedStatement.executeQuery();
         if(resultSet.first()){
             campagna.setId(id);
             campagna.setNome(resultSet.getString("nome"));
             campagna.setSconto(resultSet.getInt("sconto"));
         }
         List<Offerta> offerte = new ArrayList<>();
         
         query = "SELECT * FROM offerta WHERE id_campagna=" + campagna.getId();
         resultSet = connection.createStatement().executeQuery(query);
         while(resultSet.next()){
             Offerta offerta = new Offerta();
             offerta.setId(resultSet.getInt("id"));
             offerta.setNome(resultSet.getString("nome"));
             offerta.setDescrizione(resultSet.getString("descrizione"));
             offerta.setSconto(resultSet.getInt("sconto"));
             offerta.setPrezzoPieno(resultSet.getDouble("prezzo_pieno"));
             offerta.setPrezzoScontato(resultSet.getDouble("prezzo_scontato"));
             offerta.setImmagine(resultSet.getString("immagine"));
             offerte.add(offerta);
         }
         campagna.setOfferte(offerte);
         return campagna;
        
    }
     
     public static Object readElencoOfferte(Connection connection, int idUtente) throws SQLException{
         /**
          * Leggo prima tutte le campagne da escludere dalla ricerca
          */
         HashMap<String, Object> elencoOfferte = new HashMap<String, Object>();
          List<Offerta> offerte = new ArrayList<>();
         String query = "SELECT id_campagna FROM selfie WHERE id_campagna > 0 AND id_utente = ?";
         PreparedStatement preparedStatement = connection.prepareStatement(query);
         preparedStatement.setInt(1, idUtente);
         ResultSet resultSet = preparedStatement.executeQuery();
         String queryOfferte = "SELECT offerta.*, "
                 + "esercente.nome AS nome_esercente, esercente.id AS id_esercente, esercente.latitudine, esercente.longitudine, "
                 + "categoria.id AS id_categoria, categoria.nome AS nome_categoria, categoria.icona AS icona_categoria, categoria.colore " +
            "FROM offerta " +
            "INNER JOIN ("
                 + "(campagna INNER JOIN esercente ON (campagna.id_esercente = esercente.id AND esercente.stato_app=1)) "
                 + "INNER JOIN categoria ON esercente.id_categoria = categoria.id "
                 + ") ON offerta.id_campagna = campagna.id WHERE 1=1";
         while(resultSet.next()){
             queryOfferte += " AND id_campagna!=" + resultSet.getInt("id_campagna");
         }
         resultSet = connection.createStatement().executeQuery(queryOfferte);
         while(resultSet.next()){
             Offerta offerta = new Offerta();
             offerta.setId(resultSet.getInt("id"));
             offerta.setNome(resultSet.getString("nome"));
             offerta.setDescrizione(resultSet.getString("descrizione"));
             offerta.setSconto(resultSet.getInt("sconto"));
             offerta.setPrezzoPieno(resultSet.getDouble("prezzo_pieno"));
             offerta.setPrezzoScontato(resultSet.getDouble("prezzo_scontato"));
             offerta.setImmagine(resultSet.getString("immagine"));
             Esercente esercente = new Esercente();
             esercente.setId(resultSet.getInt("id_esercente"));
             esercente.setNome(resultSet.getString("nome_esercente"));
             esercente.setLatitudine(resultSet.getDouble("latitudine"));
             esercente.setLongitudine(resultSet.getDouble("longitudine"));
             Categoria categoria = new Categoria();
             categoria.setId(resultSet.getInt("id_categoria"));
             categoria.setNome(resultSet.getString("nome_categoria"));
             categoria.setIcona(resultSet.getString("icona_categoria"));
             categoria.setColore(resultSet.getString("colore"));
             esercente.setCategoria(categoria);
             offerta.setEsercente(esercente);
             offerte.add(offerta);
         }
         elencoOfferte.put("elencoOfferte", offerte);
         return  elencoOfferte;
         
     }
    
    
    
}
