/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import app.model.Categoria;
import newsletter.model.Template;
import newsletter.model.TipoRegistrazione;
import newsletter.model.EmailUtente;

/**
 *
 * @author Davide
 */
public class NewsLetterDAO {

    public static List<Template> readTemplate(Connection connection) throws SQLException {
        List<Template> elencoTemplate = new ArrayList<>();
        String query = "SELECT * FROM " + Costanti.TBL_NEWSLETTER_TEMPLATE + " WHERE 1=1";
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        while (resultSet.next()) {
            Template template = new Template();
            template.setId(resultSet.getInt("id"));
            template.setSendFrom(resultSet.getString("send_from"));
            template.setSubject(resultSet.getString("subject"));
            template.setTitle(resultSet.getString("title"));
            template.setText(resultSet.getString("text"));
            template.setSpotUser(resultSet.getInt("spot_user") == 1);
            template.setSpotMerchant(resultSet.getInt("spot_merchant") == 1);
            elencoTemplate.add(template);
        }
        resultSet.close();
        return elencoTemplate;

    }

    public static Template readTemplate(Connection connection, int id) throws SQLException {
        Template template = new Template();
        String query = "SELECT * FROM " + Costanti.TBL_NEWSLETTER_TEMPLATE + " WHERE id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            template.setId(resultSet.getInt("id"));
            template.setSendFrom(resultSet.getString("send_from"));
            template.setSubject(resultSet.getString("subject"));
            template.setTitle(resultSet.getString("title"));
            template.setText(resultSet.getString("text"));
            template.setSpotUser(resultSet.getInt("spot_user") == 1);
            template.setSpotMerchant(resultSet.getInt("spot_merchant") == 1);
        }
        preparedStatement.close();
        resultSet.close();
        return template;

    }

    public static void createTemplate(Connection connection, Template template) throws SQLException {
        String query = "INSERT INTO " + Costanti.TBL_NEWSLETTER_TEMPLATE + " (send_from, subject, title, text, spot_user, spot_merchant) "
                + "VALUES (?, ?, ?, ?, ?, ?) ";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, template.getSendFrom());
        preparedStatement.setString(2, template.getSubject());
        preparedStatement.setString(3, template.getTitle());
        preparedStatement.setString(4, template.getText());
        preparedStatement.setInt(5, template.getSpotUser() ? 1 : 0);
        preparedStatement.setInt(6, template.getSpotMerchant() ? 1 : 0);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public static void updateTemplate(Connection connection, Template template) throws SQLException {
        String query = "UPDATE " + Costanti.TBL_NEWSLETTER_TEMPLATE
                + " SET send_from=?, subject=?, title=?, text=?, spot_user=?, spot_merchant=? WHERE id=? ";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, template.getSendFrom());
        preparedStatement.setString(2, template.getSubject());
        preparedStatement.setString(3, template.getTitle());
        preparedStatement.setString(4, template.getText());
        preparedStatement.setInt(5, template.getSpotUser() ? 1 : 0);
        preparedStatement.setInt(6, template.getSpotMerchant() ? 1 : 0);
        preparedStatement.setInt(7, template.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public static void deleteTemplate(Connection connection, Template template) throws SQLException {
        String query = "DELETE FROM " + Costanti.TBL_NEWSLETTER_TEMPLATE + " WHERE id=? ";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, template.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public static List<TipoRegistrazione> readTipoRegistrazione(Connection connection) throws SQLException {
        List<TipoRegistrazione> elencoTipiRegistrazione = new ArrayList<>();
        String query = "SELECT * FROM tipo_registrazione WHERE 1=1";
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        while (resultSet.next()) {
            TipoRegistrazione tipoRegistrazione = new TipoRegistrazione();
            tipoRegistrazione.setId(resultSet.getInt("id"));
            tipoRegistrazione.setName(resultSet.getString("name"));
            elencoTipiRegistrazione.add(tipoRegistrazione);
        }
        resultSet.close();

        for (int i = 0; i < elencoTipiRegistrazione.size(); i++) {
            query = "SELECT COUNT(*) AS  count \n"
                    + "FROM email_utente WHERE tipo_registrazione=" + elencoTipiRegistrazione.get(i).getId() 
                    + " GROUP BY tipo_registrazione   ";
            resultSet = connection.createStatement().executeQuery(query);
            if(resultSet.first())
                elencoTipiRegistrazione.get(i).setTotale(resultSet.getInt("count"));
            else
                elencoTipiRegistrazione.get(i).setTotale(0);
        }
        return elencoTipiRegistrazione;

    }

    public static TipoRegistrazione readTipoRegistrazione(Connection connection, int id) throws SQLException {
        TipoRegistrazione tipoRegistrazione = new TipoRegistrazione();
        String query = "SELECT * FROM " + Costanti.TBL_NEWSLETTER_TEMPLATE + " WHERE id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            tipoRegistrazione.setId(resultSet.getInt("id"));
            tipoRegistrazione.setName(resultSet.getString("name"));
        }
        preparedStatement.close();
        resultSet.close();
        return tipoRegistrazione;

    }

    public static void createTipoRegistrazione(Connection connection, TipoRegistrazione tipoRegistrazione) throws SQLException {
        String query = "INSERT INTO " + Costanti.TBL_NEWSLETTER_TEMPLATE + " (name) "
                + "VALUES (?) ";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, tipoRegistrazione.getName());
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public static void updateTipoRegistrazione(Connection connection, TipoRegistrazione tipoRegistrazione) throws SQLException {
        String query = "UPDATE " + Costanti.TBL_NEWSLETTER_TEMPLATE
                + " SET name=? WHERE id=? ";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, tipoRegistrazione.getName());
        preparedStatement.setInt(2, tipoRegistrazione.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public static void deleteTipoRegistrazione(Connection connection, TipoRegistrazione tipoRegistrazione) throws SQLException {
        String query = "DELETE FROM " + Costanti.TBL_NEWSLETTER_TEMPLATE + " WHERE id=? ";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, tipoRegistrazione.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    /*
    
     */
    public static List<EmailUtente> readEmailUtente(Connection connection) throws SQLException {
        List<EmailUtente> elencoEmailUtente = new ArrayList<>();
        String query = "SELECT * FROM " + Costanti.TBL_NEWSLETTER_TEMPLATE + " "
                + " INNER JOIN tipo_registrazione ON tipo_registrazione.id = email_utente.tipo_registrazione "
                + " WHERE 1=1 ORDER BY registrato_il DESC";
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        while (resultSet.next()) {
            EmailUtente emailUtente = new EmailUtente();
            emailUtente.setId(resultSet.getInt("id"));
            emailUtente.setName(resultSet.getString("nome"));
            emailUtente.setEmail(resultSet.getString("email"));
            emailUtente.setCategories(resultSet.getString("categorie"));
            emailUtente.setSelfieCoin(resultSet.getInt("selfie_coin"));
            emailUtente.setRegisteredAt(resultSet.getString("registrato_il"));
            emailUtente.setRegistrationType(resultSet.getInt("tipo_registrazione"));
            emailUtente.setRegistrationTypeString(resultSet.getString("name"));
            elencoEmailUtente.add(emailUtente);
        }
        resultSet.close();
        return elencoEmailUtente;
    }

    public static List<EmailUtente> readEmailUtenteFiltrate(Connection connection, int idTipoRegistrazione) throws SQLException {
        List<EmailUtente> elencoEmailUtente = new ArrayList<>();
        String query = "SELECT * FROM " + Costanti.TBL_NEWSLETTER_TEMPLATE + " WHERE tipo_registrazione=" + idTipoRegistrazione + " ORDER BY registrato_il DESC";
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        while (resultSet.next()) {
            EmailUtente emailUtente = new EmailUtente();
            emailUtente.setId(resultSet.getInt("id"));
            emailUtente.setName(resultSet.getString("nome"));
            emailUtente.setEmail(resultSet.getString("email"));
            emailUtente.setCategories(resultSet.getString("categorie"));
            emailUtente.setSelfieCoin(resultSet.getInt("selfie_coin"));
            emailUtente.setRegisteredAt(resultSet.getString("registrato_il"));
            elencoEmailUtente.add(emailUtente);
        }
        resultSet.close();
        return elencoEmailUtente;
    }

    public static List<EmailUtente> readEmailUtenteLimit(Connection connection, int idTipoRegistrazione, int offset, int limit) throws SQLException {
        List<EmailUtente> elencoEmailUtente = new ArrayList<>();
        String query = "SELECT * FROM " + Costanti.TBL_NEWSLETTER_TEMPLATE + " WHERE registration_type=" + idTipoRegistrazione + " LIMIT " + offset + "," + limit;
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        while (resultSet.next()) {
            EmailUtente emailUtente = new EmailUtente();
            emailUtente.setId(resultSet.getInt("id"));
            emailUtente.setName(resultSet.getString("nome"));
            emailUtente.setEmail(resultSet.getString("email"));
            emailUtente.setCategories(resultSet.getString("categorie"));
            emailUtente.setSelfieCoin(resultSet.getInt("selfie_coin"));
            emailUtente.setRegisteredAt(resultSet.getString("registrato_il"));
            emailUtente.setRegistrationType(resultSet.getInt("tipo_registrazione"));
            elencoEmailUtente.add(emailUtente);
        }
        resultSet.close();
        return elencoEmailUtente;
    }

    public static EmailUtente readEmailUtente(Connection connection, int id) throws SQLException {
        EmailUtente emailUtente = new EmailUtente();
        String query = "SELECT * FROM " + Costanti.TBL_NEWSLETTER_TEMPLATE + " WHERE id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            emailUtente.setId(resultSet.getInt("id"));
            emailUtente.setName(resultSet.getString("nome"));
            emailUtente.setEmail(resultSet.getString("email"));
            emailUtente.setCategories(resultSet.getString("categorie"));
            emailUtente.setSelfieCoin(resultSet.getInt("selfie_coin"));
            emailUtente.setRegisteredAt(resultSet.getString("registrato_il"));
            emailUtente.setRegistrationType(resultSet.getInt("tipo_registrazione"));
        }
        preparedStatement.close();
        resultSet.close();
        return emailUtente;
    }

    public static void createEmailUtente(Connection connection, EmailUtente emailUtente) throws SQLException {
        String query = "INSERT INTO " + Costanti.TBL_NEWSLETTER_TEMPLATE + " (nome, email, categorie, selfie_coin, registrato_il, tipo_registrazione) "
                + "VALUES (?, ?, ?, ?, ?, ?) ";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, emailUtente.getName().trim());
        preparedStatement.setString(2, emailUtente.getEmail().trim());
        preparedStatement.setString(3, emailUtente.getCategories());
        preparedStatement.setDouble(4, emailUtente.getSelfieCoin());
        preparedStatement.setString(5, emailUtente.getRegisteredAt());
        preparedStatement.setInt(6, emailUtente.getRegistrationType());
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public static void updateEmailUtente(Connection connection, EmailUtente emailUtente) throws SQLException {
        String query = "UPDATE " + Costanti.TBL_NEWSLETTER_TEMPLATE
                + " SET nome=?, email=?, categorie=?, selfie_coin=?, registrato_il=?, tipo_registrazione=? WHERE id=? ";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, emailUtente.getName());
        preparedStatement.setString(2, emailUtente.getEmail());
        preparedStatement.setString(3, emailUtente.getCategories());
        preparedStatement.setDouble(4, emailUtente.getSelfieCoin());
        preparedStatement.setString(5, emailUtente.getRegisteredAt());
        preparedStatement.setInt(6, emailUtente.getRegistrationType());
        preparedStatement.setInt(7, emailUtente.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public static void deleteEmailUtente(Connection connection, EmailUtente emailUtente) throws SQLException {
        String query = "DELETE FROM " + Costanti.TBL_NEWSLETTER_TEMPLATE + " WHERE id=? ";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, emailUtente.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

}
