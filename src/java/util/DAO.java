/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Davide
 */
public class DAO {

    public static boolean doLogin(String username, String password, Connection connection) throws SQLException {
        ResultSet resultSet;
        String query = "SELECT * FROM amministratori WHERE username=? AND password=?  ";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setString(1, username);
        ps.setString(2, password);
        resultSet = ps.executeQuery();
        if (resultSet.first()) {
            return true;
        }
        resultSet.close();
        ps.close();
        return false;
    }
}
