/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import app.notification.FcmResponse;
import app.notification.NotificationRaven;
import app.notification.Pushraven;
import com.google.gson.Gson;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Davide
 */
public class NotificationManager {

    private final List<String> tokenValidi;
    private final List<String> tokenScaduti;

    public NotificationManager() {
        tokenScaduti = new ArrayList<>();
        tokenValidi = new ArrayList<>();
    }

    public void startSessione() {
        tokenValidi.clear();
        tokenScaduti.clear();
    }

    public List<String> getTokenValidi() {
        return tokenValidi;
    }

    public List<String> getTokenScaduti() {
        return tokenScaduti;
    }

    public void inviaNotificaiOS(String token, String titolo, String messaggio, int view, int id) {
        if (token != null && !token.isEmpty()) {
            Pushraven.setKey(Costanti.ANDROID_NOTIFICATION_KEY);
            HashMap<String, Object> data = new HashMap<>();
            data.put("titolo", titolo);
            data.put("testo", messaggio);
            data.put("view", view);
            data.put("id", id);
            List<String> tokens = new ArrayList<>();
            tokens.add(token);
            NotificationRaven raven = new NotificationRaven();
            raven.registration_ids(tokens)
                    .data(data)
                    .title(titolo)
                    .body(messaggio)
                    .addNotificationAttribute("click_action", "notification")
                    .addNotificationAttribute("badge", "1")
                    .sound("default");
            FcmResponse response = Pushraven.push(raven);
            aggiungiReport(response, token);
        }

    }

    public void inviaNotificaAndroid(String token, String titolo, String messaggio, int view, int id) {
        if (token != null && !token.isEmpty()) {
            Pushraven.setKey(Costanti.ANDROID_NOTIFICATION_KEY);
            HashMap<String, Object> data = new HashMap<>();
            data.put("titolo", titolo);
            data.put("testo", messaggio);
            data.put("view", view);
            data.put("id", id);
            List<String> tokens = new ArrayList<>();
            tokens.add(token);
            NotificationRaven raven = new NotificationRaven();
            raven.registration_ids(tokens).data(data);
            FcmResponse response = Pushraven.push(raven);
            aggiungiReport(response, token);
        }

    }

    private void aggiungiReport(FcmResponse response, String token) {
        String successMessage = response.getSuccessResponseMessage();
        int code = response.getResponseCode();
        if (code == 200) {
            try {
                Map<Object, Object> map = new Gson().fromJson(successMessage, Map.class);
                double success = (double) map.get("success");
                double failure = (double) map.get("failure");
                if (success == 1.0) {
                    tokenValidi.add(token);
                }
                if (failure == 1.0) {
                    tokenScaduti.add(token);
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                Logger.log(ex.getMessage());
            }
        }
    }

    public void pulisciDatabase() {
        try (Connection connection = DBConnection.getConnection()) {
            for (String token : tokenScaduti) {
                String query = "DELETE FROM utente_device WHERE token=? AND id > 0";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, token);
                preparedStatement.executeUpdate();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            Logger.log(ex.getMessage());
        }
    }

    public void stampaReport() {
        StringBuilder report = new StringBuilder();
        report.append(tokenValidi.size()).append(" notifiche inviate correttamente - ");
        report.append(tokenScaduti.size()).append(" token scaduti e rimossi");
        Logger.log(report.toString());
    }

}
