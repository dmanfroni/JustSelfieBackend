/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Davide
 */
@ManagedBean(name = "Costanti")
@ViewScoped
public class Costanti implements Serializable {

    public static final String MD5_KEY = "Aiz238d34ddfsje2_4349";

    public static final String NOTIFICA_SCONTO_DA_OFFERTA = "Fantastico! Usando la tua offerta di benvenuto, "
            + "hai guadagnato #SCONTO% di sconto da utilizzare entro #GIORNIVALIDITA giorni presso #ESERCENTE!";

    public static final String NOTIFICA_SCONTO_DA_SCONTO = "EHI! Sei stato taggato in una foto presso #ESERCENTE, "
            + "sblocca e recati in cassa per convalidare con JS Beacon e ottenere subito il #SCONTO% di sconto.";

    public static final String NOTIFICA_SELFIE_DA_CONVALIDARE = "EHI! Sei stato taggato in una foto presso #ESERCENTE, "
            + "sblocca e recati in cassa per convalidare con JS Beacon e gudagnare #SELFIECOIN Selfie Coin!";

    public static final String TBL_NEWSLETTER_TEMPLATE = "newsletter_template";
    public static final String TBL_USER_ACQUISITION = "email_utente";
    public static final String TBL_REGISTRATION_TYPE = "tipo_registrazione";
    public static final String TBL_CATEGORIA = "categoria";
    public static final String TBL_CATEGORIA_CONTRATTO = "categoria_contratto";
    public static final String TBL_CATEGORIA_SCAGLIONI_SELFIE = "categoria_scaglioni_selfie";
    
    
    public static String REPORT_FILE = "/var/www/vhosts/justselfie.it/cron_jobs/report_id_utente.txt";
    public static String REPORT_NOTIFICHE_FILE = "/var/www/vhosts/justselfie.it/cron_jobs/report.txt";
    //public static String REPORT_NOTIFICHE_FILE = "C:\\Users\\Davide\\Documents\\appcontent\\cron_jobs\\report.txt";

    
    /*public static String IMMAGINI_DIRECTORY = "/var/www/vhosts/justselfie.it/demo/appcontent/immagini";
    public static String SELFIE_DIRECTORY = "/var/www/vhosts/justselfie.it/demo/appcontent/selfie";
    public static String CONTRATTI_DIRECTORY = "/var/www/vhosts/justselfie.it/demo/appcontent/contratti";
    public static String ESPORTAZIONE_CONTATTI = "/var/www/vhosts/justselfie.it/demo/appcontent/newsletter_esportazioni";
    */

    
    public static String IMMAGINI_DIRECTORY = "/var/www/vhosts/justselfie.it/httpdocs/appcontent/immagini";
    public static String SELFIE_DIRECTORY = "/var/www/vhosts/justselfie.it/httpdocs/appcontent/selfie";
    public static String CONTRATTI_DIRECTORY = "/var/www/vhosts/justselfie.it/httpdocs/appcontent/contratti";
    public static String ESPORTAZIONE_CONTATTI = "/var/www/vhosts/justselfie.it/httpdocs/appcontent/newsletter_esportazioni";
    
    
    /*
    public static String IMMAGINI_DIRECTORY = "C:\\Users\\Davide\\Documents\\appcontent\\immagini\\";
    public static String SELFIE_DIRECTORY = "C:\\Users\\Davide\\Documents\\appcontent\\selfie\\";
    public static String CONTRATTI_DIRECTORY = "C:\\Users\\Davide\\Documents\\appcontent\\contratti\\";
    public static String ESPORTAZIONE_CONTATTI = "C:\\Users\\Davide\\Documents\\appcontent\\esp_contatti\\";
    */
    
    public static final String DB_DRIVER_CLASS = "com.mysql.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://localhost:3306/";
    public static final String DB_USERNAME = "root";
    public static final String DB_PASSWORD = "bbp5$22P";
    public static final boolean produzione = true;
    //public final static String DB_PASSWORD = "root";
    
    public final static String DB_NAME = "justselfie?useUnicode=yes&amp;characterEncoding=UTF8";
    //public static final String DB_NAME = "admin_test?useUnicode=yes&amp;characterEncoding=UTF8";
    

    public static int factorySelfieCoin = 5;
    public static String dayPromoEnd = "2016-12-12 00:00:00";

    public static String ANDROID_NOTIFICATION_URL = "https://fcm.googleapis.com/fcm/send";
    public static String ANDROID_NOTIFICATION_KEY = "AIzaSyDP5G85mrNGR4OLVXV-DCo_155fsBBru_s";
    
    public static final int ACTION_HOME_VIEW = 1;
    public static final int ACTION_SCONTI_VIEW = 2;
    public static final int ACTION_SELFIES_VIEW = 3;
    public static final int ACTION_MERCHANT_VIEW = 4;
    public static final int ACTION_OFFER_VIEW = 5;
    public static final int ACTION_JSCARD_VIEW = 6;
    public static final int ACTION_OFFER_PENDING_VIEW = 69;

    /**
     * @return the MD5_KEY
     */
    public static String getMD5_KEY() {
        return MD5_KEY;
    }

    /**
     * @return the NOTIFICA_SCONTO_DA_OFFERTA
     */
    public static String getNOTIFICA_SCONTO_DA_OFFERTA() {
        return NOTIFICA_SCONTO_DA_OFFERTA;
    }

    /**
     * @return the NOTIFICA_SCONTO_DA_SCONTO
     */
    public static String getNOTIFICA_SCONTO_DA_SCONTO() {
        return NOTIFICA_SCONTO_DA_SCONTO;
    }

    /**
     * @return the NOTIFICA_SELFIE_DA_CONVALIDARE
     */
    public static String getNOTIFICA_SELFIE_DA_CONVALIDARE() {
        return NOTIFICA_SELFIE_DA_CONVALIDARE;
    }

    /**
     * @return the TBL_NEWSLETTER_TEMPLATE
     */
    public static String getTBL_NEWSLETTER_TEMPLATE() {
        return TBL_NEWSLETTER_TEMPLATE;
    }

    /**
     * @return the TBL_USER_ACQUISITION
     */
    public static String getTBL_USER_ACQUISITION() {
        return TBL_USER_ACQUISITION;
    }

    /**
     * @return the TBL_REGISTRATION_TYPE
     */
    public static String getTBL_REGISTRATION_TYPE() {
        return TBL_REGISTRATION_TYPE;
    }

    /**
     * @return the TBL_CATEGORIA
     */
    public static String getTBL_CATEGORIA() {
        return TBL_CATEGORIA;
    }

    /**
     * @return the TBL_CATEGORIA_CONTRATTO
     */
    public static String getTBL_CATEGORIA_CONTRATTO() {
        return TBL_CATEGORIA_CONTRATTO;
    }

    /**
     * @return the TBL_CATEGORIA_SCAGLIONI_SELFIE
     */
    public static String getTBL_CATEGORIA_SCAGLIONI_SELFIE() {
        return TBL_CATEGORIA_SCAGLIONI_SELFIE;
    }

    /**
     * @return the IMMAGINI_DIRECTORY
     */
    public static String getIMMAGINI_DIRECTORY() {
        return IMMAGINI_DIRECTORY;
    }

    /**
     * @return the SELFIE_DIRECTORY
     */
    public static String getSELFIE_DIRECTORY() {
        return SELFIE_DIRECTORY;
    }

    /**
     * @return the CONTRATTI_DIRECTORY
     */
    public static String getCONTRATTI_DIRECTORY() {
        return CONTRATTI_DIRECTORY;
    }

    /**
     * @return the ESPORTAZIONE_CONTATTI
     */
    public static String getESPORTAZIONE_CONTATTI() {
        return ESPORTAZIONE_CONTATTI;
    }

    /**
     * @return the DB_DRIVER_CLASS
     */
    public static String getDB_DRIVER_CLASS() {
        return DB_DRIVER_CLASS;
    }

    /**
     * @return the DB_URL
     */
    public static String getDB_URL() {
        return DB_URL;
    }

    /**
     * @return the DB_USERNAME
     */
    public static String getDB_USERNAME() {
        return DB_USERNAME;
    }

    /**
     * @return the DB_PASSWORD
     */
    public static String getDB_PASSWORD() {
        return DB_PASSWORD;
    }

    /**
     * @return the DB_NAME
     */
    public static String getDB_NAME() {
        return DB_NAME;
    }

    /**
     * @return the factorySelfieCoin
     */
    public static int getFactorySelfieCoin() {
        return factorySelfieCoin;
    }

    /**
     * @return the dayPromoEnd
     */
    public static String getDayPromoEnd() {
        return dayPromoEnd;
    }

    /**
     * @return the ANDROID_NOTIFICATION_URL
     */
    public static String getANDROID_NOTIFICATION_URL() {
        return ANDROID_NOTIFICATION_URL;
    }

    /**
     * @return the ANDROID_NOTIFICATION_KEY
     */
    public static String getANDROID_NOTIFICATION_KEY() {
        return ANDROID_NOTIFICATION_KEY;
    }

    public Costanti() {

    }

    public static String sStringToHMACMD5(String s, String keyString) {
        String sEncodedString = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), "HmacMD5");
            Mac mac = Mac.getInstance("HmacMD5");
            mac.init(key);

            byte[] bytes = mac.doFinal(s.getBytes("ASCII"));

            StringBuffer hash = new StringBuffer();

            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            sEncodedString = hash.toString();
        } catch (UnsupportedEncodingException e) {
        } catch (InvalidKeyException e) {
        } catch (NoSuchAlgorithmException e) {
        }
        return sEncodedString;
    }

    public static String getDataTimeDisplay(String input) {
        try {
            String expectedPattern = "yyyy-MM-dd HH:mm";
            DateFormat df = new SimpleDateFormat(expectedPattern);
            Date todaysDate = df.parse(input);
            DateFormat df6 = new SimpleDateFormat("E, dd MMM HH:mm");
            String str6 = df6.format(todaysDate);
            return str6.toUpperCase();
        } catch (ParseException ex) {
            return "";
            //Logger.getLogger(ControlloVeicolo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String getDataDisplay(String input) {
        try {
            String expectedPattern = "yyyy-MM-dd";
            DateFormat df = new SimpleDateFormat(expectedPattern);
            Date todaysDate = df.parse(input);
            DateFormat df6 = new SimpleDateFormat("E, dd MMM");
            String str6 = df6.format(todaysDate);
            return str6.toUpperCase();
        } catch (ParseException ex) {
            return "";
            //Logger.getLogger(ControlloVeicolo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String getDataFormatted(String input) {
        try {
            String expectedPattern = "yyyy-MM-dd";
            DateFormat df = new SimpleDateFormat(expectedPattern);
            Date todaysDate = df.parse(input);
            DateFormat df6 = new SimpleDateFormat("dd-MM-yyyy");
            String str6 = df6.format(todaysDate);
            return str6.toUpperCase();
        } catch (ParseException ex) {
            return "";
            //Logger.getLogger(ControlloVeicolo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String getDataInverse(String input) {
        try {
            String expectedPattern = "dd-MM-yyyy";
            DateFormat df = new SimpleDateFormat(expectedPattern);
            Date todaysDate = df.parse(input);
            DateFormat df6 = new SimpleDateFormat("yyyy-MM-dd");
            String str6 = df6.format(todaysDate);
            return str6.toUpperCase();
        } catch (ParseException ex) {
            return "";
            //Logger.getLogger(ControlloVeicolo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String encrypt(String Data) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(MD5_KEY.getBytes());
        md.update(Data.getBytes());
        byte hash[] = md.digest();
        StringBuffer sb = new StringBuffer();
        for (byte b : hash) {
            sb.append(String.format("%02x", b & 0xff));
        }
        return sb.toString();

    }

    /* public static String decrypt(String encryptedData) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
        byte[] decValue = c.doFinal(decordedValue);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }
    private static Key generateKey() throws Exception {
        Key key = new SecretKeySpec(keyValue, ALGO);
        return key;
}
     */
}
