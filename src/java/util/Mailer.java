/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.File;
import java.util.Properties;
import java.util.Scanner;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Davide
 */
public class Mailer extends Authenticator {

    private static final String SMTP_HOST_NAME = "justselfie.it";
    private static final String SMTP_AUTH_USER = "info@justselfie.it";
    private static final String SMTP_AUTH_PWD = "Manfro92";
    private static final String SMTP_PORT = "25";

    public void emailAttivazioneApp(String email) throws Exception {
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", SMTP_HOST_NAME);
        props.put("mail.smtp.user", SMTP_AUTH_USER);
        props.put("mail.smtp.password", SMTP_AUTH_PWD);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", SMTP_PORT);
          props.put("mail.debug", "true");

        ClassLoader classLoader = getClass().getClassLoader();
        File input = new File(classLoader.getResource("/newsletter/properties/email_attivazione_app.html").getFile());
        StringBuilder result = new StringBuilder();
        try (Scanner scanner = new Scanner(input)) {
            while (scanner.hasNextLine()) {
                result.append(scanner.nextLine());
            }
            scanner.close();
        }

        String emailBody = result.toString();

       Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                String username = SMTP_AUTH_USER;
                String password = SMTP_AUTH_PWD;
                return new PasswordAuthentication(username, password);
            }
        });

       // session = session.getInstance(props, this);

        Transport transport = session.getTransport("smtp");

        Message message = new MimeMessage(session);

        message.setFrom(new InternetAddress("assistenza@justselfie.it"));
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
        message.setSubject("Attivazione JustSelfieApp");
        message.setContent(emailBody, "text/html");
      transport.connect(SMTP_HOST_NAME, SMTP_AUTH_USER, SMTP_AUTH_PWD);
        transport.send(message);
        transport.close();
    }

    public void emailAttivazioneBackend(String email, String password) throws Exception {
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", SMTP_HOST_NAME);
        props.put("mail.smtp.user", SMTP_AUTH_USER);
        props.put("mail.smtp.password", SMTP_AUTH_PWD);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", SMTP_PORT);
          props.put("mail.debug", "true");

        ClassLoader classLoader = getClass().getClassLoader();
        File input = new File(classLoader.getResource("/newsletter/properties/email_attivazione_backend.html").getFile());
        StringBuilder result = new StringBuilder();
        try (Scanner scanner = new Scanner(input)) {
            while (scanner.hasNextLine()) {
                result.append(scanner.nextLine());
            }
            scanner.close();
        }

        String emailBody = result.toString();
        emailBody = emailBody.replace("#EMAIL", email);
        emailBody = emailBody.replace("#PASSWORD", password);

       Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                String username = SMTP_AUTH_USER;
                String password = SMTP_AUTH_PWD;
                return new PasswordAuthentication(username, password);
            }
        });

       // session = session.getInstance(props, this);

        Transport transport = session.getTransport("smtp");

        Message message = new MimeMessage(session);

        message.setFrom(new InternetAddress("assistenza@justselfie.it"));
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
        message.setSubject("Attivazione area riservata");
        message.setContent(emailBody, "text/html");
      transport.connect(SMTP_HOST_NAME, SMTP_AUTH_USER, SMTP_AUTH_PWD);
        transport.send(message);
        transport.close();
    }

    private class SMTPAuthenticator extends javax.mail.Authenticator {

        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            String username = SMTP_AUTH_USER;
            String password = SMTP_AUTH_PWD;
            return new PasswordAuthentication(username, password);
        }
    }
}
