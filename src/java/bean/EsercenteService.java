/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import app.dao.DAOEsercente;
import app.model.Esercente;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import app.model.Categoria;
import org.primefaces.model.UploadedFile;
import util.Costanti;
import util.DBConnection;
import util.Mailer;

/**
 *
 * @author Davide
 */
@ManagedBean(name = "esercentiService")
@ViewScoped
public class EsercenteService implements Serializable {

    private List<Esercente> elencoEsercenti;
    private Esercente esercenteSelezionato;
    private UploadedFile file;

    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.containsKey("id")) {
            int id = Integer.valueOf(params.get("id"));
            try (Connection connection = DBConnection.getJustSelfieConnection()) {
                setEsercenteSelezionato(DAOEsercente.read(connection, id));
            } catch (ClassNotFoundException | SQLException ex) {
                System.out.println(ex.getMessage());
                setEsercenteSelezionato(new Esercente());
            }
        } else {
            try (Connection connection = DBConnection.getJustSelfieConnection()) {
                setElencoEsercenti(DAOEsercente.read(connection));
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                setElencoEsercenti(new ArrayList<>());
            }
        }
    }

    
    public String createOrUpdateIfExist() {
        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            byte[] bytes = null;
            if (null != file && !file.getFileName().isEmpty()) {
                bytes = file.getContents();
                String filename = file.getFileName();
                filename= filename.replace(" ", "_");
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(Costanti.CONTRATTI_DIRECTORY, filename)));
                stream.write(bytes);
                stream.close();
                getEsercenteSelezionato().getDatiFatturazione().setPdfContratto(filename);
            } 
            String dateFormat = getEsercenteSelezionato().getDatiFatturazione().getDataContratto().split("-")[2]
                    + "-" + getEsercenteSelezionato().getDatiFatturazione().getDataContratto().split("-")[1]
                    + "-" + getEsercenteSelezionato().getDatiFatturazione().getDataContratto().split("-")[0];
            getEsercenteSelezionato().getDatiFatturazione().setDataContratto(dateFormat);
            if (getEsercenteSelezionato().getId() > 0) {
                DAOEsercente.update(connection, getEsercenteSelezionato());
            } else {
                DAOEsercente.create(connection, getEsercenteSelezionato());
            }
            return "elencoEsercenti";
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Qualcosa è andato storto durante l'aggiornamento. Ricontrollare i dati inseriti"));
            return "";
        }
        
    }
    
    public void delete(Esercente esercenti){
        
    }
    
    public String abilitaBackend(Esercente esercente){
        try(Connection connection = DBConnection.getJustSelfieConnection()){
            DAOEsercente.toggleStatoBackend(connection, esercente.getId(), 1);
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM esercente WHERE id=?");
            preparedStatement.setInt(1, esercente.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                esercente.setPassword(resultSet.getString("password"));
                esercente.setEmail(resultSet.getString("email"));
            }
            Mailer mailer = new Mailer();
            mailer.emailAttivazioneBackend(esercente.getEmail(), esercente.getPassword());
            
        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Qualcosa è andato storto durante l'aggiornamento. Ricontrollare i dati inseriti"));
        }
        return "elencoEsercenti";
        
        
    }
    
    public String disabilitaBackend(Esercente esercente){
        try(Connection connection = DBConnection.getJustSelfieConnection()){
            DAOEsercente.toggleStatoBackend(connection, esercente.getId(), 0);
        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Qualcosa è andato storto durante l'aggiornamento. Ricontrollare i dati inseriti"));
        }
        return "elencoEsercenti";
    }
    
    public String abilitaApp(Esercente esercente){
         try(Connection connection = DBConnection.getJustSelfieConnection()){
            DAOEsercente.toggleStatoApp(connection, esercente.getId(), 1);
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM esercente WHERE id=?");
            preparedStatement.setInt(1, esercente.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                esercente.setEmail(resultSet.getString("email"));
            }
            Mailer mailer = new Mailer();
            mailer.emailAttivazioneApp(esercente.getEmail());
        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Qualcosa è andato storto durante l'aggiornamento. Ricontrollare i dati inseriti"));
        }
        return "elencoEsercenti";
    }
    
    public String disabilitaApp(Esercente esercente){
         try(Connection connection = DBConnection.getJustSelfieConnection()){
            DAOEsercente.toggleStatoApp(connection, esercente.getId(), 0);
        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Qualcosa è andato storto durante l'aggiornamento. Ricontrollare i dati inseriti"));
        }
        return "elencoEsercenti";
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    /**
     * @return the elencoEsercenti
     */
    public List<Esercente> getElencoEsercenti() {
        return elencoEsercenti;
    }

    /**
     * @param elencoEsercenti the elencoEsercenti to set
     */
    public void setElencoEsercenti(List<Esercente> elencoEsercenti) {
        this.elencoEsercenti = elencoEsercenti;
    }

    /**
     * @return the esercenteSelezionato
     */
    public Esercente getEsercenteSelezionato() {
        return esercenteSelezionato;
    }

    /**
     * @param esercenteSelezionato the esercenteSelezionato to set
     */
    public void setEsercenteSelezionato(Esercente esercenteSelezionato) {
        this.esercenteSelezionato = esercenteSelezionato;
    }
}
