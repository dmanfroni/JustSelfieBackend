/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import app.model.CodicePromo;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import app.model.Categoria;
import org.primefaces.model.UploadedFile;
import util.Costanti;
import app.dao.DAOCategorie;
import app.dao.DAOCodicePromo;
import util.DBConnection;

/**
 *
 * @author Davide
 */
@ManagedBean(name = "codiciPromoService")
@ViewScoped
public class CodiciPromoService implements Serializable{
     public List<CodicePromo> elencoCodiciPromo;
    public CodicePromo codicePromoSelezionato;

    @PostConstruct
    public void init() {
         Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.containsKey("id")) {
            int id = Integer.valueOf(params.get("id"));
            try(Connection connection = DBConnection.getJustSelfieConnection()) {
                setCodicePromoSelezionato(DAOCodicePromo.readCodicePromo(connection, id));
            } catch (ClassNotFoundException | SQLException ex) {
                System.out.println(ex.getMessage());
                setCodicePromoSelezionato(new CodicePromo());
            }
        } else {
            try(Connection connection = DBConnection.getJustSelfieConnection()) {
                setElencoCodiciPromo(DAOCodicePromo.readCodicePromo(connection));
            } catch (ClassNotFoundException | SQLException ex) {
                System.out.println(ex.getMessage());
                setElencoCodiciPromo(new ArrayList<>());
            }
        }
    }

    public String createOrUpdateIfExist() {
        try(Connection connection = DBConnection.getJustSelfieConnection()) {
            
            if (getCodicePromoSelezionato().getId() > 0) {
                DAOCodicePromo.updateCodicePromo(connection, getCodicePromoSelezionato());
               
            } else {
                DAOCodicePromo.createCodicePromo(connection, getCodicePromoSelezionato());
                
            }
             return "elencoCodiciPromo";
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Qualcosa è andato storto durante l'aggiornamento. Ricontrollare i dati inseriti"));
            return "";
        }
    }
    
    public void delete(CodicePromo codicePromo) {
        try (Connection connection = DBConnection.getJustSelfieConnection();) {
            getElencoCodiciPromo().remove(codicePromo);
            DAOCodicePromo.deleteCodicePromo(connection, codicePromo);
        } catch (Exception ex) {

        }
    }

    

    public List<Categoria> loadCategorie() {
        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            return DAOCategorie.readCategoria(connection);
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex.getMessage());
            return new ArrayList<>();
        }
    }

    /**
     * @return the elencoCodiciPromo
     */
    public List<CodicePromo> getElencoCodiciPromo() {
        return elencoCodiciPromo;
    }

    /**
     * @param elencoCodiciPromo the elencoCodiciPromo to set
     */
    public void setElencoCodiciPromo(List<CodicePromo> elencoCodiciPromo) {
        this.elencoCodiciPromo = elencoCodiciPromo;
    }

    /**
     * @return the codicePromoSelezionato
     */
    public CodicePromo getCodicePromoSelezionato() {
        return codicePromoSelezionato;
    }

    /**
     * @param codicePromoSelezionato the codicePromoSelezionato to set
     */
    public void setCodicePromoSelezionato(CodicePromo codicePromoSelezionato) {
        this.codicePromoSelezionato = codicePromoSelezionato;
    }
    
}
