/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import util.DBConnection;
import util.NotificationManager;

/**
 *
 * @author Davide
 */
@ManagedBean(name = "notificheService")
@ViewScoped
public class NotificheService implements Serializable {

    private String titolo;
    private String testo;
    private int view;
    private String id;
    private int tipoInvio;
    private String testToken;

    public NotificheService() {

    }

    public void inviaNotifiche() {
        NotificationManager notificationManager = new NotificationManager();
        notificationManager.startSessione();
        if (view == 4 && id.matches("")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Inserire l'id dell'esercente", ""));
            return;
        } else if (view != 4) {
            id = "0";
        }
        if ((tipoInvio == 0 || tipoInvio == 1) && testToken.matches("")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Inserire il token", ""));
            return;
        } else if (tipoInvio == 0) {
            notificationManager.inviaNotificaiOS(testToken, titolo, testo, view, Integer.valueOf(id));
        } else if (tipoInvio == 1) {
            notificationManager.inviaNotificaAndroid(testToken, titolo, testo, view, Integer.valueOf(id));
        } else if (tipoInvio != 0) {
            String query = "SELECT * FROM utente_device WHERE tipo_dispositivo ";
            String operatore = "=";
            if (tipoInvio == 4) {
                operatore = "<";
            }
            query = query + operatore + (tipoInvio - 1);
            try (Connection connection = DBConnection.getJustSelfieConnection()) {
                ResultSet resultSet = connection.createStatement().executeQuery(query);
                while (resultSet.next()) {
                    String token = resultSet.getString("token");
                    int dispositivo = resultSet.getInt("tipo_dispositivo");
                    if (dispositivo == 1) {
                        notificationManager.inviaNotificaiOS(token, titolo, testo, view, Integer.valueOf(id));
                    } else if (dispositivo == 2) {
                        notificationManager.inviaNotificaAndroid(token, titolo, testo, view, Integer.valueOf(id));
                    }
                }
                notificationManager.pulisciDatabase();
            } catch (Exception ex) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            }
        }
        
        notificationManager.stampaReport();
        int notificheInviate = notificationManager.getTokenValidi().size();
        int notificheScadute = notificationManager.getTokenScaduti().size();
        FacesContext.getCurrentInstance().addMessage(
                null, 
                new FacesMessage(FacesMessage.SEVERITY_INFO, notificheInviate + " notifiche inviate - " + notificheScadute + " token scaduti", ""));
    }

    /**
     * @return the titolo
     */
    public String getTitolo() {
        return titolo;
    }

    /**
     * @param titolo the titolo to set
     */
    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    /**
     * @return the testo
     */
    public String getTesto() {
        return testo;
    }

    /**
     * @param testo the testo to set
     */
    public void setTesto(String testo) {
        this.testo = testo;
    }

    /**
     * @return the view
     */
    public int getView() {
        return view;
    }

    /**
     * @param view the view to set
     */
    public void setView(int view) {
        this.view = view;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the tipoInvio
     */
    public int getTipoInvio() {
        return tipoInvio;
    }

    /**
     * @param tipoInvio the tipoInvio to set
     */
    public void setTipoInvio(int tipoInvio) {
        this.tipoInvio = tipoInvio;
    }

    /**
     * @return the testToken
     */
    public String getTestToken() {
        return testToken;
    }

    /**
     * @param testToken the testToken to set
     */
    public void setTestToken(String testToken) {
        this.testToken = testToken;
    }

}
