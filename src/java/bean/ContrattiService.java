/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import app.model.Contratto;
import model.ScaglioneSelfie;
import app.dao.DAOContratti;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Scope;
import newsletter.model.EmailUtente;
import org.primefaces.model.UploadedFile;
import util.DBConnection;
import util.NewsLetterDAO;

/**
 *
 * @author Davide
 */
@ManagedBean(name = "contrattiService")
@ViewScoped
public class ContrattiService implements Serializable {

    private List<Contratto> elencoContratti;
    private Contratto contratto;

    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.containsKey("id")) {
            int id = Integer.valueOf(params.get("id"));
            try (Connection connection = DBConnection.getJustSelfieConnection()) {
                setContratto(DAOContratti.readContratto(connection, id));
            } catch (ClassNotFoundException | SQLException ex) {
                System.out.println(ex.getMessage());
                setContratto(new Contratto());
            }
        } else {
            try (Connection connection = DBConnection.getJustSelfieConnection()) {
                setElencoContratti(DAOContratti.readContratto(connection));
            } catch (ClassNotFoundException | SQLException ex) {
                System.out.println(ex.getMessage());
                setElencoContratti(new ArrayList<>());
            }
        }
    }

    public List<Contratto> getContratti() {
        return elencoContratti;
    }

    public List<Contratto> loadContratti() {
        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            return DAOContratti.readContratto(connection);
        } catch (ClassNotFoundException | SQLException ex) {
            return new ArrayList<>();
        }
    }

    public String createOrUpdateIfExist() {
        try {
            Connection connection = DBConnection.getJustSelfieConnection();
            if (getContratto().getId() > 0) {
                DAOContratti.updateContratto(connection, getContratto());
                return "elencoContratti";
            } else {
                DAOContratti.createContratto(connection, getContratto());
                return "elencoContratti";
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Qualcosa è andato storto durante l'aggiornamento. Ricontrollare i dati inseriti"));
            return "";
        }
    }

   
    public void delete(Contratto contratto) {
        try (Connection connection = DBConnection.getJustSelfieConnection();) {
            getElencoContratti().remove(contratto);
            DAOContratti.deleteContratto(connection, contratto);
        } catch (Exception ex) {

        }
    }

    /**
     * @return the elencoContratti
     */
    public List<Contratto> getElencoContratti() {
        return elencoContratti;
    }

    /**
     * @param elencoContratti the elencoContratti to set
     */
    public void setElencoContratti(List<Contratto> elencoContratti) {
        this.elencoContratti = elencoContratti;
    }

    /**
     * @return the contratto
     */
    public Contratto getContratto() {
        return contratto;
    }

    /**
     * @param contratto the contratto to set
     */
    public void setContratto(Contratto contratto) {
        this.contratto = contratto;
    }


}
