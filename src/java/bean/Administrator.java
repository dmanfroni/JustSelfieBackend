/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.servlet.http.HttpSession;
import util.DAO;
import util.DBConnection;
import util.ReCaptchaService;
import util.Session;

/**
 *
 * @author Davide
 */
@ManagedBean(name = "administrator")
@SessionScoped
public class Administrator implements Serializable{

    private int id;
    private String name;
    private String username;
    private String password;
    private long serialVersionUID;

    @PostConstruct
    public void init() {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return serialVersionUID;
    }

    public void setId(long id) {
        this.serialVersionUID = id;
    }

    public String doLogin() {
        //if (checkCaptcha()) {
        try( Connection connection = DBConnection.getAmministrazioneConnection();) {
           
            if (DAO.doLogin(username, password, connection)) {
                HttpSession httpSession = Session.getSession();
                httpSession.setAttribute("auth", 1);
                return "index";
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Inserisci i dati corretti"));
                return "";
            }
        } catch (ClassNotFoundException | SQLException | NullPointerException ex) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Inserisci i dati corretti"));
                return "";
        }
      
    }

    public String doLogout() {
        HttpSession session = Session.getSession();
        session.invalidate();
        return "logout";
    }

    private boolean checkCaptcha() {
        return ReCaptchaService.verify(getReCaptchaResponse());
    }

    private String getReCaptchaResponse() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("g-recaptcha-response");
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

}
