/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import app.model.Categoria;
import app.dao.DAOCategorie;
import util.DBConnection;

/**
 *
 * @author Davide
 */
@ManagedBean(name = "giftcardService")
@ViewScoped
public class GiftCardService implements Serializable {

    private List<Categoria> elencoCategorie;
    private Categoria categoriaSelezionata;

    @PostConstruct
    public void init() {
         Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.containsKey("id")) {
            int id = Integer.valueOf(params.get("id"));
            try(Connection connection = DBConnection.getJustSelfieConnection()) {
                setCategoriaSelezionata(DAOCategorie.readCategoria(connection, id));
            } catch (ClassNotFoundException | SQLException ex) {
                System.out.println(ex.getMessage());
                setCategoriaSelezionata(new Categoria());
            }
        } else {
            try(Connection connection = DBConnection.getJustSelfieConnection()) {
                setElencoCategorie(DAOCategorie.readCategoria(connection));
            } catch (ClassNotFoundException | SQLException ex) {
                System.out.println(ex.getMessage());
                setElencoCategorie(new ArrayList<>());
            }
        }
    }

    public String createOrUpdateIfExist() {
        try(Connection connection = DBConnection.getJustSelfieConnection()) {
            
            if (getCategoriaSelezionata().getId() > 0) {
                DAOCategorie.updateCategoria(connection, getCategoriaSelezionata());
                return "elencoCategorie";
            } else {
                DAOCategorie.createCategoria(connection, getCategoriaSelezionata());
                return "elencoCategorie";
            }
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", "Qualcosa è andato storto durante l'aggiornamento. Ricontrollare i dati inseriti"));
            return "";
        }
    }
    
    public void delete(Categoria categoria) {
        try (Connection connection = DBConnection.getJustSelfieConnection();) {
            getElencoCategorie().remove(categoria);
            DAOCategorie.deleteCategoria(connection, categoria);
        } catch (Exception ex) {

        }
    }

    /**
     * @return the elencoCategorie
     */
    public List<Categoria> getElencoCategorie() {
        return this.elencoCategorie;
    }

    public List<Categoria> loadCategorie() {
        try (Connection connection = DBConnection.getJustSelfieConnection()) {
            return DAOCategorie.readCategoria(connection);
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex.getMessage());
            return new ArrayList<>();
        }
    }

    /**
     * @param elencoCategorie the elencoCategorie to set
     */
    public void setElencoCategorie(List<Categoria> elencoCategorie) {
        this.elencoCategorie = elencoCategorie;
    }
    
    /**
     * @return the categoriaSelezionata
     */
    public Categoria getCategoriaSelezionata() {
        return categoriaSelezionata;
    }

    /**
     * @param categoriaSelezionata the categoriaSelezionata to set
     */
    public void setCategoriaSelezionata(Categoria categoriaSelezionata) {
        this.categoriaSelezionata = categoriaSelezionata;
    }

}
