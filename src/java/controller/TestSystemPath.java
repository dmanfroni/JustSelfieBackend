/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.pushraven.FcmResponse;
import com.pushraven.Notification;
import com.pushraven.Pushraven;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.Costanti;

/**
 *
 * @author dmanfroni
 */
@WebServlet(name = "TestSystemPath", urlPatterns = {"/getSystemPath"})
@MultipartConfig
public class TestSystemPath extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse httpResponse) throws ServletException, IOException {
        String title = request.getParameter("title");
        String text = request.getParameter("text");
        String view = request.getParameter("view");
        String id = request.getParameter("id");
        String token = request.getParameter("token");
        ServletContext servletContext = getServletContext();
        String contextPath = servletContext.getRealPath(File.separator);
        PrintWriter out = httpResponse.getWriter();
        out.println("<br/>File system context path (in TestServlet): " + contextPath);
        
        File file = new File(Costanti.CONTRATTI_DIRECTORY);
        if(file.exists())
            out.println("asas!");
            
        else{
            file.mkdirs();
                out.println(file.getAbsolutePath());
            System.out.println(file.getAbsolutePath());
        }
        Pushraven.setKey("AIzaSyDP5G85mrNGR4OLVXV-DCo_155fsBBru_s");
		
		
		// create Notification object 
		Notification raven = new Notification();

		
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("titolo", title);
		data.put("testo", text);
		data.put("view", Integer.parseInt(view));
                data.put("id", Integer.parseInt(id));
		
		// build raven message using the builder pattern
		raven
                        .to(token)
			.data(data)
			.title(title)
			.body(text)
                        .sound("default")
                        .addRequestAttribute("content_available", true);
		
		
		

		// push the raven message
		FcmResponse response = Pushraven.push(raven);
		
		// alternatively set static notification first.
		/*Pushraven.setNotification(raven);
		response = Pushraven.push();*/
		
		// prints response code and message
		System.out.println(response);
    }

}
