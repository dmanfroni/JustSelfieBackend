/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.pushraven.FcmResponse;
import com.pushraven.Notification;
import com.pushraven.Pushraven;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.Costanti;

/**
 *
 * @author Davide
 */
@WebServlet(name = "FileSystemParse", urlPatterns = {"/fileSystemParse"})
@MultipartConfig
public class FileSystemParse extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse httpResponse) throws ServletException, IOException {
        File directory = new File(Costanti.SELFIE_DIRECTORY);
        File[] elencoFile = directory.listFiles();
        for(File file : elencoFile){
            String fileName = file.getName();
            fileName = fileName.replace("tmp", "prv");
            file.renameTo(new File(directory, fileName));
            
        }
    }

}
